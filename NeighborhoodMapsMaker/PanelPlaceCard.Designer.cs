﻿namespace NeighborhoodMapsMaker
{
    partial class PanelPlaceCard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelPlaceCard));
            this.lblBizName = new DevExpress.XtraEditors.LabelControl();
            this.lblBizAdd = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBizName
            // 
            this.lblBizName.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBizName.Appearance.Options.UseFont = true;
            this.lblBizName.Appearance.Options.UseTextOptions = true;
            this.lblBizName.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lblBizName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblBizName.Location = new System.Drawing.Point(3, 3);
            this.lblBizName.Name = "lblBizName";
            this.lblBizName.Size = new System.Drawing.Size(600, 20);
            this.lblBizName.TabIndex = 0;
            this.lblBizName.Text = "App Inc.,";
            // 
            // lblBizAdd
            // 
            this.lblBizAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBizAdd.Appearance.Options.UseFont = true;
            this.lblBizAdd.Appearance.Options.UseTextOptions = true;
            this.lblBizAdd.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lblBizAdd.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblBizAdd.Location = new System.Drawing.Point(20, 21);
            this.lblBizAdd.Name = "lblBizAdd";
            this.lblBizAdd.Size = new System.Drawing.Size(579, 19);
            this.lblBizAdd.TabIndex = 1;
            this.lblBizAdd.Text = "North Tantau Avenue, Cupertino, CA, USA";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // PanelPlaceCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblBizAdd);
            this.Controls.Add(this.lblBizName);
            this.Name = "PanelPlaceCard";
            this.Size = new System.Drawing.Size(606, 44);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblBizName;
        private DevExpress.XtraEditors.LabelControl lblBizAdd;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
