﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz6ScrapePOIs : DevExpress.XtraEditors.XtraUserControl
    {
        string TASKNAME;
        BackgroundWorker bw = new BackgroundWorker();

        public PanelWiz6ScrapePOIs()
        {
            InitializeComponent();

            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;

            bw.DoWork += bw_DoWork;
            bw.ProgressChanged += bw_ProgressChanged;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
            if (TASKNAME.Equals("GETPOI")){

                //pnlPOICardsHolder.Controls.Clear();
                EliminateProgressBar();

                if (!msg.Equals("OK")) SetNoPOI();
                if (msg.Equals("OK")) CreatePOICards();

                USER_WISH = "";
                btnShowMeWhatsAvailable.Text = "&Show Me What's Available";

                tmrCardCreator.Enabled = false;

                KillBrowser();

            }
            if (e.Cancelled == true | e.Error != null) return;

        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //Me.Text = e.ProgressPercentage.ToString() & "% completed"
        }

        private void EliminateProgressBar()
        {
            for (int x = 0; x < pnlPOICardsHolder.Controls.Count; x++)
            {
                if (pnlPOICardsHolder.Controls[x] is DevExpress.XtraWaitForm.ProgressPanel)
                {
                    pnlPOICardsHolder.Controls.RemoveAt(x);
                }
            }
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker worker = (BackgroundWorker)sender;

                if (bw.CancellationPending == true) e.Cancel = true;
                if (TASKNAME.Equals("GETPOI")) GETPOI();
            }
            catch
            {
                if (bw.WorkerSupportsCancellation == true)
                {
                    bw.CancelAsync();
                }
            }
        }

        List<string> poi_res_titles = new List<string>();
        List<string> poi_res_link = new List<string>();
        List<string> poi_res_address = new List<string>();
        List<Boolean> poi_res_shown = new List<Boolean>();

        string msg = "";
        private string GETPOI()
        {
            msg = "OK";

            file_content = "";

            Boolean ifchrome = InitiateChrome();
            if (ifchrome == false) return "Chrome was not initiated properly!";

            string stmt = "SELECT GMBCountryID FROM tbl_projects WHERE RowID = "+ Globals.VAR_PROJECT_ID.ToString() + ";";
            string Country_ID = clsSQLiteCRUD.GetACellVal(stmt);

            stmt = "SELECT IFNULL(GMBCity, '') AS GMBCity, "
                + "(SELECT nicename FROM tbl_countries WHERE tbl_countries.row_id = tbl_projects.GMBCountryID) AS GMBCountry FROM tbl_projects "
                + "WHERE tbl_projects.RowID = "+ Globals.VAR_PROJECT_ID.ToString();
            DataTable dt = clsSQLiteCRUD.GiveADataTable(stmt);

            string gmb_city = dt == null ? "" :(dt.Rows.Count==0?"":(dt.Rows[0]["GMBCity"].ToString()));
            string gmb_country = dt == null ? "" : (dt.Rows.Count == 0 ? "" : (dt.Rows[0]["GMBCountry"].ToString()));

            if (gmb_city.Equals("")) return "GMB City is not found!";
            string search_param = WebUtility.UrlEncode((gmb_city + " " + gmb_country).Trim());

            string init_url = "https://www.google.com/search?q="+ search_param + "+points+of+interest&npsic=0&rflfq=1&rlha=0";
            MY_HEADLESS_DRIVER.Navigate().GoToUrl(init_url);

            int TotalWait = 0;
            string WHOLEVAL = "";
            //wait to get source
            while (!WHOLEVAL.Contains("</html>") && TotalWait < 200)
            {
                WHOLEVAL = MY_HEADLESS_DRIVER.PageSource;
                System.Threading.Thread.Sleep(50);
                if (USER_WISH.Equals("STOP")) break;
                TotalWait++;
            }
            if (USER_WISH.Equals("STOP")) return "Scraping has been stopped forcibly.";

            //Search for More places button, this is the initial search
            List<IWebElement> MorePlacesMarkers = new List<IWebElement>();
                
            //find more places button
            string more_xpath_1 = "//*[@id=\"ZpxfC\"]/div/div[2]/a";
            string more_xpath_2 = "//*[@id=\"rso\"]/div[1]/div/div/div[2]/div/div[4]/div[2]/div/div/a";

            MorePlacesMarkers.AddRange(MY_HEADLESS_DRIVER.FindElements(By.XPath(more_xpath_1)));
            if (MorePlacesMarkers.Count <= 0) MorePlacesMarkers.AddRange(MY_HEADLESS_DRIVER.FindElements(By.XPath(more_xpath_2)));
            if (MorePlacesMarkers.Count<=0) return "No POI Found!";

            //Click on more places
            string more_places_link = MorePlacesMarkers[0].GetAttribute("href");
            //MorePlacesMarkers[0].Click();
            MY_HEADLESS_DRIVER.Navigate().GoToUrl(more_places_link);

            //Result page
            TotalWait = 0;
            string Cur_Source = MY_HEADLESS_DRIVER.PageSource;
            while (Globals.INSTANCE_COUNT(MY_HEADLESS_DRIVER.PageSource, "<li class=\"zb2Jbd") <=0 && TotalWait < 100)
            {
                System.Threading.Thread.Sleep(50);
                TotalWait++;
            }

            //Count results
            WHOLEVAL = MY_HEADLESS_DRIVER.PageSource;
            int TotalPOI = Globals.INSTANCE_COUNT(WHOLEVAL, "<li class=\"zb2Jbd");
            if (TotalPOI <= 0) {
                TotalPOI = Globals.INSTANCE_COUNT(WHOLEVAL, "<div class=\"VkpGBb\">");
            }

            if (TotalPOI <= 0) return "No POI Found!";
            
            //Check how many requred
            stmt = "SELECT IFNULL(HowManyGooglePOI, -1) AS HowManyGooglePOI FROM tbl_projects WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
            int qty_to_scrape = Int16.Parse(clsSQLiteCRUD.GetACellVal(stmt));
            int qty_scrape = qty_to_scrape < 0? TotalPOI : qty_to_scrape;
            qty_scrape = qty_scrape > TotalPOI ? TotalPOI : qty_scrape;
            
            poi_res_titles = new List<string>();
            poi_res_link = new List<string>();
            poi_res_address = new List<string>();

            for (int x = 0; x< qty_scrape; x++)
            {

                if (USER_WISH.Equals("STOP")) break;

                string one_card_xpath = "//*[@id=\"ow5\"]/div[3]/div[2]/div[2]/div/ol/li["+(x+1).ToString()+"]";
                List<IWebElement> card_elems = new List<IWebElement>();
                card_elems.AddRange(MY_HEADLESS_DRIVER.FindElements(By.XPath(one_card_xpath)));

             
                //Next if no card element
                if (card_elems.Count <= 0) continue;

                //Keep the current page source for later comparison
                WHOLEVAL = MY_HEADLESS_DRIVER.PageSource;

                //Hit the card for new source
                //IWebElement one_card = card_elems[0];
                ((IJavaScriptExecutor)MY_HEADLESS_DRIVER).ExecuteScript("arguments[0].scrollIntoView();", card_elems[0]);
                card_elems[0].Click();


                //IJavaScriptExecutor js = (IJavaScriptExecutor)MY_HEADLESS_DRIVER;
                //js.executeScript("document.querySelector('attributeValue').value='new value'");

               
                //Wait
                TotalWait = 0;
                string N_WHOLEVAL = "";
                string address_marker = "<span class=\"LrzXr\">";
                while ((N_WHOLEVAL.Equals("") || !N_WHOLEVAL.Contains(address_marker)) && TotalWait < 100)
                {
                    System.Threading.Thread.Sleep(50);
                    N_WHOLEVAL = MY_HEADLESS_DRIVER.PageSource;
                    TotalWait++;
                }

                if (!N_WHOLEVAL.Contains(address_marker)) continue;

                //necessary variables
                string POI_TITLE = "";
                string POI_ADDRESS = "";
                int spos = 0;
                int epos = 0;
                WHOLEVAL = N_WHOLEVAL;

                //Get Title & Address
                string one_title_xpath = "//*[@id=\"akp_uid_0\"]/div/div/div/div/div/div[1]/div/div[1]/div[2]/div[1]/div/div[1]/div/div[1]/div/span";
                List<IWebElement> title_elems = new List<IWebElement>();
                title_elems.AddRange(MY_HEADLESS_DRIVER.FindElements(By.XPath(one_title_xpath)));
                if (title_elems.Count > 0)
                {
                    POI_TITLE = title_elems[0].Text;
                    POI_TITLE = POI_TITLE.Replace("&amp;", "&");
                }

                spos = WHOLEVAL.IndexOf(address_marker);
                if (spos > 0)
                {
                    spos += address_marker.Length;
                    epos = WHOLEVAL.IndexOf("</span>", spos);
                    POI_ADDRESS = (spos > 0 && epos > 0 && epos > spos ? WHOLEVAL.Substring(spos, epos - spos) : "");
                    POI_ADDRESS = POI_ADDRESS.Replace("&amp;", "&");
                }

                if (POI_TITLE.Equals("") || POI_ADDRESS.Equals("")) continue;

                //Check if already in db under this project
                string sql = "SELECT Count(*) AS CNT FROM tbl_poi WHERE "
                    + "ProjectID = " + Globals.VAR_PROJECT_ID.ToString() + " "
                    + "AND POIAddress = '" + POI_ADDRESS.Replace("\"", "'").Replace("'", "''") + "';";
                string cnt = clsSQLiteCRUD.GetACellVal(sql);
                if (Int16.Parse(cnt) > 0) continue;


                //Website
                string link_marker = "<a class=\"CL9Uqc ab_button\" href=\"";
                string POI_LINK = "";
                if (WHOLEVAL.Contains(link_marker))
                {
                    spos = WHOLEVAL.IndexOf(link_marker);
                    if (spos>0)
                    {
                        spos+= link_marker.Length;
                        epos = WHOLEVAL.IndexOf("\"", spos);
                        POI_LINK = (spos > 0 && epos > 0 && epos > spos ? WHOLEVAL.Substring(spos, epos - spos) : "");
                        POI_LINK = POI_LINK.Replace("&amp;", "&");
                    }
                }

                //get lat-long

                
                //save to db
                sql = "INSERT INTO tbl_poi (ProjectID, IsGoogle, POITitle, POILink, POIAddress) VALUES ("
                    + Globals.VAR_PROJECT_ID.ToString() + ", 1, '" 
                    + POI_TITLE.Replace("\"", "'").Replace("'", "''") + "', " 
                    + (POI_LINK.Equals("")?"NULL":"'"+ POI_LINK.Replace("\"", "'").Replace("'", "''")+"'") + ", '" 
                    + POI_ADDRESS.Replace("\"", "'").Replace("'", "''") + "');";

                int retval = clsSQLiteCRUD.INSERT_ROWS(sql, false);

                if (retval > 0)
                {
                    poi_res_titles.Add(POI_TITLE);
                    poi_res_link.Add(POI_LINK);
                    poi_res_address.Add(POI_ADDRESS);
                    poi_res_shown.Add(false);

                    file_content += (file_content.Equals("")?"":Environment.NewLine)
                        + Country_ID + "|" 
                        + gmb_city + "|"
                        + "1|"
                        + POI_TITLE + "|" 
                        + POI_LINK + "|" 
                        + POI_ADDRESS;

                }
            }

            //Create the text file
            String UsersDBDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Local\\NMM";
            string text_file_name = Globals.SetAName() + ".txt";
            string text_file_path = UsersDBDir + "\\" + text_file_name;
            Globals.WRITE_APPEND_TEXT(text_file_path, file_content);


            //Read file data
            FileStream fs = new FileStream(text_file_path, FileMode.Open, FileAccess.Read);
            byte[] data = new byte[fs.Length];
            fs.Read(data, 0, data.Length);
            fs.Close();

            //Generate post objects
            Dictionary<string, object> postParameters = new Dictionary<string, object>();
            postParameters.Add("key", "22(KJJLKJJL233!");
            postParameters.Add("file", new FormUpload.FileParameter(data, text_file_path, "text/plain"));

            //Create request and receive response
            string postURL = "https://gmbtoolbox.com/neighborhood/api/push/";
            string userAgent = Globals.USER_AGENT;
            HttpWebResponse webResponse = FormUpload.MultipartFormDataPost(postURL, userAgent, postParameters);

            //Process response
            StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
            string fullResponse = responseReader.ReadToEnd();
            webResponse.Close();
            //Console.Write(fullResponse);

            //Try delete the file
            if (fullResponse.Contains("Response_Code")) //whatever code!!
            {
                try { System.IO.File.Delete(text_file_path); } catch (Exception ex) { }
            }

            //Console.WriteLine(fullResponse);
            
            if (USER_WISH.Equals("STOP")) return "Scraping has been stopped forcibly.";

            return msg;
        }

        private void SurfALink(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.HyperlinkLabelControl poi_title = (DevExpress.XtraEditors.HyperlinkLabelControl)sender;
            string v = poi_title.Tag.ToString();
            if (!v.ToLower().Contains("https") && !v.ToLower().Contains("www")) return;
            System.Diagnostics.Process.Start(v);
        }

        string file_content = "";
        private void CreatePOICards()
        {

            if (poi_res_titles.Count == 0) return;

            //render results
            for (int x =0; x< poi_res_titles.Count; x++)
            {
                if (poi_res_shown[x] == true) continue;

                //Render on UI
                PanelModelPOI pmpoi = new PanelModelPOI();
                DevExpress.XtraEditors.HyperlinkLabelControl lblPOICardTitle
                   = (DevExpress.XtraEditors.HyperlinkLabelControl)pmpoi.Controls["lblPOICardTitle"];
                lblPOICardTitle.Text = "<href=" + poi_res_link[x] + ">" + poi_res_titles[x] + "</href>";
                lblPOICardTitle.Tag = poi_res_link[x];


                DevExpress.XtraEditors.LabelControl lblPOIAddress
                        = (DevExpress.XtraEditors.LabelControl)pmpoi.Controls["lblPOIAddress"];
                lblPOIAddress.Text = (poi_res_address[x].Equals("")?"(Address not found)": poi_res_address[x]);

                DevExpress.XtraEditors.CheckEdit chkUseThisPOI
                   = (DevExpress.XtraEditors.CheckEdit)pmpoi.Controls["chkUseThisPOI"];
                chkUseThisPOI.Tag = poi_res_link[x];
                chkUseThisPOI.Checked = true;

                //Add Click listener
                lblPOICardTitle.Click += new EventHandler(SurfALink);
                chkUseThisPOI.Click += new EventHandler(UsePOIOrNot_Click);

                if (pnlPOICardsHolder.Controls.Count > 0)
                {
                    for (int d = pnlPOICardsHolder.Controls.Count - 1; d >= 0; d--)
                    {
                        if (pnlPOICardsHolder.Controls[d] is PanelModelPOI)
                        {
                            int curTop = pnlPOICardsHolder.Controls[d].Location.Y;
                            int newTop = curTop + 5 + pnlPOICardsHolder.Controls[d].Height;
                            int cWidth = pnlPOICardsHolder.Controls[d].Width;
                            int cHeight = pnlPOICardsHolder.Controls[d].Height;
                            pnlPOICardsHolder.Controls[d].SetBounds(0, newTop, cWidth, cHeight);
                        }
                    }
                }
                
                pnlPOICardsHolder.Controls.Add(pmpoi);
                //int yPos = (x * pmpoi.Height) + (x * 5);
                pmpoi.SetBounds(0, 0, pmpoi.Width, pmpoi.Height);

                poi_res_shown[x] = true;
            }

            if (poi_res_titles.Count > 6)
            {
                pnlPOICardsHolder.VerticalScroll.Visible = true;
            }
            else
            {
                pnlPOICardsHolder.VerticalScroll.Visible = false;
            }
            
            btnCheckUncheck.Visible = true;
            btnDeleteSelected.Visible = true;

        }

        private void UsePOIOrNot_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit CallCheckEdit = (DevExpress.XtraEditors.CheckEdit)sender;
            Boolean ToUse = CallCheckEdit.Checked;

            for (int x = 0; x < pnlPOICardsHolder.Controls.Count; x++)
            {
                DevExpress.XtraEditors.XtraUserControl OneCard = (DevExpress.XtraEditors.XtraUserControl)pnlPOICardsHolder.Controls[x];
                DevExpress.XtraEditors.CheckEdit OneCheckEdit = (DevExpress.XtraEditors.CheckEdit)OneCard.Controls["chkUseThisPOI"];

                if (CallCheckEdit.Tag.ToString().Equals(OneCheckEdit.Tag.ToString()))
                {
                    int cond = 1;
                    if (ToUse == false) cond = 0;
                    string stmt = "UPDATE tbl_poi SET ToUse = " + cond.ToString() + " WHERE ProjectID = " + Globals.VAR_PROJECT_ID.ToString();
                    int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                }
            }
        }

        private void Panel6ScrapePOIs_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            DevExpress.XtraEditors.XtraForm.CheckForIllegalCrossThreadCalls = false;

            this.btnCheckUncheck.ToolTipTitle = Globals.AppName;
            this.btnDeleteSelected.ToolTipTitle = Globals.AppName;

            //lblScrapingCaution.Text = "Pressing this button will start the program scraping. Please leave it alone until it finishes";
        }

        string USER_WISH = "";
        private void btnShowMeWhatsAvailable_Click(object sender, EventArgs e)
        {
            if (bw.IsBusy || USER_WISH.Equals("SCRAPE"))
            {
                DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
                args.Caption = Globals.AppName;
                args.Text = Environment.NewLine + Environment.NewLine + "Are you sure to terminate search?"
                    + Environment.NewLine + Environment.NewLine;                    
                args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.Yes, System.Windows.Forms.DialogResult.No };
                //args.Icon = System.Drawing.SystemIcons.Question;
                if (DevExpress.XtraEditors.XtraMessageBox.Show(args) == System.Windows.Forms.DialogResult.No) return;

                //if (bw.WorkerSupportsCancellation) bw.CancelAsync();
                //KillBrowser();
                //btnShowMeWhatsAvailable.Text = "Show Me What's Available";
                //EliminateProgressBar();
                //tmrCardCreator.Enabled = false;
                USER_WISH = "STOP";
                return;
            }

            string chrome_binary_path = Globals.CHROME_BINARY_PATH();
            if (chrome_binary_path.Equals("")) return;
            if (!File.Exists(chrome_binary_path)) return;


            for (int x =0; x<pnlPOICardsHolder.Controls.Count; x++)
            {
                if (pnlPOICardsHolder.Controls[x] is PanelPOINoCards) pnlPOICardsHolder.Controls.RemoveAt(x);
            }

            DevExpress.XtraWaitForm.ProgressPanel progBusy = new DevExpress.XtraWaitForm.ProgressPanel();
            progBusy.Name = "progBusy";
            pnlPOICardsHolder.Controls.Add(progBusy);
            progBusy.SetBounds(200, 100, progBusy.Width, progBusy.Height);

            
            TASKNAME = "GETPOI";
            USER_WISH = "SCRAPE";
            btnShowMeWhatsAvailable.Text = "&Stop Scraping";
            tmrCardCreator.Enabled = true;
            bw.RunWorkerAsync();
        }

        IWebDriver MY_HEADLESS_DRIVER;
        public Boolean InitiateChrome()
        {
            //Cautions target machines
            //Go to Internet Options > Uncheck "Automatically Detect Settings"

            Boolean IfSelChromeUP = false;

            String UsersDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Local\\NMM";
            ChromeDriverService driverService = ChromeDriverService.CreateDefaultService(UsersDataDir);

            driverService.HideCommandPromptWindow = true;

            var options = new ChromeOptions();
            options.BinaryLocation = @Globals.CHROME_BINARY_PATH();
            if (!chkKeepChromeVisisble.Checked) options.AddArgument("--headless");

            options.AddUserProfilePreference("download.default_directory", UsersDataDir);
            options.AddUserProfilePreference("disable-popup-blocking", "true");
            options.AddUserProfilePreference("download.prompt_for_download", false);

            options.AddUserProfilePreference("profile.default_content_setting_values.images", 2);
            options.AddArgument("--blink-settings=imagesEnabled=false");

            options.AcceptInsecureCertificates = true;
            options.PageLoadStrategy = PageLoadStrategy.Normal;
            
            options.AddArguments("disable-infobars");
            options.AddArgument("--user-agent=" + Globals.USER_AGENT);
            options.AddArgument("--enable-javascript");
            //options.AddArguments(@"--incognito");
            options.AddArguments("--no-sandbox");
            //options.AddArguments("--lang=en-US");
            options.AddArguments("--lang=en-GB");
            options.AddArguments("--enable-logging");
            if (chkKeepChromeVisisble.Checked)  options.AddArgument("--start-maximized");
            
                        
            MY_HEADLESS_DRIVER = new ChromeDriver(driverService, options);

            MY_HEADLESS_DRIVER.Manage().Timeouts().ImplicitWait.Add(TimeSpan.FromSeconds(30));
            MY_HEADLESS_DRIVER.Manage().Timeouts().PageLoad.Add(TimeSpan.FromSeconds(30));
            MY_HEADLESS_DRIVER.Manage().Timeouts().AsynchronousJavaScript.Add(TimeSpan.FromSeconds(30));

            /*using (MY_HEADLESS_DRIVER)
            {
                IWait<IWebDriver> wait = new WebDriverWait(MY_HEADLESS_DRIVER, TimeSpan.FromSeconds(30.0));
                wait.Until(driver1 => ((IJavaScriptExecutor)MY_HEADLESS_DRIVER).ExecuteScript("return document.readyState").Equals("complete"));
            }*/

            IfSelChromeUP = true;
            return IfSelChromeUP;
        }

        /*public Boolean InitiatePhantom()
        {

            Boolean IfSelChromeUP = false;

            String UsersDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Local\\NMM";
            
            PhantomJSDriverService driverService = PhantomJSDriverService.CreateDefaultService(UsersDataDir);
            driverService.HideCommandPromptWindow = true;
            driverService.IgnoreSslErrors = true;
            driverService.LoadImages = true;
            driverService.ProxyType = "none";

            var options = new PhantomJSOptions();
            String USER_AGENT = Globals.USER_AGENT;
            options.AddAdditionalCapability("phantomjs.page.settings.userAgent", USER_AGENT);
            //Initiate Driver
            MY_HEADLESS_DRIVER = new PhantomJSDriver(driverService, options);
            MY_HEADLESS_DRIVER.Manage().Window.Size = new System.Drawing.Size(1440, 2500);

            IWait<IWebDriver> wait = new OpenQA.Selenium.Support.UI.WebDriverWait(MY_HEADLESS_DRIVER, TimeSpan.FromSeconds(30.0));
            wait.Until(driver1 => ((IJavaScriptExecutor)MY_HEADLESS_DRIVER).ExecuteScript("return document.readyState").Equals("complete"));
            
            return IfSelChromeUP;
        }*/

        public void KillBrowser()
        {
            try
            {
                MY_HEADLESS_DRIVER.Close();
                MY_HEADLESS_DRIVER.Quit();
                MY_HEADLESS_DRIVER.Dispose();
                foreach (var process in Process.GetProcessesByName("chromedriver")) process.Kill();
            }
            catch(Exception ex)
            {
                //do nothing
            }
            
        }

        private void btnDeleteSelected_Click(object sender, EventArgs e)
        {
            
            if (bw.IsBusy) return;

            int total_to_del = 0;
            string del_cond = "";
            for (int x = pnlPOICardsHolder.Controls.Count-1; x>=0; x--)
            {
                if (pnlPOICardsHolder.Controls[x] is PanelModelPOI)
                {
                    PanelModelPOI curModel = (PanelModelPOI)pnlPOICardsHolder.Controls[x];
                    DevExpress.XtraEditors.CheckEdit chksel = (DevExpress.XtraEditors.CheckEdit) curModel.Controls["chkSelect"];

                    if (chksel.Checked)
                    {
                        DevExpress.XtraEditors.LabelControl lblAddress = (DevExpress.XtraEditors.LabelControl)curModel.Controls["lblPOIAddress"];
                        string compare_with = lblAddress.Text.Replace("\"", "'");
                        compare_with = lblAddress.Text.Replace("'", "''");
                        del_cond += (del_cond.Equals("")?"":" OR ") +  "POIAddress = '" + compare_with + "'";

                        total_to_del++;
                    }
                }
            }

            if (total_to_del==0) return;

            //Get Permission
            DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
            args.Caption = Globals.AppName;
            args.Text = Environment.NewLine + Environment.NewLine 
                + "This will delete " + total_to_del.ToString() + " POI permanently from the current project." 
                + Environment.NewLine + Environment.NewLine
                + "Are you sure you want to proceed?" + Environment.NewLine + Environment.NewLine; 
            args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.Yes, System.Windows.Forms.DialogResult.No };
            if (DevExpress.XtraEditors.XtraMessageBox.Show(args) == System.Windows.Forms.DialogResult.No) return;

            //Remove from UI
            for (int x = pnlPOICardsHolder.Controls.Count-1; x>=0; x--)
            {
                if (pnlPOICardsHolder.Controls[x] is PanelModelPOI)
                {
                    PanelModelPOI curModel = (PanelModelPOI)pnlPOICardsHolder.Controls[x];
                    DevExpress.XtraEditors.CheckEdit chksel = (DevExpress.XtraEditors.CheckEdit)curModel.Controls["chkSelect"];
                    if (chksel.Checked)
                    {
                        pnlPOICardsHolder.Controls.RemoveAt(x);
                    }
                }
            }

            //Delete from DB
            string sql = "DELETE FROM tbl_poi WHERE ProjectID = " + Globals.VAR_PROJECT_ID.ToString() + " AND IsGoogle = 1 AND ("+ del_cond + ");";
            int res = clsSQLiteCRUD.UPDATE_ROWS(sql);

            int cards_remain = 0;
            for (int x = 0; x < pnlPOICardsHolder.Controls.Count; x++)
            {
                if (pnlPOICardsHolder.Controls[x] is PanelModelPOI) cards_remain++;
            }

            //Show Null card if no more
            if (cards_remain == 0)
            {
                SetNoPOI();
            }
            else
            {
                ResetLoationOfCards();
                btnCheckUncheck.Visible = true;
                btnDeleteSelected.Visible = true;
            }
        }

        private void SetNoPOI()
        {
            btnCheckUncheck.Visible = false;
            btnDeleteSelected.Visible = false;
            ShowNoPOICard();
        }

        private void ShowNoPOICard()
        {
            PanelPOINoCards noPOI = new PanelPOINoCards();
            pnlPOICardsHolder.Controls.Add(noPOI);
            noPOI.SetBounds(0, 0, noPOI.Width, noPOI.Height);
            pnlPOICardsHolder.VerticalScroll.Visible = false;
        }

        private void ResetLoationOfCards()

        {
            if (pnlPOICardsHolder.Controls.Count == 0)
            {
                ShowNoPOICard();
                return;
            }

            for (int x = 0; x < pnlPOICardsHolder.Controls.Count - 1; x++)
            {
                if (pnlPOICardsHolder.Controls[x] is PanelModelPOI)
                {
                    PanelModelPOI curModel = (PanelModelPOI)pnlPOICardsHolder.Controls[x];
                    int yPos = (x * curModel.Height) + (x * 5);
                    curModel.SetBounds(0, yPos, curModel.Width, curModel.Height);
                }
            }
        }

        private void btnCheckUncheck_Click(object sender, EventArgs e)
        {

            if (bw.IsBusy) return;

            if (pnlPOICardsHolder.Controls.Count == 0) return;

            int cnt = 0;
            for (int x = 0; x < pnlPOICardsHolder.Controls.Count; x++)
            {
                if (pnlPOICardsHolder.Controls[x] is PanelModelPOI)
                {
                    PanelModelPOI curModel = (PanelModelPOI)pnlPOICardsHolder.Controls[x];
                    DevExpress.XtraEditors.CheckEdit chkSel = (DevExpress.XtraEditors.CheckEdit)curModel.Controls["chkSelect"];
                    chkSel.Checked = btnCheckUncheck.Tag.ToString().Equals("Unchecked") ?true: false;
                    chkSel.CheckState = btnCheckUncheck.Tag.ToString().Equals("Unchecked") ? System.Windows.Forms.CheckState.Checked : System.Windows.Forms.CheckState.Unchecked;
                    cnt++;
                }
            }

            if (cnt > 0)
            {
                btnCheckUncheck.Tag = btnCheckUncheck.Tag.ToString().Equals("Unchecked") ? "Checked" : "Unchecked";
            }


        }

        private void tmrCardCreator_Tick(object sender, EventArgs e)
        {
            CreatePOICards();
        }
    }
}
