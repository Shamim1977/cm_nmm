﻿namespace NeighborhoodMapsMaker
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions10 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions11 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions12 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.picAppLogo = new System.Windows.Forms.PictureBox();
            this.lblChadConnects = new DevExpress.XtraEditors.LabelControl();
            this.lblAppName = new DevExpress.XtraEditors.LabelControl();
            this.lblAppVer = new DevExpress.XtraEditors.LabelControl();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picAppLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.BackColor = System.Drawing.Color.White;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.BorderColor = System.Drawing.Color.Silver;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseBackColor = true;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseBorderColor = true;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions10.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions10.Image")));
            windowsUIButtonImageOptions11.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions11.Image")));
            windowsUIButtonImageOptions12.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions12.Image")));
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Wizard", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, true, true, "Wizard", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Settings", true, windowsUIButtonImageOptions10, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Settings", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Support", true, windowsUIButtonImageOptions11, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Support", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Training", true, windowsUIButtonImageOptions12, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Training", -1, false)});
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(1118, 196);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(96, 284);
            this.windowsUIButtonPanel1.TabIndex = 2;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.windowsUIButtonPanel1_ButtonClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "appicon.png");
            this.imageList1.Images.SetKeyName(1, "settings24.png");
            this.imageList1.Images.SetKeyName(2, "support24.png");
            this.imageList1.Images.SetKeyName(3, "training24.png");
            this.imageList1.Images.SetKeyName(4, "wizard24.png");
            // 
            // picAppLogo
            // 
            this.picAppLogo.Image = ((System.Drawing.Image)(resources.GetObject("picAppLogo.Image")));
            this.picAppLogo.Location = new System.Drawing.Point(23, 21);
            this.picAppLogo.Name = "picAppLogo";
            this.picAppLogo.Size = new System.Drawing.Size(50, 50);
            this.picAppLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picAppLogo.TabIndex = 3;
            this.picAppLogo.TabStop = false;
            this.toolTipController1.SetTitle(this.picAppLogo, "Neighborhood Maps Maker");
            this.toolTipController1.SetToolTip(this.picAppLogo, "Double click to quit");
            this.picAppLogo.DoubleClick += new System.EventHandler(this.picAppLogo_DoubleClick);
            // 
            // lblChadConnects
            // 
            this.lblChadConnects.Appearance.Font = new System.Drawing.Font("Lucida Handwriting", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChadConnects.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblChadConnects.Appearance.Options.UseFont = true;
            this.lblChadConnects.Appearance.Options.UseForeColor = true;
            this.lblChadConnects.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblChadConnects.Location = new System.Drawing.Point(1046, 606);
            this.lblChadConnects.Name = "lblChadConnects";
            this.lblChadConnects.Size = new System.Drawing.Size(145, 21);
            this.lblChadConnects.TabIndex = 4;
            this.lblChadConnects.Tag = "http://www.chadconnects.com/";
            this.lblChadConnects.Text = "#ChadConnects";
            this.lblChadConnects.ToolTip = "Click to Visit Developer Site";
            this.lblChadConnects.ToolTipTitle = "Neighborhood Maps Maker";
            this.lblChadConnects.Click += new System.EventHandler(this.lblChadConnects_Click);
            // 
            // lblAppName
            // 
            this.lblAppName.Appearance.Font = new System.Drawing.Font("Georgia", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppName.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblAppName.Appearance.Options.UseFont = true;
            this.lblAppName.Appearance.Options.UseForeColor = true;
            this.lblAppName.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblAppName.Location = new System.Drawing.Point(79, 39);
            this.lblAppName.Name = "lblAppName";
            this.lblAppName.Size = new System.Drawing.Size(208, 20);
            this.lblAppName.TabIndex = 5;
            this.lblAppName.Tag = "";
            this.lblAppName.Text = "Neighborhood Maps Maker";
            // 
            // lblAppVer
            // 
            this.lblAppVer.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppVer.Appearance.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblAppVer.Appearance.Options.UseFont = true;
            this.lblAppVer.Appearance.Options.UseForeColor = true;
            this.lblAppVer.Location = new System.Drawing.Point(293, 28);
            this.lblAppVer.Name = "lblAppVer";
            this.lblAppVer.Size = new System.Drawing.Size(39, 16);
            this.lblAppVer.TabIndex = 6;
            this.lblAppVer.Text = "v 1.1.2";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1247, 661);
            this.Controls.Add(this.lblAppVer);
            this.Controls.Add(this.lblAppName);
            this.Controls.Add(this.lblChadConnects);
            this.Controls.Add(this.picAppLogo);
            this.Controls.Add(this.windowsUIButtonPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Neighborhood Maps Maker";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Click += new System.EventHandler(this.frmMain_Click);
            ((System.ComponentModel.ISupportInitialize)(this.picAppLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.PictureBox picAppLogo;
        private DevExpress.XtraEditors.LabelControl lblChadConnects;
        private DevExpress.XtraEditors.LabelControl lblAppName;
        private DevExpress.XtraEditors.LabelControl lblAppVer;
        private DevExpress.Utils.ToolTipController toolTipController1;
    }
}