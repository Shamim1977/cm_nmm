﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz12GoogleAuth
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz12GoogleAuth));
            this.lblGoogleAuthEmail = new DevExpress.XtraEditors.LabelControl();
            this.txtGoogleAuthEmail = new DevExpress.XtraEditors.TextEdit();
            this.lblGoogleAuthHeader = new DevExpress.XtraEditors.LabelControl();
            this.lblGoogleAuthPass = new DevExpress.XtraEditors.LabelControl();
            this.txtGoogleAuthPass = new DevExpress.XtraEditors.TextEdit();
            this.lblGoogleAuthRecoEmail = new DevExpress.XtraEditors.LabelControl();
            this.txtGoogleAuthRecoEmail = new DevExpress.XtraEditors.TextEdit();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.chkRememberMe = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnHelp_P2_1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnShowHidePass = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoogleAuthEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoogleAuthPass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoogleAuthRecoEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRememberMe.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblGoogleAuthEmail
            // 
            this.lblGoogleAuthEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoogleAuthEmail.Appearance.Options.UseFont = true;
            this.lblGoogleAuthEmail.Location = new System.Drawing.Point(40, 60);
            this.lblGoogleAuthEmail.Name = "lblGoogleAuthEmail";
            this.lblGoogleAuthEmail.Size = new System.Drawing.Size(125, 18);
            this.lblGoogleAuthEmail.TabIndex = 16;
            this.lblGoogleAuthEmail.Text = "Google Login Email:";
            // 
            // txtGoogleAuthEmail
            // 
            this.txtGoogleAuthEmail.Location = new System.Drawing.Point(171, 60);
            this.txtGoogleAuthEmail.Name = "txtGoogleAuthEmail";
            this.txtGoogleAuthEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGoogleAuthEmail.Properties.Appearance.Options.UseFont = true;
            this.txtGoogleAuthEmail.Size = new System.Drawing.Size(379, 24);
            this.txtGoogleAuthEmail.TabIndex = 0;
            this.txtGoogleAuthEmail.Leave += new System.EventHandler(this.txtGoogleAuthEmail_Leave);
            // 
            // lblGoogleAuthHeader
            // 
            this.lblGoogleAuthHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoogleAuthHeader.Appearance.Options.UseFont = true;
            this.lblGoogleAuthHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGoogleAuthHeader.Location = new System.Drawing.Point(40, 6);
            this.lblGoogleAuthHeader.Name = "lblGoogleAuthHeader";
            this.lblGoogleAuthHeader.Size = new System.Drawing.Size(216, 23);
            this.lblGoogleAuthHeader.TabIndex = 17;
            this.lblGoogleAuthHeader.Text = "Google Authentication";
            // 
            // lblGoogleAuthPass
            // 
            this.lblGoogleAuthPass.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoogleAuthPass.Appearance.Options.UseFont = true;
            this.lblGoogleAuthPass.Location = new System.Drawing.Point(40, 104);
            this.lblGoogleAuthPass.Name = "lblGoogleAuthPass";
            this.lblGoogleAuthPass.Size = new System.Drawing.Size(66, 18);
            this.lblGoogleAuthPass.TabIndex = 20;
            this.lblGoogleAuthPass.Text = "Password:";
            // 
            // txtGoogleAuthPass
            // 
            this.txtGoogleAuthPass.Location = new System.Drawing.Point(171, 104);
            this.txtGoogleAuthPass.Name = "txtGoogleAuthPass";
            this.txtGoogleAuthPass.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGoogleAuthPass.Properties.Appearance.Options.UseFont = true;
            this.txtGoogleAuthPass.Properties.PasswordChar = '*';
            this.txtGoogleAuthPass.Size = new System.Drawing.Size(341, 24);
            this.txtGoogleAuthPass.TabIndex = 1;
            // 
            // lblGoogleAuthRecoEmail
            // 
            this.lblGoogleAuthRecoEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoogleAuthRecoEmail.Appearance.Options.UseFont = true;
            this.lblGoogleAuthRecoEmail.Location = new System.Drawing.Point(40, 148);
            this.lblGoogleAuthRecoEmail.Name = "lblGoogleAuthRecoEmail";
            this.lblGoogleAuthRecoEmail.Size = new System.Drawing.Size(104, 18);
            this.lblGoogleAuthRecoEmail.TabIndex = 23;
            this.lblGoogleAuthRecoEmail.Text = "Recovery Email:";
            // 
            // txtGoogleAuthRecoEmail
            // 
            this.txtGoogleAuthRecoEmail.Location = new System.Drawing.Point(171, 148);
            this.txtGoogleAuthRecoEmail.Name = "txtGoogleAuthRecoEmail";
            this.txtGoogleAuthRecoEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGoogleAuthRecoEmail.Properties.Appearance.Options.UseFont = true;
            this.txtGoogleAuthRecoEmail.Size = new System.Drawing.Size(379, 24);
            this.txtGoogleAuthRecoEmail.TabIndex = 2;
            this.txtGoogleAuthRecoEmail.Leave += new System.EventHandler(this.txtGoogleAuthRecoEmail_Leave);
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 43;
            // 
            // chkRememberMe
            // 
            this.chkRememberMe.Location = new System.Drawing.Point(171, 189);
            this.chkRememberMe.Name = "chkRememberMe";
            this.chkRememberMe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRememberMe.Properties.Appearance.Options.UseFont = true;
            this.chkRememberMe.Properties.Caption = "Remember me in this machine";
            this.chkRememberMe.Size = new System.Drawing.Size(264, 22);
            this.chkRememberMe.TabIndex = 45;
            this.chkRememberMe.EditValueChanged += new System.EventHandler(this.chkRememberMe_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(189, 210);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(387, 16);
            this.labelControl1.TabIndex = 46;
            this.labelControl1.Text = "(Credentials won\'t be saved in any DB but in this app\'s preferences)";
            // 
            // btnHelp_P2_1
            // 
            this.btnHelp_P2_1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp_P2_1.ImageOptions.Image")));
            this.btnHelp_P2_1.Location = new System.Drawing.Point(555, 60);
            this.btnHelp_P2_1.Name = "btnHelp_P2_1";
            this.btnHelp_P2_1.Size = new System.Drawing.Size(25, 24);
            this.btnHelp_P2_1.TabIndex = 48;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(556, 147);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(25, 24);
            this.simpleButton1.TabIndex = 49;
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(555, 104);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(25, 24);
            this.simpleButton2.TabIndex = 50;
            // 
            // btnShowHidePass
            // 
            this.btnShowHidePass.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnShowHidePass.ImageOptions.Image")));
            this.btnShowHidePass.Location = new System.Drawing.Point(518, 104);
            this.btnShowHidePass.Name = "btnShowHidePass";
            this.btnShowHidePass.Size = new System.Drawing.Size(32, 24);
            this.btnShowHidePass.TabIndex = 51;
            this.btnShowHidePass.Tag = "Show";
            this.btnShowHidePass.Click += new System.EventHandler(this.btnShowHidePass_Click);
            // 
            // PanelWiz12GoogleAuth
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnShowHidePass);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnHelp_P2_1);
            this.Controls.Add(this.chkRememberMe);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblGoogleAuthRecoEmail);
            this.Controls.Add(this.txtGoogleAuthRecoEmail);
            this.Controls.Add(this.lblGoogleAuthPass);
            this.Controls.Add(this.txtGoogleAuthPass);
            this.Controls.Add(this.lblGoogleAuthHeader);
            this.Controls.Add(this.lblGoogleAuthEmail);
            this.Controls.Add(this.txtGoogleAuthEmail);
            this.Controls.Add(this.labelControl1);
            this.Name = "PanelWiz12GoogleAuth";
            this.Size = new System.Drawing.Size(617, 244);
            this.Load += new System.EventHandler(this.Panel9GoogleLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtGoogleAuthEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoogleAuthPass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoogleAuthRecoEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRememberMe.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblGoogleAuthEmail;
        private DevExpress.XtraEditors.TextEdit txtGoogleAuthEmail;
        private DevExpress.XtraEditors.LabelControl lblGoogleAuthHeader;
        private DevExpress.XtraEditors.LabelControl lblGoogleAuthPass;
        private DevExpress.XtraEditors.TextEdit txtGoogleAuthPass;
        private DevExpress.XtraEditors.LabelControl lblGoogleAuthRecoEmail;
        private DevExpress.XtraEditors.TextEdit txtGoogleAuthRecoEmail;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraEditors.CheckEdit chkRememberMe;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnHelp_P2_1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton btnShowHidePass;
    }
}
