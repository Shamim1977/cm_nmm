﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz5ChoosePOIs
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz5ChoosePOIs));
            this.lblGooglePOIChooseWhichOnesHeader = new DevExpress.XtraEditors.LabelControl();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.chkUseUnused = new DevExpress.XtraEditors.CheckEdit();
            this.chkLetMeChoosePOI = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseUnused.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLetMeChoosePOI.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblGooglePOIChooseWhichOnesHeader
            // 
            this.lblGooglePOIChooseWhichOnesHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGooglePOIChooseWhichOnesHeader.Appearance.Options.UseFont = true;
            this.lblGooglePOIChooseWhichOnesHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGooglePOIChooseWhichOnesHeader.Location = new System.Drawing.Point(40, 6);
            this.lblGooglePOIChooseWhichOnesHeader.Name = "lblGooglePOIChooseWhichOnesHeader";
            this.lblGooglePOIChooseWhichOnesHeader.Size = new System.Drawing.Size(513, 23);
            this.lblGooglePOIChooseWhichOnesHeader.TabIndex = 16;
            this.lblGooglePOIChooseWhichOnesHeader.Text = "Google Places of Interest (POI)";
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 42;
            // 
            // chkUseUnused
            // 
            this.chkUseUnused.EditValue = true;
            this.chkUseUnused.Location = new System.Drawing.Point(40, 61);
            this.chkUseUnused.Name = "chkUseUnused";
            this.chkUseUnused.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUseUnused.Properties.Appearance.Options.UseFont = true;
            this.chkUseUnused.Properties.Caption = "Choose ones I haven\'t used yet";
            this.chkUseUnused.Size = new System.Drawing.Size(242, 22);
            this.chkUseUnused.TabIndex = 43;
            this.chkUseUnused.EditValueChanged += new System.EventHandler(this.chkUseUnused_EditValueChanged);
            // 
            // chkLetMeChoosePOI
            // 
            this.chkLetMeChoosePOI.Location = new System.Drawing.Point(40, 100);
            this.chkLetMeChoosePOI.Name = "chkLetMeChoosePOI";
            this.chkLetMeChoosePOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLetMeChoosePOI.Properties.Appearance.Options.UseFont = true;
            this.chkLetMeChoosePOI.Properties.Caption = "Let me choose which ones I want";
            this.chkLetMeChoosePOI.Size = new System.Drawing.Size(265, 22);
            this.chkLetMeChoosePOI.TabIndex = 44;
            this.chkLetMeChoosePOI.EditValueChanged += new System.EventHandler(this.chkLetMeChoosePOI_EditValueChanged);
            // 
            // PanelWiz5ChoosePOIs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkLetMeChoosePOI);
            this.Controls.Add(this.chkUseUnused);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblGooglePOIChooseWhichOnesHeader);
            this.Name = "PanelWiz5ChoosePOIs";
            this.Size = new System.Drawing.Size(563, 149);
            this.Tag = "4|6";
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseUnused.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLetMeChoosePOI.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl lblGooglePOIChooseWhichOnesHeader;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.CheckEdit chkUseUnused;
        private DevExpress.XtraEditors.CheckEdit chkLetMeChoosePOI;
    }
}
