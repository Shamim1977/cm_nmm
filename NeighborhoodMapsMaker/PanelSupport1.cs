﻿using System;

namespace NeighborhoodMapsMaker
{
    public partial class PanelSupport1 : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelSupport1()
        {
            InitializeComponent();
        }

        private void btnGoToSupport_Click(object sender, System.EventArgs e)
        {
            if (Globals.CheckNet() == false)
            {
                DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
                args.Caption = Globals.AppName;
                args.Text = Environment.NewLine + Environment.NewLine
                    + "No Internet Access!"
                    + Environment.NewLine + Environment.NewLine;
                args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.OK };
                DevExpress.XtraEditors.XtraMessageBox.Show(args);

                return;
            }

            System.Diagnostics.Process.Start(Globals.SupportPage);
        }
    }
}
