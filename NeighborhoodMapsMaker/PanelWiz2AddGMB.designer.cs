﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz2AddGMB
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz2AddGMB));
            this.txtGMBListing = new DevExpress.XtraEditors.TextEdit();
            this.lblGMBListingCountry = new DevExpress.XtraEditors.LabelControl();
            this.lblGMBCity = new DevExpress.XtraEditors.LabelControl();
            this.txtGMBCity = new DevExpress.XtraEditors.TextEdit();
            this.lblGMBMainKeyword = new DevExpress.XtraEditors.LabelControl();
            this.txtGMBMainKeyword = new DevExpress.XtraEditors.TextEdit();
            this.lblGMBSecondaryKeyword = new DevExpress.XtraEditors.LabelControl();
            this.txtGMBSecondaryKeyword = new DevExpress.XtraEditors.TextEdit();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.lblMakingMapsHeader = new DevExpress.XtraEditors.LabelControl();
            this.pnlAutoSuggestHolder = new DevExpress.XtraEditors.XtraScrollableControl();
            this.lblNoNetNoti = new DevExpress.XtraEditors.LabelControl();
            this.cmbCountries = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblGMBListingName = new DevExpress.XtraEditors.LabelControl();
            this.btnGetSuggest = new DevExpress.XtraEditors.SimpleButton();
            this.lblGMBURL = new DevExpress.XtraEditors.LabelControl();
            this.txtGMBURL = new DevExpress.XtraEditors.TextEdit();
            this.btnHelp_P2_1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnSurfGMB = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMBListing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMBCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMBMainKeyword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMBSecondaryKeyword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCountries.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMBURL.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtGMBListing
            // 
            this.txtGMBListing.Location = new System.Drawing.Point(194, 103);
            this.txtGMBListing.Name = "txtGMBListing";
            this.txtGMBListing.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGMBListing.Properties.Appearance.Options.UseFont = true;
            this.txtGMBListing.Size = new System.Drawing.Size(441, 24);
            this.txtGMBListing.TabIndex = 1;
            this.txtGMBListing.Enter += new System.EventHandler(this.txtGMBListing_Enter);
            this.txtGMBListing.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtGMBListing_KeyUp);
            this.txtGMBListing.MouseUp += new System.Windows.Forms.MouseEventHandler(this.txtGMBListing_MouseUp);
            // 
            // lblGMBListingCountry
            // 
            this.lblGMBListingCountry.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGMBListingCountry.Appearance.Options.UseFont = true;
            this.lblGMBListingCountry.Location = new System.Drawing.Point(40, 60);
            this.lblGMBListingCountry.Name = "lblGMBListingCountry";
            this.lblGMBListingCountry.Size = new System.Drawing.Size(56, 18);
            this.lblGMBListingCountry.TabIndex = 13;
            this.lblGMBListingCountry.Text = "Country:";
            // 
            // lblGMBCity
            // 
            this.lblGMBCity.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGMBCity.Appearance.Options.UseFont = true;
            this.lblGMBCity.Location = new System.Drawing.Point(40, 190);
            this.lblGMBCity.Name = "lblGMBCity";
            this.lblGMBCity.Size = new System.Drawing.Size(29, 18);
            this.lblGMBCity.TabIndex = 15;
            this.lblGMBCity.Text = "City:";
            // 
            // txtGMBCity
            // 
            this.txtGMBCity.Location = new System.Drawing.Point(194, 190);
            this.txtGMBCity.Name = "txtGMBCity";
            this.txtGMBCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGMBCity.Properties.Appearance.Options.UseFont = true;
            this.txtGMBCity.Size = new System.Drawing.Size(529, 24);
            this.txtGMBCity.TabIndex = 3;
            // 
            // lblGMBMainKeyword
            // 
            this.lblGMBMainKeyword.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGMBMainKeyword.Appearance.Options.UseFont = true;
            this.lblGMBMainKeyword.Location = new System.Drawing.Point(40, 231);
            this.lblGMBMainKeyword.Name = "lblGMBMainKeyword";
            this.lblGMBMainKeyword.Size = new System.Drawing.Size(96, 18);
            this.lblGMBMainKeyword.TabIndex = 17;
            this.lblGMBMainKeyword.Text = "Main Keyword:";
            // 
            // txtGMBMainKeyword
            // 
            this.txtGMBMainKeyword.Location = new System.Drawing.Point(194, 231);
            this.txtGMBMainKeyword.Name = "txtGMBMainKeyword";
            this.txtGMBMainKeyword.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGMBMainKeyword.Properties.Appearance.Options.UseFont = true;
            this.txtGMBMainKeyword.Size = new System.Drawing.Size(529, 24);
            this.txtGMBMainKeyword.TabIndex = 4;
            // 
            // lblGMBSecondaryKeyword
            // 
            this.lblGMBSecondaryKeyword.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGMBSecondaryKeyword.Appearance.Options.UseFont = true;
            this.lblGMBSecondaryKeyword.Location = new System.Drawing.Point(40, 272);
            this.lblGMBSecondaryKeyword.Name = "lblGMBSecondaryKeyword";
            this.lblGMBSecondaryKeyword.Size = new System.Drawing.Size(141, 18);
            this.lblGMBSecondaryKeyword.TabIndex = 19;
            this.lblGMBSecondaryKeyword.Text = "Secondary Keywords:";
            // 
            // txtGMBSecondaryKeyword
            // 
            this.txtGMBSecondaryKeyword.EditValue = "Semi-colon separated keywords";
            this.txtGMBSecondaryKeyword.Location = new System.Drawing.Point(194, 272);
            this.txtGMBSecondaryKeyword.Name = "txtGMBSecondaryKeyword";
            this.txtGMBSecondaryKeyword.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGMBSecondaryKeyword.Properties.Appearance.Options.UseFont = true;
            this.txtGMBSecondaryKeyword.Properties.NullText = "Semi-Colon separated keywords";
            this.txtGMBSecondaryKeyword.Size = new System.Drawing.Size(529, 24);
            this.txtGMBSecondaryKeyword.TabIndex = 5;
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 40;
            // 
            // lblMakingMapsHeader
            // 
            this.lblMakingMapsHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMakingMapsHeader.Appearance.Options.UseFont = true;
            this.lblMakingMapsHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblMakingMapsHeader.Location = new System.Drawing.Point(40, 6);
            this.lblMakingMapsHeader.Name = "lblMakingMapsHeader";
            this.lblMakingMapsHeader.Size = new System.Drawing.Size(424, 23);
            this.lblMakingMapsHeader.TabIndex = 39;
            this.lblMakingMapsHeader.Text = "Google My Business";
            // 
            // pnlAutoSuggestHolder
            // 
            this.pnlAutoSuggestHolder.Appearance.Options.UseBorderColor = true;
            this.pnlAutoSuggestHolder.Location = new System.Drawing.Point(194, 307);
            this.pnlAutoSuggestHolder.Name = "pnlAutoSuggestHolder";
            this.pnlAutoSuggestHolder.ScrollBarSmallChange = 15;
            this.pnlAutoSuggestHolder.Size = new System.Drawing.Size(662, 220);
            this.pnlAutoSuggestHolder.TabIndex = 0;
            this.pnlAutoSuggestHolder.Visible = false;
            // 
            // lblNoNetNoti
            // 
            this.lblNoNetNoti.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoNetNoti.Appearance.Options.UseFont = true;
            this.lblNoNetNoti.Appearance.Options.UseTextOptions = true;
            this.lblNoNetNoti.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblNoNetNoti.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNoNetNoti.Location = new System.Drawing.Point(194, 38);
            this.lblNoNetNoti.Name = "lblNoNetNoti";
            this.lblNoNetNoti.Size = new System.Drawing.Size(529, 20);
            this.lblNoNetNoti.TabIndex = 41;
            // 
            // cmbCountries
            // 
            this.cmbCountries.Location = new System.Drawing.Point(193, 60);
            this.cmbCountries.Name = "cmbCountries";
            this.cmbCountries.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCountries.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmbCountries.Properties.Appearance.Options.UseFont = true;
            this.cmbCountries.Properties.Appearance.Options.UseForeColor = true;
            this.cmbCountries.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCountries.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbCountries.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCountries.Properties.AppearanceFocused.Options.UseFont = true;
            this.cmbCountries.Properties.AppearanceItemDisabled.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCountries.Properties.AppearanceItemDisabled.Options.UseFont = true;
            this.cmbCountries.Properties.AppearanceItemHighlight.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCountries.Properties.AppearanceItemHighlight.Options.UseFont = true;
            this.cmbCountries.Properties.AppearanceItemSelected.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCountries.Properties.AppearanceItemSelected.Options.UseFont = true;
            this.cmbCountries.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCountries.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cmbCountries.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCountries.Properties.DropDownRows = 15;
            this.cmbCountries.Size = new System.Drawing.Size(530, 24);
            this.cmbCountries.TabIndex = 0;
            // 
            // lblGMBListingName
            // 
            this.lblGMBListingName.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGMBListingName.Appearance.Options.UseFont = true;
            this.lblGMBListingName.Location = new System.Drawing.Point(40, 106);
            this.lblGMBListingName.Name = "lblGMBListingName";
            this.lblGMBListingName.Size = new System.Drawing.Size(135, 18);
            this.lblGMBListingName.TabIndex = 43;
            this.lblGMBListingName.Text = "Google My Business:";
            // 
            // btnGetSuggest
            // 
            this.btnGetSuggest.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetSuggest.Appearance.Options.UseFont = true;
            this.btnGetSuggest.Location = new System.Drawing.Point(641, 103);
            this.btnGetSuggest.Name = "btnGetSuggest";
            this.btnGetSuggest.Size = new System.Drawing.Size(82, 23);
            this.btnGetSuggest.TabIndex = 44;
            this.btnGetSuggest.Text = "&Suggest";
            this.btnGetSuggest.Click += new System.EventHandler(this.btnGetSuggest_Click);
            // 
            // lblGMBURL
            // 
            this.lblGMBURL.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGMBURL.Appearance.Options.UseFont = true;
            this.lblGMBURL.Location = new System.Drawing.Point(40, 145);
            this.lblGMBURL.Name = "lblGMBURL";
            this.lblGMBURL.Size = new System.Drawing.Size(67, 18);
            this.lblGMBURL.TabIndex = 46;
            this.lblGMBURL.Text = "GMB URL:";
            // 
            // txtGMBURL
            // 
            this.txtGMBURL.Location = new System.Drawing.Point(194, 145);
            this.txtGMBURL.Name = "txtGMBURL";
            this.txtGMBURL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGMBURL.Properties.Appearance.Options.UseFont = true;
            this.txtGMBURL.Size = new System.Drawing.Size(491, 24);
            this.txtGMBURL.TabIndex = 2;
            // 
            // btnHelp_P2_1
            // 
            this.btnHelp_P2_1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp_P2_1.ImageOptions.Image")));
            this.btnHelp_P2_1.Location = new System.Drawing.Point(729, 60);
            this.btnHelp_P2_1.Name = "btnHelp_P2_1";
            this.btnHelp_P2_1.Size = new System.Drawing.Size(25, 24);
            this.btnHelp_P2_1.TabIndex = 47;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(729, 103);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(25, 24);
            this.simpleButton1.TabIndex = 48;
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(729, 145);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(25, 24);
            this.simpleButton2.TabIndex = 49;
            // 
            // simpleButton3
            // 
            this.simpleButton3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.ImageOptions.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(729, 190);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(25, 24);
            this.simpleButton3.TabIndex = 50;
            // 
            // simpleButton4
            // 
            this.simpleButton4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.ImageOptions.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(729, 231);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(25, 24);
            this.simpleButton4.TabIndex = 51;
            // 
            // simpleButton5
            // 
            this.simpleButton5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.ImageOptions.Image")));
            this.simpleButton5.Location = new System.Drawing.Point(729, 272);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(25, 24);
            this.simpleButton5.TabIndex = 52;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnSurfGMB
            // 
            this.btnSurfGMB.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton6.ImageOptions.Image")));
            this.btnSurfGMB.Location = new System.Drawing.Point(691, 144);
            this.btnSurfGMB.Name = "btnSurfGMB";
            this.btnSurfGMB.Size = new System.Drawing.Size(32, 24);
            this.btnSurfGMB.TabIndex = 53;
            this.btnSurfGMB.ToolTip = "Click to to visit GMB site";
            this.btnSurfGMB.ToolTipTitle = "Neighborhood Maps Maker";
            this.btnSurfGMB.Click += new System.EventHandler(this.btnSurfGMB_Click);
            // 
            // PanelWiz2AddGMB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSurfGMB);
            this.Controls.Add(this.pnlAutoSuggestHolder);
            this.Controls.Add(this.lblGMBURL);
            this.Controls.Add(this.btnGetSuggest);
            this.Controls.Add(this.lblGMBListingName);
            this.Controls.Add(this.cmbCountries);
            this.Controls.Add(this.lblNoNetNoti);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblMakingMapsHeader);
            this.Controls.Add(this.lblGMBSecondaryKeyword);
            this.Controls.Add(this.txtGMBSecondaryKeyword);
            this.Controls.Add(this.lblGMBMainKeyword);
            this.Controls.Add(this.txtGMBMainKeyword);
            this.Controls.Add(this.lblGMBCity);
            this.Controls.Add(this.txtGMBCity);
            this.Controls.Add(this.lblGMBListingCountry);
            this.Controls.Add(this.txtGMBListing);
            this.Controls.Add(this.txtGMBURL);
            this.Controls.Add(this.simpleButton5);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnHelp_P2_1);
            this.Name = "PanelWiz2AddGMB";
            this.Size = new System.Drawing.Size(859, 363);
            this.Tag = "1|3";
            this.Load += new System.EventHandler(this.Panel2AddGMB_Load);
            this.Click += new System.EventHandler(this.PanelWiz2AddGMB_Click);
            ((System.ComponentModel.ISupportInitialize)(this.txtGMBListing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMBCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMBMainKeyword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMBSecondaryKeyword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCountries.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGMBURL.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.TextEdit txtGMBListing;
        private DevExpress.XtraEditors.LabelControl lblGMBListingCountry;
        private DevExpress.XtraEditors.LabelControl lblGMBCity;
        private DevExpress.XtraEditors.TextEdit txtGMBCity;
        private DevExpress.XtraEditors.LabelControl lblGMBMainKeyword;
        private DevExpress.XtraEditors.TextEdit txtGMBMainKeyword;
        private DevExpress.XtraEditors.LabelControl lblGMBSecondaryKeyword;
        private DevExpress.XtraEditors.TextEdit txtGMBSecondaryKeyword;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.LabelControl lblMakingMapsHeader;
        private DevExpress.XtraEditors.XtraScrollableControl pnlAutoSuggestHolder;
        private DevExpress.XtraEditors.LabelControl lblNoNetNoti;
        private DevExpress.XtraEditors.ComboBoxEdit cmbCountries;
        private DevExpress.XtraEditors.LabelControl lblGMBListingName;
        private DevExpress.XtraEditors.SimpleButton btnGetSuggest;
        private DevExpress.XtraEditors.LabelControl lblGMBURL;
        private DevExpress.XtraEditors.TextEdit txtGMBURL;
        private DevExpress.XtraEditors.SimpleButton btnHelp_P2_1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.SimpleButton btnSurfGMB;
    }
}
