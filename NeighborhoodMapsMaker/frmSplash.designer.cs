﻿namespace NeighborhoodMapsMaker
{
    partial class frmSplash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSplash));
            this.picAppLogo = new System.Windows.Forms.PictureBox();
            this.lblAppName = new DevExpress.XtraEditors.LabelControl();
            this.lblAppVer = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.progFancy = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picAppLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progFancy.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // picAppLogo
            // 
            this.picAppLogo.Image = ((System.Drawing.Image)(resources.GetObject("picAppLogo.Image")));
            this.picAppLogo.Location = new System.Drawing.Point(27, 31);
            this.picAppLogo.Name = "picAppLogo";
            this.picAppLogo.Size = new System.Drawing.Size(64, 64);
            this.picAppLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picAppLogo.TabIndex = 0;
            this.picAppLogo.TabStop = false;
            // 
            // lblAppName
            // 
            this.lblAppName.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppName.Appearance.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblAppName.Appearance.Options.UseFont = true;
            this.lblAppName.Appearance.Options.UseForeColor = true;
            this.lblAppName.Location = new System.Drawing.Point(97, 52);
            this.lblAppName.Name = "lblAppName";
            this.lblAppName.Size = new System.Drawing.Size(249, 25);
            this.lblAppName.TabIndex = 1;
            this.lblAppName.Text = "Neighborhood Maps Maker";
            // 
            // lblAppVer
            // 
            this.lblAppVer.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppVer.Appearance.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblAppVer.Appearance.Options.UseFont = true;
            this.lblAppVer.Appearance.Options.UseForeColor = true;
            this.lblAppVer.Location = new System.Drawing.Point(299, 76);
            this.lblAppVer.Name = "lblAppVer";
            this.lblAppVer.Size = new System.Drawing.Size(50, 19);
            this.lblAppVer.TabIndex = 2;
            this.lblAppVer.Text = "v 1.1.4";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(435, 265);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(104, 25);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Chad Ware";
            // 
            // progFancy
            // 
            this.progFancy.EditValue = 0;
            this.progFancy.Location = new System.Drawing.Point(27, 276);
            this.progFancy.Name = "progFancy";
            this.progFancy.Size = new System.Drawing.Size(256, 10);
            this.progFancy.TabIndex = 4;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmSplash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
            this.BackgroundImageStore = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImageStore")));
            this.ClientSize = new System.Drawing.Size(580, 320);
            this.Controls.Add(this.progFancy);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.lblAppVer);
            this.Controls.Add(this.lblAppName);
            this.Controls.Add(this.picAppLogo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSplash";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Neighborhood Maps Maker - Welcome";
            this.Load += new System.EventHandler(this.frmSplash_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picAppLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progFancy.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picAppLogo;
        private DevExpress.XtraEditors.LabelControl lblAppName;
        private DevExpress.XtraEditors.LabelControl lblAppVer;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.MarqueeProgressBarControl progFancy;
        private System.Windows.Forms.Timer timer1;
    }
}