﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz8QtyPOI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz8QtyPOI));
            this.lblNonGooglePOIQtyHelp = new DevExpress.XtraEditors.LabelControl();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.lblNonGooglePOIHowManyHeader = new DevExpress.XtraEditors.LabelControl();
            this.txtNonGooglePOIQtyNumber = new DevExpress.XtraEditors.TextEdit();
            this.lblNonGooglePOIQtyLabel = new DevExpress.XtraEditors.LabelControl();
            this.chkGPOIWantSome = new DevExpress.XtraEditors.CheckEdit();
            this.chkGPOIWantAll = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNonGooglePOIQtyNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGPOIWantSome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGPOIWantAll.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNonGooglePOIQtyHelp
            // 
            this.lblNonGooglePOIQtyHelp.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNonGooglePOIQtyHelp.Appearance.Options.UseFont = true;
            this.lblNonGooglePOIQtyHelp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNonGooglePOIQtyHelp.Location = new System.Drawing.Point(215, 112);
            this.lblNonGooglePOIQtyHelp.Name = "lblNonGooglePOIQtyHelp";
            this.lblNonGooglePOIQtyHelp.Size = new System.Drawing.Size(113, 21);
            this.lblNonGooglePOIQtyHelp.TabIndex = 48;
            this.lblNonGooglePOIQtyHelp.Text = "(Integer)";
            this.lblNonGooglePOIQtyHelp.Visible = false;
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 47;
            // 
            // lblNonGooglePOIHowManyHeader
            // 
            this.lblNonGooglePOIHowManyHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNonGooglePOIHowManyHeader.Appearance.Options.UseFont = true;
            this.lblNonGooglePOIHowManyHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNonGooglePOIHowManyHeader.Location = new System.Drawing.Point(40, 6);
            this.lblNonGooglePOIHowManyHeader.Name = "lblNonGooglePOIHowManyHeader";
            this.lblNonGooglePOIHowManyHeader.Size = new System.Drawing.Size(375, 23);
            this.lblNonGooglePOIHowManyHeader.TabIndex = 45;
            this.lblNonGooglePOIHowManyHeader.Text = "Other Places of Interest (POI)";
            // 
            // txtNonGooglePOIQtyNumber
            // 
            this.txtNonGooglePOIQtyNumber.EditValue = "";
            this.txtNonGooglePOIQtyNumber.Location = new System.Drawing.Point(136, 112);
            this.txtNonGooglePOIQtyNumber.Name = "txtNonGooglePOIQtyNumber";
            this.txtNonGooglePOIQtyNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNonGooglePOIQtyNumber.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            this.txtNonGooglePOIQtyNumber.Properties.Appearance.Options.UseFont = true;
            this.txtNonGooglePOIQtyNumber.Properties.Appearance.Options.UseForeColor = true;
            this.txtNonGooglePOIQtyNumber.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtNonGooglePOIQtyNumber.Size = new System.Drawing.Size(73, 26);
            this.txtNonGooglePOIQtyNumber.TabIndex = 43;
            this.txtNonGooglePOIQtyNumber.Visible = false;
            // 
            // lblNonGooglePOIQtyLabel
            // 
            this.lblNonGooglePOIQtyLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNonGooglePOIQtyLabel.Appearance.Options.UseFont = true;
            this.lblNonGooglePOIQtyLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblNonGooglePOIQtyLabel.Location = new System.Drawing.Point(40, 112);
            this.lblNonGooglePOIQtyLabel.Name = "lblNonGooglePOIQtyLabel";
            this.lblNonGooglePOIQtyLabel.Size = new System.Drawing.Size(91, 21);
            this.lblNonGooglePOIQtyLabel.TabIndex = 46;
            this.lblNonGooglePOIQtyLabel.Text = "How Many:";
            this.lblNonGooglePOIQtyLabel.Visible = false;
            // 
            // chkGPOIWantSome
            // 
            this.chkGPOIWantSome.Location = new System.Drawing.Point(159, 62);
            this.chkGPOIWantSome.Name = "chkGPOIWantSome";
            this.chkGPOIWantSome.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGPOIWantSome.Properties.Appearance.Options.UseFont = true;
            this.chkGPOIWantSome.Properties.Caption = "I Want Some";
            this.chkGPOIWantSome.Size = new System.Drawing.Size(113, 22);
            this.chkGPOIWantSome.TabIndex = 56;
            this.chkGPOIWantSome.EditValueChanged += new System.EventHandler(this.chkGPOIWantSome_EditValueChanged);
            // 
            // chkGPOIWantAll
            // 
            this.chkGPOIWantAll.EditValue = true;
            this.chkGPOIWantAll.Location = new System.Drawing.Point(40, 62);
            this.chkGPOIWantAll.Name = "chkGPOIWantAll";
            this.chkGPOIWantAll.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGPOIWantAll.Properties.Appearance.Options.UseFont = true;
            this.chkGPOIWantAll.Properties.Caption = "I Want All";
            this.chkGPOIWantAll.Size = new System.Drawing.Size(113, 22);
            this.chkGPOIWantAll.TabIndex = 55;
            this.chkGPOIWantAll.EditValueChanged += new System.EventHandler(this.chkGPOIWantAll_EditValueChanged);
            // 
            // PanelWiz8QtyPOI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkGPOIWantSome);
            this.Controls.Add(this.chkGPOIWantAll);
            this.Controls.Add(this.lblNonGooglePOIQtyHelp);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblNonGooglePOIHowManyHeader);
            this.Controls.Add(this.txtNonGooglePOIQtyNumber);
            this.Controls.Add(this.lblNonGooglePOIQtyLabel);
            this.Name = "PanelWiz8QtyPOI";
            this.Size = new System.Drawing.Size(418, 153);
            this.Tag = "7|9";
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNonGooglePOIQtyNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGPOIWantSome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGPOIWantAll.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblNonGooglePOIQtyHelp;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.LabelControl lblNonGooglePOIHowManyHeader;
        private DevExpress.XtraEditors.TextEdit txtNonGooglePOIQtyNumber;
        private DevExpress.XtraEditors.LabelControl lblNonGooglePOIQtyLabel;
        private DevExpress.XtraEditors.CheckEdit chkGPOIWantSome;
        private DevExpress.XtraEditors.CheckEdit chkGPOIWantAll;
    }
}
