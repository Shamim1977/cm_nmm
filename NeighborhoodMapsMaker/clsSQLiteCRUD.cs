﻿using System;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace NeighborhoodMapsMaker
{
    class clsSQLiteCRUD
    {
        //Install SQLite from Project>Manage Nuget Packages...
        //Choose System.Data.SQLite to install

        public static String AppDataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Local\\NMM";
        public static String UserDBPath = AppDataDirectory + "\\nmm.db";

        public static int INSERT_ROWS(string stmt, Boolean multiple)
        {
            int ret = -1;
            if (!File.Exists(UserDBPath)) return ret;

            try
            {
                SQLiteConnection cn = new SQLiteConnection(@"Data Source=" + UserDBPath);
                cn.Open();
                SQLiteCommand command = new SQLiteCommand(cn);
                command.CommandType = CommandType.Text;
                command.CommandText = CleanseFirst(stmt);
                command.ExecuteNonQuery();
                cn.Close();
                ret = 1;
            }
            catch(Exception ex)
            {
                //Do nothing
            }
            
            return ret;
        }

        public static int UPDATE_ROWS(string stmt)
        {
            int Ret = -1;
            if (!File.Exists(UserDBPath)) return Ret;

            try
            {
                SQLiteConnection cn = new SQLiteConnection(@"Data Source=" + UserDBPath);
                cn.Open();
                SQLiteCommand command = new SQLiteCommand(cn);
                command.CommandType = CommandType.Text;
                command.CommandText = CleanseFirst(stmt);
                command.ExecuteNonQuery();
                Ret = 1;
                cn.Close();
            }
            catch (SQLiteException ex)
            {
                //Do Nothing
            }
            
            return Ret;
        }

        public static string GetACellVal(string stmt)
        {
            string Ret = "";
            if (!File.Exists(UserDBPath)) return Ret;

            SQLiteConnection cn = new SQLiteConnection(@"Data Source=" + UserDBPath);
            cn.Open();
            DataSet ds = new DataSet();
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(CleanseFirst(stmt), cn);
            adapter.Fill(ds);
            DataTable dt = ds.Tables[0];

            Ret = dt!=null?(dt.Rows.Count > 0 ? (dt.Rows[0][0] is null?"": dt.Rows[0][0].ToString()) : "") :"";

            cn.Close();

            return Ret;
        }

        public static DataTable GiveADataTable(string stmt)
        {
            DataTable DTable = null;
            if (!File.Exists(UserDBPath)) return DTable;

            SQLiteConnection cn = new SQLiteConnection(@"Data Source=" + UserDBPath);
            cn.Open();
            DataSet ds = new DataSet();
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(CleanseFirst(stmt), cn);
            adapter.Fill(ds);
            DTable = ds.Tables[0].Rows.Count==0?null:ds.Tables[0];
            cn.Close();

            return DTable;
        }

        public static string CleanseFirst(string input)
        {
            //input = input.Replace("'", "''");
            //input = input.Replace("\"", "\"\"");
            return input;
        }

    }
}
