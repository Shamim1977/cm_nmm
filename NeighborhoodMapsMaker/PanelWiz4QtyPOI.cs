﻿using System;

namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz4QtyPOI : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelWiz4QtyPOI()
        {
            InitializeComponent();
        }

        private void ShowHideQtyControlsForPOI()
        {
            if (this.chkGPOIWantAll.Checked)
            {
                this.txtGooglePOIQtyNumber.Visible = false;
                this.lblGooglePOIQtyLabel.Visible = false;
                this.lblGooglePOIQtyHelp.Visible = false;
            }
            else
            {
                this.txtGooglePOIQtyNumber.Visible = true;
                this.lblGooglePOIQtyLabel.Visible = true;
                this.lblGooglePOIQtyHelp.Visible = true;
                txtGooglePOIQtyNumber.Focus();
            }
        }

        private void chkGPOIWantSome_EditValueChanged(object sender, EventArgs e)
        {
            if (chkGPOIWantSome.Checked)
            {
                chkGPOIWantAll.Checked = false;
            }
            else
            {
                chkGPOIWantAll.Checked = true;
            }

            ShowHideQtyControlsForPOI();
        }

        private void chkGPOIWantAll_EditValueChanged(object sender, EventArgs e)
        {
            if (chkGPOIWantAll.Checked)
            {
                chkGPOIWantSome.Checked = false;
            }
            else
            {
                chkGPOIWantSome.Checked = true;
            }
        }
    }
}
