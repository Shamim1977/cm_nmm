﻿namespace NeighborhoodMapsMaker
{
    partial class PanelModelPOI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblPOICardTitle = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.lblPOIAddress = new DevExpress.XtraEditors.LabelControl();
            this.chkSelect = new DevExpress.XtraEditors.CheckEdit();
            this.chkUseThisPOI = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelect.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseThisPOI.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(0, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(3, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // lblPOICardTitle
            // 
            this.lblPOICardTitle.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblPOICardTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPOICardTitle.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblPOICardTitle.Appearance.Options.UseBackColor = true;
            this.lblPOICardTitle.Appearance.Options.UseFont = true;
            this.lblPOICardTitle.Appearance.Options.UseForeColor = true;
            this.lblPOICardTitle.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblPOICardTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPOICardTitle.Location = new System.Drawing.Point(34, 2);
            this.lblPOICardTitle.Name = "lblPOICardTitle";
            this.lblPOICardTitle.Size = new System.Drawing.Size(621, 18);
            this.lblPOICardTitle.TabIndex = 2;
            this.lblPOICardTitle.Text = "<href=https://en.wikipedia.org/wiki/Pyramid>Pyramid - What a Wonder</href>";
            this.lblPOICardTitle.Click += new System.EventHandler(this.lblPOICardTitle_Click);
            // 
            // lblPOIAddress
            // 
            this.lblPOIAddress.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblPOIAddress.Location = new System.Drawing.Point(34, 22);
            this.lblPOIAddress.Name = "lblPOIAddress";
            this.lblPOIAddress.Size = new System.Drawing.Size(621, 16);
            this.lblPOIAddress.TabIndex = 8;
            this.lblPOIAddress.Text = "Brooklyn Bridge, New York, NY 10038, USA";
            // 
            // chkSelect
            // 
            this.chkSelect.Location = new System.Drawing.Point(10, 2);
            this.chkSelect.Name = "chkSelect";
            this.chkSelect.Properties.Caption = "";
            this.chkSelect.Size = new System.Drawing.Size(18, 19);
            this.chkSelect.TabIndex = 9;
            // 
            // chkUseThisPOI
            // 
            this.chkUseThisPOI.EditValue = true;
            this.chkUseThisPOI.Location = new System.Drawing.Point(701, 12);
            this.chkUseThisPOI.Name = "chkUseThisPOI";
            this.chkUseThisPOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUseThisPOI.Properties.Appearance.Options.UseFont = true;
            this.chkUseThisPOI.Properties.Caption = "Use";
            this.chkUseThisPOI.Size = new System.Drawing.Size(62, 20);
            this.chkUseThisPOI.TabIndex = 10;
            // 
            // PanelModelPOI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkUseThisPOI);
            this.Controls.Add(this.chkSelect);
            this.Controls.Add(this.lblPOIAddress);
            this.Controls.Add(this.lblPOICardTitle);
            this.Controls.Add(this.label1);
            this.Name = "PanelModelPOI";
            this.Size = new System.Drawing.Size(768, 44);
            ((System.ComponentModel.ISupportInitialize)(this.chkSelect.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseThisPOI.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.HyperlinkLabelControl lblPOICardTitle;
        private DevExpress.XtraEditors.LabelControl lblPOIAddress;
        private DevExpress.XtraEditors.CheckEdit chkSelect;
        private DevExpress.XtraEditors.CheckEdit chkUseThisPOI;
    }
}
