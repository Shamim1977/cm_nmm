﻿using System;

namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz1ChooseProject : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelWiz1ChooseProject()
        {
            InitializeComponent();
        }

        private void Panel1ChooseProject_Load(object sender, EventArgs e)
        {
            SetAName();
            GetProjectList();
        }

       private void GetProjectList()
        {

            this.cmbProjectList.Properties.Items.Add("-None-");
        }

        private void btnGenProjectName_Click(object sender, EventArgs e)
        {
            SetAName();
        }

        private void SetAName()
        {
            String datepart = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff");
            String project_name = "project-nmm-" + datepart;
            this.txtNewProjectName.Text = project_name;
            Globals.VAR_PROJECT_NAME = project_name;
        }

        private void ShowHideControls()
        {
            if (chkCreateNewProject.Checked)
            {
                cmbProjectList.Visible = false;
                txtNewProjectName.Visible = true;
                btnGenProjectName.Visible = true;

                txtNewProjectName.SetBounds(40, 95, txtNewProjectName.Width, txtNewProjectName.Height);
                btnGenProjectName.SetBounds(363, 94, btnGenProjectName.Width, btnGenProjectName.Height);

                txtNewProjectName.Focus();
            }
            else
            {
                txtNewProjectName.Visible = false;
                btnGenProjectName.Visible = false;
                cmbProjectList.SetBounds(40, 95, cmbProjectList.Width, cmbProjectList.Height);
                cmbProjectList.Visible = true;
                cmbProjectList.Focus();
            }
        }

        private void chkCreateNewProject_EditValueChanged(object sender, EventArgs e)
        {
            chkChooseOneExisting.Checked = !chkCreateNewProject.Checked;
            ShowHideControls();
        }

        private void chkChooseOneExisting_EditValueChanged(object sender, EventArgs e)
        {
            chkCreateNewProject.Checked = !chkChooseOneExisting.Checked;
            ShowHideControls();
        }
    }
}
