﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz1ChooseProject
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz1ChooseProject));
            this.btnGenProjectName = new DevExpress.XtraEditors.SimpleButton();
            this.cmbProjectList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtNewProjectName = new DevExpress.XtraEditors.TextEdit();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.lblMakingMapsHeader = new DevExpress.XtraEditors.LabelControl();
            this.chkCreateNewProject = new DevExpress.XtraEditors.CheckEdit();
            this.chkChooseOneExisting = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProjectList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateNewProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChooseOneExisting.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGenProjectName
            // 
            this.btnGenProjectName.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenProjectName.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnGenProjectName.Appearance.Options.UseFont = true;
            this.btnGenProjectName.Appearance.Options.UseForeColor = true;
            this.btnGenProjectName.Location = new System.Drawing.Point(363, 94);
            this.btnGenProjectName.Name = "btnGenProjectName";
            this.btnGenProjectName.Size = new System.Drawing.Size(146, 25);
            this.btnGenProjectName.TabIndex = 9;
            this.btnGenProjectName.Text = "&Generate Name";
            this.btnGenProjectName.Click += new System.EventHandler(this.btnGenProjectName_Click);
            // 
            // cmbProjectList
            // 
            this.cmbProjectList.Location = new System.Drawing.Point(40, 125);
            this.cmbProjectList.Name = "cmbProjectList";
            this.cmbProjectList.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProjectList.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmbProjectList.Properties.Appearance.Options.UseFont = true;
            this.cmbProjectList.Properties.Appearance.Options.UseForeColor = true;
            this.cmbProjectList.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProjectList.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbProjectList.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProjectList.Properties.AppearanceFocused.Options.UseFont = true;
            this.cmbProjectList.Properties.AppearanceItemDisabled.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProjectList.Properties.AppearanceItemDisabled.Options.UseFont = true;
            this.cmbProjectList.Properties.AppearanceItemHighlight.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProjectList.Properties.AppearanceItemHighlight.Options.UseFont = true;
            this.cmbProjectList.Properties.AppearanceItemSelected.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProjectList.Properties.AppearanceItemSelected.Options.UseFont = true;
            this.cmbProjectList.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProjectList.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cmbProjectList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbProjectList.Size = new System.Drawing.Size(317, 24);
            this.cmbProjectList.TabIndex = 3;
            this.cmbProjectList.Visible = false;
            // 
            // txtNewProjectName
            // 
            this.txtNewProjectName.Location = new System.Drawing.Point(40, 95);
            this.txtNewProjectName.Name = "txtNewProjectName";
            this.txtNewProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewProjectName.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.txtNewProjectName.Properties.Appearance.Options.UseFont = true;
            this.txtNewProjectName.Properties.Appearance.Options.UseForeColor = true;
            this.txtNewProjectName.Properties.NullValuePrompt = "Project Name";
            this.txtNewProjectName.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtNewProjectName.Size = new System.Drawing.Size(317, 24);
            this.txtNewProjectName.TabIndex = 0;
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 40;
            // 
            // lblMakingMapsHeader
            // 
            this.lblMakingMapsHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMakingMapsHeader.Appearance.Options.UseFont = true;
            this.lblMakingMapsHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblMakingMapsHeader.Location = new System.Drawing.Point(40, 6);
            this.lblMakingMapsHeader.Name = "lblMakingMapsHeader";
            this.lblMakingMapsHeader.Size = new System.Drawing.Size(485, 23);
            this.lblMakingMapsHeader.TabIndex = 39;
            this.lblMakingMapsHeader.Text = "Create / Edit Project";
            // 
            // chkCreateNewProject
            // 
            this.chkCreateNewProject.EditValue = true;
            this.chkCreateNewProject.Location = new System.Drawing.Point(40, 54);
            this.chkCreateNewProject.Name = "chkCreateNewProject";
            this.chkCreateNewProject.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCreateNewProject.Properties.Appearance.Options.UseFont = true;
            this.chkCreateNewProject.Properties.Caption = "Create a New Project";
            this.chkCreateNewProject.Size = new System.Drawing.Size(189, 22);
            this.chkCreateNewProject.TabIndex = 41;
            this.chkCreateNewProject.EditValueChanged += new System.EventHandler(this.chkCreateNewProject_EditValueChanged);
            // 
            // chkChooseOneExisting
            // 
            this.chkChooseOneExisting.Location = new System.Drawing.Point(235, 54);
            this.chkChooseOneExisting.Name = "chkChooseOneExisting";
            this.chkChooseOneExisting.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkChooseOneExisting.Properties.Appearance.Options.UseFont = true;
            this.chkChooseOneExisting.Properties.Caption = "Choose One Existing";
            this.chkChooseOneExisting.Size = new System.Drawing.Size(179, 22);
            this.chkChooseOneExisting.TabIndex = 42;
            this.chkChooseOneExisting.EditValueChanged += new System.EventHandler(this.chkChooseOneExisting_EditValueChanged);
            // 
            // PanelWiz1ChooseProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkChooseOneExisting);
            this.Controls.Add(this.chkCreateNewProject);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblMakingMapsHeader);
            this.Controls.Add(this.btnGenProjectName);
            this.Controls.Add(this.cmbProjectList);
            this.Controls.Add(this.txtNewProjectName);
            this.Name = "PanelWiz1ChooseProject";
            this.Size = new System.Drawing.Size(524, 167);
            this.Tag = "0|2";
            this.Load += new System.EventHandler(this.Panel1ChooseProject_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmbProjectList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateNewProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChooseOneExisting.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.ComboBoxEdit cmbProjectList;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraEditors.TextEdit txtNewProjectName;
        private DevExpress.XtraEditors.SimpleButton btnGenProjectName;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.LabelControl lblMakingMapsHeader;
        private DevExpress.XtraEditors.CheckEdit chkCreateNewProject;
        private DevExpress.XtraEditors.CheckEdit chkChooseOneExisting;
    }
}
