﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz15MapCreation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz15MapCreation));
            this.lblMakingMapsHeader = new DevExpress.XtraEditors.LabelControl();
            this.lblMakingMapsETA = new DevExpress.XtraEditors.LabelControl();
            this.lblMakingMapsETAVal = new DevExpress.XtraEditors.LabelControl();
            this.lblMakingMapsQty = new DevExpress.XtraEditors.LabelControl();
            this.lblMakingMapsQtyPOI = new DevExpress.XtraEditors.LabelControl();
            this.lblMakingMapsWhat = new DevExpress.XtraEditors.LabelControl();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMakingMapsHeader
            // 
            this.lblMakingMapsHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMakingMapsHeader.Appearance.Options.UseFont = true;
            this.lblMakingMapsHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblMakingMapsHeader.Location = new System.Drawing.Point(40, 6);
            this.lblMakingMapsHeader.Name = "lblMakingMapsHeader";
            this.lblMakingMapsHeader.Size = new System.Drawing.Size(216, 23);
            this.lblMakingMapsHeader.TabIndex = 19;
            this.lblMakingMapsHeader.Text = "Making Maps";
            // 
            // lblMakingMapsETA
            // 
            this.lblMakingMapsETA.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMakingMapsETA.Appearance.Options.UseFont = true;
            this.lblMakingMapsETA.Location = new System.Drawing.Point(40, 103);
            this.lblMakingMapsETA.Name = "lblMakingMapsETA";
            this.lblMakingMapsETA.Size = new System.Drawing.Size(191, 18);
            this.lblMakingMapsETA.TabIndex = 18;
            this.lblMakingMapsETA.Text = "Estimated Time to Complete:";
            // 
            // lblMakingMapsETAVal
            // 
            this.lblMakingMapsETAVal.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMakingMapsETAVal.Appearance.Options.UseFont = true;
            this.lblMakingMapsETAVal.Location = new System.Drawing.Point(257, 103);
            this.lblMakingMapsETAVal.Name = "lblMakingMapsETAVal";
            this.lblMakingMapsETAVal.Size = new System.Drawing.Size(137, 18);
            this.lblMakingMapsETAVal.TabIndex = 20;
            this.lblMakingMapsETAVal.Text = "11 Minute 38 Second";
            // 
            // lblMakingMapsQty
            // 
            this.lblMakingMapsQty.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMakingMapsQty.Appearance.Options.UseFont = true;
            this.lblMakingMapsQty.Location = new System.Drawing.Point(257, 60);
            this.lblMakingMapsQty.Name = "lblMakingMapsQty";
            this.lblMakingMapsQty.Size = new System.Drawing.Size(16, 18);
            this.lblMakingMapsQty.TabIndex = 22;
            this.lblMakingMapsQty.Text = "17";
            // 
            // lblMakingMapsQtyPOI
            // 
            this.lblMakingMapsQtyPOI.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMakingMapsQtyPOI.Appearance.Options.UseFont = true;
            this.lblMakingMapsQtyPOI.Location = new System.Drawing.Point(40, 60);
            this.lblMakingMapsQtyPOI.Name = "lblMakingMapsQtyPOI";
            this.lblMakingMapsQtyPOI.Size = new System.Drawing.Size(143, 18);
            this.lblMakingMapsQtyPOI.TabIndex = 21;
            this.lblMakingMapsQtyPOI.Text = "Total Number of POI:";
            // 
            // lblMakingMapsWhat
            // 
            this.lblMakingMapsWhat.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMakingMapsWhat.Appearance.Options.UseFont = true;
            this.lblMakingMapsWhat.Location = new System.Drawing.Point(40, 143);
            this.lblMakingMapsWhat.Name = "lblMakingMapsWhat";
            this.lblMakingMapsWhat.Size = new System.Drawing.Size(141, 18);
            this.lblMakingMapsWhat.TabIndex = 23;
            this.lblMakingMapsWhat.Text = "What you\'re creating:";
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 38;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(257, 187);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(209, 150);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 41;
            this.pictureBox3.TabStop = false;
            // 
            // PanelWiz15MapCreation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblMakingMapsWhat);
            this.Controls.Add(this.lblMakingMapsQty);
            this.Controls.Add(this.lblMakingMapsQtyPOI);
            this.Controls.Add(this.lblMakingMapsETAVal);
            this.Controls.Add(this.lblMakingMapsHeader);
            this.Controls.Add(this.lblMakingMapsETA);
            this.Name = "PanelWiz15MapCreation";
            this.Size = new System.Drawing.Size(743, 362);
            this.Tag = "14|0";
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblMakingMapsHeader;
        private DevExpress.XtraEditors.LabelControl lblMakingMapsETA;
        private DevExpress.XtraEditors.LabelControl lblMakingMapsETAVal;
        private DevExpress.XtraEditors.LabelControl lblMakingMapsQty;
        private DevExpress.XtraEditors.LabelControl lblMakingMapsQtyPOI;
        private DevExpress.XtraEditors.LabelControl lblMakingMapsWhat;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}
