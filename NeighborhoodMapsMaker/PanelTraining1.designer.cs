﻿namespace NeighborhoodMapsMaker
{
    partial class PanelTraining1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelTraining1));
            this.btnGoToTraining = new DevExpress.XtraEditors.SimpleButton();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.lblTrainignHeader = new DevExpress.XtraEditors.LabelControl();
            this.lblTrainingSubHeader = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGoToTraining
            // 
            this.btnGoToTraining.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoToTraining.Appearance.Options.UseFont = true;
            this.btnGoToTraining.Location = new System.Drawing.Point(49, 96);
            this.btnGoToTraining.Name = "btnGoToTraining";
            this.btnGoToTraining.Size = new System.Drawing.Size(186, 34);
            this.btnGoToTraining.TabIndex = 49;
            this.btnGoToTraining.Text = "Take Me There";
            this.btnGoToTraining.Click += new System.EventHandler(this.btnGoToTraining_Click);
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 48;
            // 
            // lblTrainignHeader
            // 
            this.lblTrainignHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrainignHeader.Appearance.Options.UseFont = true;
            this.lblTrainignHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTrainignHeader.Location = new System.Drawing.Point(40, 6);
            this.lblTrainignHeader.Name = "lblTrainignHeader";
            this.lblTrainignHeader.Size = new System.Drawing.Size(274, 23);
            this.lblTrainignHeader.TabIndex = 47;
            this.lblTrainignHeader.Text = "Training";
            // 
            // lblTrainingSubHeader
            // 
            this.lblTrainingSubHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrainingSubHeader.Appearance.Options.UseFont = true;
            this.lblTrainingSubHeader.Appearance.Options.UseTextOptions = true;
            this.lblTrainingSubHeader.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTrainingSubHeader.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblTrainingSubHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTrainingSubHeader.Location = new System.Drawing.Point(49, 60);
            this.lblTrainingSubHeader.Name = "lblTrainingSubHeader";
            this.lblTrainingSubHeader.Size = new System.Drawing.Size(658, 23);
            this.lblTrainingSubHeader.TabIndex = 46;
            this.lblTrainingSubHeader.Text = "ChadConnects has a rich set of training materials for Neighborhood Maps Maker, Al" +
    "l Free!";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(40, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(3, 70);
            this.label1.TabIndex = 45;
            this.label1.Text = "label1";
            // 
            // PanelTraining1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnGoToTraining);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblTrainignHeader);
            this.Controls.Add(this.lblTrainingSubHeader);
            this.Controls.Add(this.label1);
            this.Name = "PanelTraining1";
            this.Size = new System.Drawing.Size(736, 150);
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnGoToTraining;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.LabelControl lblTrainignHeader;
        private DevExpress.XtraEditors.LabelControl lblTrainingSubHeader;
        private System.Windows.Forms.Label label1;
    }
}
