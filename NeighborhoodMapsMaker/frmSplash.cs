﻿using System;
using System.Drawing;

namespace NeighborhoodMapsMaker
{
    public partial class frmSplash : DevExpress.XtraEditors.XtraForm
    {
        public frmSplash()
        {
            InitializeComponent();
        }

        private void frmSplash_Load(object sender, EventArgs e)
        {
            this.Text = Globals.AppName;

            lblAppName.Text = Globals.AppName;
            lblAppVer.Text = "v " + Globals.AppVer;
        
            picAppLogo.Parent = this;
            picAppLogo.BackColor = Color.Transparent;

            progFancy.Parent = this;
            progFancy.BackColor = Color.Transparent;
        }

        int ProgressValue = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ProgressValue >= 4)
            {
                timer1.Enabled = false;
                this.Visible = false;

                //Load Main Window
                Globals.frmmain = new frmMain();
                Globals.frmmain.Show();

            }
            else
            {
                ProgressValue += 1;
                //int time_remains = 4 - ProgressValue;
                //progFancy.Description = "App loads in " + time_remains + " second" + (time_remains > 1 ? "s" : "");
            }
        }
    }
}