﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz4QtyPOI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz4QtyPOI));
            this.txtGooglePOIQtyNumber = new DevExpress.XtraEditors.TextEdit();
            this.lblGooglePOIHowManyHeader = new DevExpress.XtraEditors.LabelControl();
            this.lblGooglePOIQtyLabel = new DevExpress.XtraEditors.LabelControl();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.lblGooglePOIQtyHelp = new DevExpress.XtraEditors.LabelControl();
            this.chkGPOIWantAll = new DevExpress.XtraEditors.CheckEdit();
            this.chkGPOIWantSome = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGooglePOIQtyNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGPOIWantAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGPOIWantSome.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtGooglePOIQtyNumber
            // 
            this.txtGooglePOIQtyNumber.EditValue = "";
            this.txtGooglePOIQtyNumber.Location = new System.Drawing.Point(137, 112);
            this.txtGooglePOIQtyNumber.Name = "txtGooglePOIQtyNumber";
            this.txtGooglePOIQtyNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGooglePOIQtyNumber.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            this.txtGooglePOIQtyNumber.Properties.Appearance.Options.UseFont = true;
            this.txtGooglePOIQtyNumber.Properties.Appearance.Options.UseForeColor = true;
            this.txtGooglePOIQtyNumber.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGooglePOIQtyNumber.Size = new System.Drawing.Size(73, 26);
            this.txtGooglePOIQtyNumber.TabIndex = 1;
            this.txtGooglePOIQtyNumber.Visible = false;
            // 
            // lblGooglePOIHowManyHeader
            // 
            this.lblGooglePOIHowManyHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGooglePOIHowManyHeader.Appearance.Options.UseFont = true;
            this.lblGooglePOIHowManyHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGooglePOIHowManyHeader.Location = new System.Drawing.Point(40, 6);
            this.lblGooglePOIHowManyHeader.Name = "lblGooglePOIHowManyHeader";
            this.lblGooglePOIHowManyHeader.Size = new System.Drawing.Size(375, 23);
            this.lblGooglePOIHowManyHeader.TabIndex = 15;
            this.lblGooglePOIHowManyHeader.Text = "Google Places of Interest (POI)";
            // 
            // lblGooglePOIQtyLabel
            // 
            this.lblGooglePOIQtyLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGooglePOIQtyLabel.Appearance.Options.UseFont = true;
            this.lblGooglePOIQtyLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGooglePOIQtyLabel.Location = new System.Drawing.Point(40, 112);
            this.lblGooglePOIQtyLabel.Name = "lblGooglePOIQtyLabel";
            this.lblGooglePOIQtyLabel.Size = new System.Drawing.Size(91, 21);
            this.lblGooglePOIQtyLabel.TabIndex = 16;
            this.lblGooglePOIQtyLabel.Text = "How Many:";
            this.lblGooglePOIQtyLabel.Visible = false;
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 41;
            // 
            // lblGooglePOIQtyHelp
            // 
            this.lblGooglePOIQtyHelp.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGooglePOIQtyHelp.Appearance.Options.UseFont = true;
            this.lblGooglePOIQtyHelp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGooglePOIQtyHelp.Location = new System.Drawing.Point(216, 112);
            this.lblGooglePOIQtyHelp.Name = "lblGooglePOIQtyHelp";
            this.lblGooglePOIQtyHelp.Size = new System.Drawing.Size(94, 21);
            this.lblGooglePOIQtyHelp.TabIndex = 42;
            this.lblGooglePOIQtyHelp.Text = "(Integer)";
            this.lblGooglePOIQtyHelp.Visible = false;
            // 
            // chkGPOIWantAll
            // 
            this.chkGPOIWantAll.EditValue = true;
            this.chkGPOIWantAll.Location = new System.Drawing.Point(40, 62);
            this.chkGPOIWantAll.Name = "chkGPOIWantAll";
            this.chkGPOIWantAll.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGPOIWantAll.Properties.Appearance.Options.UseFont = true;
            this.chkGPOIWantAll.Properties.Caption = "I Want All";
            this.chkGPOIWantAll.Size = new System.Drawing.Size(113, 22);
            this.chkGPOIWantAll.TabIndex = 53;
            this.chkGPOIWantAll.EditValueChanged += new System.EventHandler(this.chkGPOIWantAll_EditValueChanged);
            // 
            // chkGPOIWantSome
            // 
            this.chkGPOIWantSome.Location = new System.Drawing.Point(159, 62);
            this.chkGPOIWantSome.Name = "chkGPOIWantSome";
            this.chkGPOIWantSome.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGPOIWantSome.Properties.Appearance.Options.UseFont = true;
            this.chkGPOIWantSome.Properties.Caption = "I Want Some";
            this.chkGPOIWantSome.Size = new System.Drawing.Size(113, 22);
            this.chkGPOIWantSome.TabIndex = 54;
            this.chkGPOIWantSome.EditValueChanged += new System.EventHandler(this.chkGPOIWantSome_EditValueChanged);
            // 
            // PanelWiz4QtyPOI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkGPOIWantSome);
            this.Controls.Add(this.chkGPOIWantAll);
            this.Controls.Add(this.lblGooglePOIQtyHelp);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblGooglePOIHowManyHeader);
            this.Controls.Add(this.txtGooglePOIQtyNumber);
            this.Controls.Add(this.lblGooglePOIQtyLabel);
            this.Name = "PanelWiz4QtyPOI";
            this.Size = new System.Drawing.Size(418, 153);
            this.Tag = "3|5";
            ((System.ComponentModel.ISupportInitialize)(this.txtGooglePOIQtyNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGPOIWantAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGPOIWantSome.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.TextEdit txtGooglePOIQtyNumber;
        private DevExpress.XtraEditors.LabelControl lblGooglePOIHowManyHeader;
        private DevExpress.XtraEditors.LabelControl lblGooglePOIQtyLabel;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.LabelControl lblGooglePOIQtyHelp;
        private DevExpress.XtraEditors.CheckEdit chkGPOIWantAll;
        private DevExpress.XtraEditors.CheckEdit chkGPOIWantSome;
    }
}
