﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz10DistanceFromBiz
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz10DistanceFromBiz));
            this.lblUseGooglePOIYesOrNo = new DevExpress.XtraEditors.LabelControl();
            this.cmbPOIFromAPIDistanceKM = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPOIFromAPIDistanceKM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUseGooglePOIYesOrNo
            // 
            this.lblUseGooglePOIYesOrNo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUseGooglePOIYesOrNo.Appearance.Options.UseFont = true;
            this.lblUseGooglePOIYesOrNo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblUseGooglePOIYesOrNo.Location = new System.Drawing.Point(40, 6);
            this.lblUseGooglePOIYesOrNo.Name = "lblUseGooglePOIYesOrNo";
            this.lblUseGooglePOIYesOrNo.Size = new System.Drawing.Size(612, 23);
            this.lblUseGooglePOIYesOrNo.TabIndex = 17;
            this.lblUseGooglePOIYesOrNo.Text = "How far from your location do you want to get POI?";
            // 
            // cmbPOIFromAPIDistanceKM
            // 
            this.cmbPOIFromAPIDistanceKM.Location = new System.Drawing.Point(40, 60);
            this.cmbPOIFromAPIDistanceKM.Name = "cmbPOIFromAPIDistanceKM";
            this.cmbPOIFromAPIDistanceKM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPOIFromAPIDistanceKM.Properties.Appearance.Options.UseFont = true;
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceDisabled.Options.UseFont = true;
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceFocused.Options.UseFont = true;
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceItemDisabled.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceItemDisabled.Options.UseFont = true;
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceItemHighlight.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceItemHighlight.Options.UseFont = true;
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceItemSelected.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceItemSelected.Options.UseFont = true;
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPOIFromAPIDistanceKM.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cmbPOIFromAPIDistanceKM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPOIFromAPIDistanceKM.Size = new System.Drawing.Size(81, 24);
            this.cmbPOIFromAPIDistanceKM.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(127, 60);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(159, 24);
            this.labelControl1.TabIndex = 19;
            this.labelControl1.Text = "Kilometer(s)";
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 43;
            // 
            // PanelWiz10DistanceFromBiz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.cmbPOIFromAPIDistanceKM);
            this.Controls.Add(this.lblUseGooglePOIYesOrNo);
            this.Name = "PanelWiz10DistanceFromBiz";
            this.Size = new System.Drawing.Size(662, 101);
            this.Tag = "9|11";
            this.Load += new System.EventHandler(this.Panel8DistanceFromBiz_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmbPOIFromAPIDistanceKM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblUseGooglePOIYesOrNo;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPOIFromAPIDistanceKM;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
    }
}
