﻿using System;
using System.ComponentModel;

namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz13YoutubeURLAndImageLoc : DevExpress.XtraEditors.XtraUserControl
    {
        string TASKNAME;
        BackgroundWorker bw = new BackgroundWorker();

        public PanelWiz13YoutubeURLAndImageLoc()
        {
            InitializeComponent();

            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;

            bw.DoWork += bw_DoWork;
            bw.ProgressChanged += bw_ProgressChanged;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            mpbcBusy.Visible = false;
            
            if (TASKNAME.Equals("ReadFile") && yt_urls_from_file.Count > 0)
            {
                FileToRead = "";
                TryLoadVids();
            }
            
        }

        private void TryLoadVids()
        {
            if (yt_urls_from_file != null)
            {
                if (yt_urls_from_file.Count > 0)
                {
                    for (int x = 0; x < yt_urls_from_file.Count; x++)
                    {
                        AddAnURL(yt_urls_from_file[x]);
                    }
                }
            }

            ManageNoVidsView();
        }

        private void ManageNoVidsView()
        {
            int vid_cards = 0;
            for (int x = 0; x < xscYoutubeURLCardsHolder.Controls.Count; x++)
            {
                if (xscYoutubeURLCardsHolder.Controls[x] is  PanelYoutubeURLModel) vid_cards++;
            }

            if (vid_cards == 0)
            {
                for (int x = xscYoutubeURLCardsHolder.Controls.Count-1; x>=0; x--) xscYoutubeURLCardsHolder.Controls.RemoveAt(x);
                PanelNoVideo pnv = new PanelNoVideo();
                xscYoutubeURLCardsHolder.Controls.Add(pnv);
                xscYoutubeURLCardsHolder.VerticalScroll.Visible = false;
            }
            else
            {
               if (vid_cards>6)
                {
                    xscYoutubeURLCardsHolder.VerticalScroll.Visible = true;
                }
                else
                {
                    xscYoutubeURLCardsHolder.VerticalScroll.Visible = false;
                }
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //Me.Text = e.ProgressPercentage.ToString() & "% completed"
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker worker = (BackgroundWorker)sender;

                if (bw.CancellationPending == true) e.Cancel = true;
                if (TASKNAME.Equals("ReadFile")) ReadFile();
            }
            catch
            {
                if (bw.WorkerSupportsCancellation == true)
                {
                    mpbcBusy.Visible = false;
                    bw.CancelAsync();
                }
            }
        }

        String FileToRead = "";
        System.Collections.Generic.List<String> yt_urls_from_file = new System.Collections.Generic.List<String>();
        private void ReadFile()
        {
            yt_urls_from_file = new System.Collections.Generic.List<String>();

            System.IO.StreamReader file = new System.IO.StreamReader(@FileToRead);
            String yt_url = "";
            int counter = 0;
            while ((yt_url = file.ReadLine()) != null)
            {
                if (!IfLinkTaken(yt_url))
                { 
                    yt_urls_from_file.Add(yt_url);
                    counter++;
                }
            }
            file.Close();           
        }

        private void btnEdtOtherFeaturesFolderOfPhotos_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string dPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            DevExpress.XtraEditors.XtraFolderBrowserDialog folderBrowserDialog = new DevExpress.XtraEditors.XtraFolderBrowserDialog();
            folderBrowserDialog.Title = "NMM - Select Folder";
            folderBrowserDialog.UseParentFormIcon = true;
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.Desktop;
            folderBrowserDialog.ShowNewFolderButton = true;

            string sel_path = "";
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                sel_path = folderBrowserDialog.SelectedPath;
                this.btnEdtOtherFeaturesFolderOfPhotos.Text = sel_path;
            }

            string sql = "UPDATE tbl_projects SET FolderOfPhotosToUse = '"
                + sel_path + "' WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
            int res = clsSQLiteCRUD.UPDATE_ROWS(sql);
                
        }

        private void btnAddUpdateYTURLOK_Click(object sender, EventArgs e)
        {
            String yt_url = txtOtherFeaturesYoutubeURL.Text.Trim();
            if (yt_url.Equals("")) return;

            SaveUpdate(yt_url);
        }

        private Boolean IfLinkTaken(String url)
        {
            int existing_count = 0;
            for (int x = 0; x < xscYoutubeURLCardsHolder.Controls.Count; x++)
            {
                if (xscYoutubeURLCardsHolder.Controls[x] is DevExpress.XtraEditors.XtraUserControl && xscYoutubeURLCardsHolder.Controls[x].Name.Equals("PanelYoutubeURLModel"))
                {
                    DevExpress.XtraEditors.HyperlinkLabelControl label_url =
                        (DevExpress.XtraEditors.HyperlinkLabelControl)xscYoutubeURLCardsHolder.Controls[x].Controls["lblPOICardTitle"];
                    String curURL = label_url.Tag.ToString();
                    if (curURL.Equals(url)) existing_count++;
                }
            }

            if (existing_count > 0) return true;

            return false;
        }

        private string if_ok_to_add(string yt_url)
        {
            if (!yt_url.ToLower().Contains("youtube.com/watch")) return "Seems to be an invalid youtube URL!";
            if (IfLinkTaken(yt_url)) return "URL already has been taken!";

            string stmt = "SELECT Count(*) AS CNT FROM tbl_vids WHERE ProjectID = " + Globals.VAR_PROJECT_ID.ToString() 
                + " AND VidLink = '"+yt_url+"';";
            string v = clsSQLiteCRUD.GetACellVal(stmt);
            if (Int32.Parse(v) > 0) return "URL already has been taken!";

            return "";
        }

        private void SaveUpdate(string yt_url) {
            
            DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
            args.Caption = Globals.AppName;
            args.Text = "";
            args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.OK };
            //args.Icon = System.Drawing.SystemIcons.Information;

            string ok_res = if_ok_to_add(yt_url);
            if (!ok_res.Equals(""))
            {
                args.Text = Environment.NewLine + ok_res + Environment.NewLine;
                DevExpress.XtraEditors.XtraMessageBox.Show(args);
                return;
            }

            Boolean if_updated = false;
            for (int x = 0; x < xscYoutubeURLCardsHolder.Controls.Count; x++)
            {
                if (xscYoutubeURLCardsHolder.Controls[x] is DevExpress.XtraEditors.XtraUserControl 
                    && xscYoutubeURLCardsHolder.Controls[x].Name.Equals("PanelYoutubeURLModel"))
                {
                    DevExpress.XtraEditors.XtraUserControl vid_holder 
                        = (DevExpress.XtraEditors.XtraUserControl)xscYoutubeURLCardsHolder.Controls[x];

                    DevExpress.XtraEditors.HyperlinkLabelControl label_url 
                        = (DevExpress.XtraEditors.HyperlinkLabelControl)vid_holder.Controls["lblPOICardTitle"];
                    System.Windows.Forms.PictureBox picEdit = (System.Windows.Forms.PictureBox)vid_holder.Controls["picEditYoutubeURL"];
                    System.Windows.Forms.PictureBox picDelete = (System.Windows.Forms.PictureBox)vid_holder.Controls["picDeleteURL"];

                    string curURL = picEdit.Tag.ToString();
                    if (curURL.Equals(Globals.WORKING_VID_URL))
                    {
                        label_url.Text = "<href=" + yt_url + ">" + yt_url + "</href>";
                        label_url.Tag = yt_url;

                        picEdit.Tag = yt_url;
                        picDelete.Tag = yt_url;
                        
                        string sql = "UPDATE tbl_vids SET VidLink = '" + yt_url
                            + "' WHERE VidLink = '" + curURL
                            + "' AND ProjectID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                        int res = clsSQLiteCRUD.UPDATE_ROWS(sql);

                        Globals.WORKING_VID_URL = "";
                        if_updated = true;
                    }
                }
            }

            if (if_updated == false) AddAnURL(yt_url);

            //and finally
            txtOtherFeaturesYoutubeURL.Text = "";
            txtOtherFeaturesYoutubeURL.Focus();
        }

        protected void GoEdit(object sender, EventArgs e)
        {
            String my_vid_link = GetURLByPicTag(sender);
            txtOtherFeaturesYoutubeURL.Text = my_vid_link;
            Globals.WORKING_VID_URL = my_vid_link;
        }

        private String GetURLByPicTag(object sender)
        {
            System.Windows.Forms.PictureBox cur_img = (System.Windows.Forms.PictureBox)sender;
            return cur_img.Tag.ToString();
        }
        protected void GDelete(object sender, EventArgs e)
        {
            string my_vid_link = GetURLByPicTag(sender);
            
            DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
            args.Caption = Globals.AppName;
            args.Text = "The following youtube link will be deleted permanently-" 
                + Environment.NewLine + Environment.NewLine + my_vid_link + Environment.NewLine + Environment.NewLine
                + "This action is irreversible."
                + Environment.NewLine + "Are you sure to proceed?" + Environment.NewLine;
            args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.Yes, System.Windows.Forms.DialogResult.No };
            //args.Icon = System.Drawing.SystemIcons.Question;
            if (DevExpress.XtraEditors.XtraMessageBox.Show(args) == System.Windows.Forms.DialogResult.No) return;

            for (int x = 0; x < xscYoutubeURLCardsHolder.Controls.Count; x++)
            {
                if (xscYoutubeURLCardsHolder.Controls[x] is DevExpress.XtraEditors.XtraUserControl 
                    && xscYoutubeURLCardsHolder.Controls[x].Name.Equals("PanelYoutubeURLModel"))
                {
                   System.Windows.Forms.PictureBox pbDel =
                        (System.Windows.Forms.PictureBox)xscYoutubeURLCardsHolder.Controls[x].Controls["picDeleteURL"];
                    string curURL = pbDel.Tag.ToString();

                    if (curURL.Equals(my_vid_link))
                    {

                        //Delete from DB
                        string sql = "DELETE FROM tbl_vids WHERE VidLink = '"+ curURL + "' AND ProjectID = "
                            + Globals.VAR_PROJECT_ID.ToString() + ";";
                        int ret = clsSQLiteCRUD.UPDATE_ROWS(sql);

                        //Delete from card holder panel
                        xscYoutubeURLCardsHolder.Controls.RemoveAt(x);

                        //Reset Row Number
                        ResetRowNums();

                        break;
                    }

                }
            }
        }

        private void ResetRowNums()
        {

            if (xscYoutubeURLCardsHolder.Controls.Count>0)
            {
                for (int x = 0; x< xscYoutubeURLCardsHolder.Controls.Count; x++)
                {
                    DevExpress.XtraEditors.XtraUserControl one_card = 
                        (DevExpress.XtraEditors.XtraUserControl) xscYoutubeURLCardsHolder.Controls[x];

                    DevExpress.XtraEditors.LabelControl one_card_sl =
                        (DevExpress.XtraEditors.LabelControl)one_card.Controls["lblOneYoutubeURLNum"];
                    one_card_sl.Text = (x+1).ToString();

                    int model_y = (x * one_card.Height) + (x * 5);
                    one_card.SetBounds(0, model_y, one_card.Width, one_card.Height);

                }
            }

            ManageNoVidsView();
        }

        private void AddAnURL(String yt_url)
        {
            string ok_res = if_ok_to_add(yt_url);
            if (!ok_res.Equals("")) return;

           
            for (int x = 0; x < xscYoutubeURLCardsHolder.Controls.Count; x++)
            {
                if (xscYoutubeURLCardsHolder.Controls[x] is PanelYoutubeURLModel)
                {
                    xscYoutubeURLCardsHolder.Controls.RemoveAt(x);
                }
            }

            //remove no res model
            int rows_now = 0;
            for (int x = xscYoutubeURLCardsHolder.Controls.Count - 1; x >= 0; x--)
            {
                if (xscYoutubeURLCardsHolder.Controls[x] is PanelYoutubeURLModel) rows_now++;
            }

            PanelYoutubeURLModel ucYTModel = new PanelYoutubeURLModel();
            DevExpress.XtraEditors.LabelControl vid_row_num = (DevExpress.XtraEditors.LabelControl)ucYTModel.Controls["lblOneYoutubeURLNum"];
            vid_row_num.Text = (rows_now + 1).ToString();
            DevExpress.XtraEditors.HyperlinkLabelControl vid_url = (DevExpress.XtraEditors.HyperlinkLabelControl)ucYTModel.Controls["lblPOICardTitle"];
            vid_url.Text = "<href=" + yt_url + ">" + yt_url + "</href>";
            vid_url.Tag = yt_url;


            if (xscYoutubeURLCardsHolder.Controls.Count > 0)
            {
                for (int d = xscYoutubeURLCardsHolder.Controls.Count - 1; d >= 0; d--)
                {
                    if (xscYoutubeURLCardsHolder.Controls[d] is PanelYoutubeURLModel)
                    {
                        int curTop = xscYoutubeURLCardsHolder.Controls[d].Location.Y;
                        int newTop = curTop + 5 + xscYoutubeURLCardsHolder.Controls[d].Height;
                        int cWidth = xscYoutubeURLCardsHolder.Controls[d].Width;
                        int cHeight = xscYoutubeURLCardsHolder.Controls[d].Height;
                        xscYoutubeURLCardsHolder.Controls[d].SetBounds(0, newTop, cWidth, cHeight);
                    }
                }
            }


            xscYoutubeURLCardsHolder.Controls.Add(ucYTModel);
            //int model_y = (rows_now * ucYTModel.Height) + (rows_now * 5);
            ucYTModel.SetBounds(0, 0, ucYTModel.Width, ucYTModel.Height);

            //event listener
            System.Windows.Forms.PictureBox picEdit = (System.Windows.Forms.PictureBox)ucYTModel.Controls["picEditYoutubeURL"];
            picEdit.Tag = yt_url;
            picEdit.Click += new EventHandler(GoEdit);
            System.Windows.Forms.PictureBox picDelete = (System.Windows.Forms.PictureBox)ucYTModel.Controls["picDeleteURL"];
            picDelete.Tag = yt_url;
            picDelete.Click += new EventHandler(GDelete);

            ResetRowNums();

            string sql = "INSERT INTO tbl_vids (ProjectID, VidLink) VALUES ("
                + Globals.VAR_PROJECT_ID.ToString() + ", '"
                + yt_url + "');";
            int res = clsSQLiteCRUD.INSERT_ROWS(sql, false);
        }

        private void btnLoadYTURLFromFile_Click(object sender, EventArgs e)
        {
            if (bw.IsBusy) return;

            FileToRead = "";

            DevExpress.XtraEditors.XtraOpenFileDialog openFileDialog = new DevExpress.XtraEditors.XtraOpenFileDialog();
            //openFileDialog.Filter = "Image Files(*.PNG; *.BMP; *.JPG; *.GIF)| *.PNG; *.BMP; *.JPG; *.GIF";
            //openFileDialog.Filter = "Image Files(*.png)| *.png";
            openFileDialog.Filter = "Text Files(*.txt)|*.txt";
            openFileDialog.Multiselect = false;
            openFileDialog.CheckFileExists = true;
            openFileDialog.FilterIndex = 0;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) FileToRead = openFileDialog.FileName;
            if (!System.IO.File.Exists(FileToRead)) return;

            TASKNAME = "ReadFile";
            mpbcBusy.Visible = true;
            bw.RunWorkerAsync();

            //ReadFile();
        }

        private void btnRemoveAllYoutubeURL_Click(object sender, EventArgs e)
        {
            int ex_urls = 0;
            for (int x = 0; x < xscYoutubeURLCardsHolder.Controls.Count; x++)
            {
                if (xscYoutubeURLCardsHolder.Controls[x] is DevExpress.XtraEditors.XtraUserControl && xscYoutubeURLCardsHolder.Controls[x].Name.Equals("PanelYoutubeURLModel"))
                {
                    ex_urls++;
                }
            }

            if (ex_urls == 0) return;

            DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
            args.Caption = Globals.AppName;
            args.Text = Environment.NewLine 
                + "Are you sure to remove " + (ex_urls.ToString()) + " link" + (ex_urls > 1 ? "s" : "") + "?" 
                + Environment.NewLine;
            args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.Yes, System.Windows.Forms.DialogResult.No };
            //args.Icon = System.Drawing.SystemIcons.Question;
            if (DevExpress.XtraEditors.XtraMessageBox.Show(args) == System.Windows.Forms.DialogResult.No) return;


            int ctrls = xscYoutubeURLCardsHolder.Controls.Count;
            if (ctrls <= 0) return;

            for (int x = ctrls-1; x >= 0; x--)
            {
                xscYoutubeURLCardsHolder.Controls.RemoveAt(x);
            }

            string sql = "DELETE FROM tbl_vids WHERE ProjectID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
            int ret = clsSQLiteCRUD.UPDATE_ROWS(sql);


            //show no res
            PanelNoVideo ucNOPOI = new PanelNoVideo();
            xscYoutubeURLCardsHolder.Controls.Add(ucNOPOI);
            ucNOPOI.SetBounds(0, 0, ucNOPOI.Width, ucNOPOI.Height);
            xscYoutubeURLCardsHolder.VerticalScroll.Visible = false;

        }

        private void fileSystemWatcher1_Changed(object sender, System.IO.FileSystemEventArgs e)
        {

        }

        private void txtOtherFeaturesYoutubeURL_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyData == System.Windows.Forms.Keys.Enter)
            {
                String yt_url = txtOtherFeaturesYoutubeURL.Text.Trim();
                if (yt_url.Equals("")) return;

                SaveUpdate(yt_url);
                //SendKeys.Send("{TAB}");
            }
        }

        private void btnClearYTField_Click(object sender, EventArgs e)
        {
            txtOtherFeaturesYoutubeURL.Text = "";
            txtOtherFeaturesYoutubeURL.Focus();
        }

        private void PanelWiz13YoutubeURLAndImageLoc_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            DevExpress.XtraEditors.XtraForm.CheckForIllegalCrossThreadCalls = false;

            this.btnEdtOtherFeaturesFolderOfPhotos.Focus();

            PanelNoVideo pnv = new PanelNoVideo();
            xscYoutubeURLCardsHolder.Controls.Add(pnv);
        }
    }
}
