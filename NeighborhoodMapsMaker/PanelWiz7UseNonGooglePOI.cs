﻿
namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz7UseNonGooglePOI : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelWiz7UseNonGooglePOI()
        {
            InitializeComponent();
        }

        private void DecideNav()
        {
            string p_prev = "3";
            if (Globals.VAR_USE_GOOGLE_POI == true)
            {
                p_prev = "6";
            }

            string p_next = "12";
            Globals.VAR_USE_NON_GOOGLE_POI = false;
            if (chkUseUnused.Checked)
            {
                p_next = "8";
                Globals.VAR_USE_NON_GOOGLE_POI = true;
            }

            this.Tag = p_prev + "|" + p_next;
        }

        private void Panel7UseNonGooglePOI_Load(object sender, System.EventArgs e)
        {
            DecideNav();
        }

        private void chkUseUnused_EditValueChanged(object sender, System.EventArgs e)
        {
            DecideNav();
            Globals.VAR_USE_NON_GOOGLE_POI = (chkUseUnused.Checked);
        }
    }
}
