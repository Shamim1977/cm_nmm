﻿
namespace NeighborhoodMapsMaker
{
    public partial class PanelModelPOI : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelModelPOI()
        {
            InitializeComponent();
        }

        private void lblPOICardTitle_Click(object sender, System.EventArgs e)
        {
            string v = lblPOICardTitle.Tag.ToString();
            if (v.Equals("")) return;
            if (!v.Contains("http") && !v.Contains("www")) return;

            System.Diagnostics.Process.Start(v);
        }
    }
}
