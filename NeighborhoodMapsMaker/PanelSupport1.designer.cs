﻿namespace NeighborhoodMapsMaker
{
    partial class PanelSupport1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelSupport1));
            this.lblSupportSubHeader = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.lblSupportHeader = new DevExpress.XtraEditors.LabelControl();
            this.btnGoToSupport = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSupportSubHeader
            // 
            this.lblSupportSubHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSupportSubHeader.Appearance.Options.UseFont = true;
            this.lblSupportSubHeader.Appearance.Options.UseTextOptions = true;
            this.lblSupportSubHeader.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblSupportSubHeader.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblSupportSubHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblSupportSubHeader.Location = new System.Drawing.Point(49, 60);
            this.lblSupportSubHeader.Name = "lblSupportSubHeader";
            this.lblSupportSubHeader.Size = new System.Drawing.Size(658, 23);
            this.lblSupportSubHeader.TabIndex = 41;
            this.lblSupportSubHeader.Text = "ChadConnects support is a 24x7 service. Are you facing any difficulty?";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(40, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(3, 70);
            this.label1.TabIndex = 40;
            this.label1.Text = "label1";
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 43;
            // 
            // lblSupportHeader
            // 
            this.lblSupportHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSupportHeader.Appearance.Options.UseFont = true;
            this.lblSupportHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblSupportHeader.Location = new System.Drawing.Point(40, 6);
            this.lblSupportHeader.Name = "lblSupportHeader";
            this.lblSupportHeader.Size = new System.Drawing.Size(274, 23);
            this.lblSupportHeader.TabIndex = 42;
            this.lblSupportHeader.Text = "Customer Support";
            // 
            // btnGoToSupport
            // 
            this.btnGoToSupport.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoToSupport.Appearance.Options.UseFont = true;
            this.btnGoToSupport.Location = new System.Drawing.Point(49, 96);
            this.btnGoToSupport.Name = "btnGoToSupport";
            this.btnGoToSupport.Size = new System.Drawing.Size(186, 34);
            this.btnGoToSupport.TabIndex = 44;
            this.btnGoToSupport.Text = "Take Me to Support Page";
            this.btnGoToSupport.Click += new System.EventHandler(this.btnGoToSupport_Click);
            // 
            // PanelSupport1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnGoToSupport);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblSupportHeader);
            this.Controls.Add(this.lblSupportSubHeader);
            this.Controls.Add(this.label1);
            this.Name = "PanelSupport1";
            this.Size = new System.Drawing.Size(736, 150);
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblSupportSubHeader;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.LabelControl lblSupportHeader;
        private DevExpress.XtraEditors.SimpleButton btnGoToSupport;
    }
}
