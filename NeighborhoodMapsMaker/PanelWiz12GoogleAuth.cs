﻿using System;

namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz12GoogleAuth : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelWiz12GoogleAuth()
        {
            InitializeComponent();
        }

        private void Panel9GoogleLogin_Load(object sender, EventArgs e)
        {
            this.txtGoogleAuthEmail.Focus();
            DecideNav();
        }

        private void DecideNav()
        {
            string p_prev = "6";
            if (Globals.VAR_USE_NON_GOOGLE_POI == true) p_prev = "7";
            this.Tag = p_prev+"|13";

        }

        private void txtGoogleAuthEmail_Leave(object sender, EventArgs e)
        {
            ValidateEmail(txtGoogleAuthEmail);
        }

        private void ValidateEmail(DevExpress.XtraEditors.TextEdit TextFieldName)
        {
            string v = TextFieldName.Text.Trim();
            if (v.Equals("")) return;

            v = v.ToLower();

            if (!Globals.IS_VALID_EMAIL_ID(v))
            {
                TextFieldName.Focus();
                ShowAMessage("Seems to be an inalid Email ID!");
                return;
            }

            if (txtGoogleAuthEmail.Text.Trim().ToLower().Equals(txtGoogleAuthRecoEmail.Text.Trim().ToLower()))
            {
                TextFieldName.Focus();
                ShowAMessage("Both Email ID and Recovery Emial ID cannot be the same!");
            }

        }
                private void ShowAMessage(string msg)
        {
            DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
            args.Caption = Globals.AppName;
            args.Text = Environment.NewLine + Environment.NewLine
                + msg
                + Environment.NewLine + Environment.NewLine;
            args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.OK };
            //args.Icon = System.Drawing.SystemIcons.Question;
            DevExpress.XtraEditors.XtraMessageBox.Show(args);
        }

        private void txtGoogleAuthRecoEmail_Leave(object sender, EventArgs e)
        {
            ValidateEmail(txtGoogleAuthRecoEmail);
        }

        private void chkRememberMe_EditValueChanged(object sender, EventArgs e)
        {
            //Set to Pref if required
            if (chkRememberMe.Checked)
            {
                Properties.Settings.Default.SETT_GOOGLE_EMAIL = this.txtGoogleAuthEmail.Text.Trim();
                Properties.Settings.Default.SETT_GOGGLE_PASS = this.txtGoogleAuthPass.Text.Trim();
                Properties.Settings.Default.SETT_GOGGLE_ALT_EMAIL = this.txtGoogleAuthRecoEmail.Text.Trim();
            }
            else
            {
                Properties.Settings.Default.SETT_GOOGLE_EMAIL = "";
                Properties.Settings.Default.SETT_GOGGLE_PASS = "";
                Properties.Settings.Default.SETT_GOGGLE_ALT_EMAIL = "";

            }
            Properties.Settings.Default.Save();
        }

        private void btnShowHidePass_Click(object sender, EventArgs e)
        {
            if (this.txtGoogleAuthPass.Text.Trim().Equals("")) return;


            string v = this.btnShowHidePass.Tag.ToString();

            if (v.Equals("Show"))
            {
                this.txtGoogleAuthPass.Properties.PasswordChar = '\0';
                btnShowHidePass.Tag = "Hide";
            }
            else
            {
                this.txtGoogleAuthPass.Properties.PasswordChar = '*';
                btnShowHidePass.Tag = "Show";
            }
        }
    }
}
