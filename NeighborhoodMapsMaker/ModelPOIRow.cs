﻿using System;

namespace NeighborhoodMapsMaker
{
    class ModelPOIRow
    {

        Boolean POICheck;
        int POINumber;
        int POIRowID;
        String POITitle;

        public ModelPOIRow(Boolean Check, int POINumber, int POIRowID, String POITitle)
        {
            this.POICheck = Check;
            this.POINumber = POINumber;
            this.POIRowID = POIRowID;
            this.POITitle = POITitle;
                
        }

        public Boolean POI_Check
        {
            get { return POICheck; }
            set { POICheck = value; }
        }

        public int POI_RowNumber
        {
            get { return POINumber; }
            set { POINumber = value; }
        }

        public int POI_DBRID
        {
            get { return POIRowID; }
            set { POIRowID = value; }
        }

        public string POI_Title
        {
            get { return POITitle; }
            set { POITitle = value; }
        }

    }
}
