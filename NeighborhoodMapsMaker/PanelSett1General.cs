﻿using Microsoft.Win32;
using System;
using System.IO;

namespace NeighborhoodMapsMaker
{
    public partial class PanelSett1General : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelSett1General()
        {
            InitializeComponent();
        }

        private void Panel1SettGeneral_Load(object sender, EventArgs e)
        {
            picArticleBuilderDotNet.Click += new EventHandler(GoToSite);
            picFancyUSEPOI.Click += new EventHandler(GoToSite);
            picGooglePlaces.Click += new EventHandler(GoToSite);
            picSpinnerChief.Click += new EventHandler(GoToSite);
            picSpinnerRewriter.Click += new EventHandler(GoToSite);
            picWordAI.Click += new EventHandler(GoToSite);
        }

        protected void GoToSite(object sender, EventArgs e)
        {
            System.Windows.Forms.PictureBox pb = (System.Windows.Forms.PictureBox)sender;
            string v = pb.Tag.ToString();
            if (!v.Contains("http") && !v.Contains("wwww")) return;
            System.Diagnostics.Process.Start(v);
        }

        private void RegisterInStartup(bool isChecked)
        {
            String AppPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Neighborhood Maps Maker.exe";
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (isChecked)
            {
                registryKey.SetValue("NMM", AppPath);
            }
            else
            {
                registryKey.DeleteValue("NMM");
            }
        }

        private void txtUserEmail_Leave(object sender, EventArgs e)
        {
            string v = txtUserEmail.Text.Trim();
            if (v.Equals("")) return;

            if (!Globals.IS_VALID_EMAIL_ID(v))
            {
                txtUserEmail.Focus();
                DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
                args.Caption = Globals.AppName;
                args.Text = Environment.NewLine + "Seems to be an invalid Email ID!" + Environment.NewLine;
                args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.OK };
                DevExpress.XtraEditors.XtraMessageBox.Show(args);

            }

        }

        private void btnSett1Next_Click(object sender, EventArgs e)
        {
            int res = SaveGenSettingsOne();

            DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
            args.Caption = Globals.AppName;
            args.Text =  Environment.NewLine + Environment.NewLine
                + (res ==0?"Failed saving settings":"Setting has been saved successfully.")
                + Environment.NewLine + Environment.NewLine;
            args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.OK };
            //args.Icon = System.Drawing.SystemIcons.Question;
            DevExpress.XtraEditors.XtraMessageBox.Show(args);

        }

        private int SaveGenSettingsOne()
        {
            int res = 0;

            RegisterInStartup(chkAutoLaunch.Checked);

            string EmailID = txtUserEmail.Text.Trim();
            String v_g_api_key = txtGooglePlacesAPIKey.Text.Trim();
            String v_ar_b_key = txtArticleBuilderAPIKey.Text.Trim();
            String v_spin_w_key = txtSpinWriterAPIKey.Text.Trim();
            String v_sp_chief_key = txtSpinnerChiefAPIKey.Text.Trim();
            String v_word_ai_key = txtWordAIPIKey.Text.Trim();


            string stmt = "UPDATE tbl_settings SET "
                + "StartWithOS = " + (chkAutoLaunch.Checked ? 1 : 0) + ", "
                + "StartSizeNormal = " + (chkStartSize.Checked ? 0 : 1) + ", "
                + "UserEmail = " + (EmailID.Equals("") ? "NULL" : "'" + EmailID.ToLower() + "'") + ", "
                + "GooglePlacesAPIKey = " + (v_g_api_key.Equals("") ? "NULL" : "'" + v_g_api_key + "'") + ", "
                + "ArticleBuilderDotNetAPIKey = " + (v_ar_b_key.Equals("") ? "NULL" : "'" + v_ar_b_key + "'") + ", "
                + "SpinRewriterAPIKey = " + (v_spin_w_key.Equals("") ? "NULL" : "'" + v_spin_w_key + "'") + ", "
                + "SpinnerChiefAPIKey = " + (v_sp_chief_key.Equals("") ? "NULL" : "'" + v_sp_chief_key + "'") + ", "
                + "WordAIAPIKey = " + (v_word_ai_key.Equals("") ? "NULL" : "'" + v_word_ai_key + "'") + " "
                + "WHERE UserID = " + Globals.USER_ID.ToString() + ";";

            res = clsSQLiteCRUD.UPDATE_ROWS(stmt);

            return res;

        }
    }
}
