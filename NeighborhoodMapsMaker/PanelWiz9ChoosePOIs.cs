﻿
namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz9ChoosePOIs : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelWiz9ChoosePOIs()
        {
            InitializeComponent();
        }

        private void chkUseUnused_EditValueChanged(object sender, System.EventArgs e)
        {
            chkLetMeChoosePOI.Checked = !chkUseUnused.Checked;
        }

        private void chkLetMeChoosePOI_EditValueChanged(object sender, System.EventArgs e)
        {
            chkUseUnused.Checked = !chkLetMeChoosePOI.Checked;
        }
    }
}
