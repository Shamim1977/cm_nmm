﻿using System;

namespace NeighborhoodMapsMaker
{
    public partial class PanelTraining1 : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelTraining1()
        {
            InitializeComponent();
        }

        private void btnGoToTraining_Click(object sender, System.EventArgs e)
        {
            if (Globals.CheckNet() == false)
            {
                DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
                args.Caption = Globals.AppName;
                args.Text = Environment.NewLine + Environment.NewLine 
                    + "No Internet Access!"
                    + Environment.NewLine + Environment.NewLine;
                args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.OK};
                DevExpress.XtraEditors.XtraMessageBox.Show(args);

                return;
            }

            System.Diagnostics.Process.Start(Globals.TutorialPage);
        }
    }
}
