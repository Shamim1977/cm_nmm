﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz6ScrapePOIs
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz6ScrapePOIs));
            this.pnlPOICardsHolder = new DevExpress.XtraEditors.XtraScrollableControl();
            this.lblScrapingCaution = new DevExpress.XtraEditors.LabelControl();
            this.btnShowMeWhatsAvailable = new DevExpress.XtraEditors.SimpleButton();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.lblOtherPOIHeader = new DevExpress.XtraEditors.LabelControl();
            this.btnDeleteSelected = new DevExpress.XtraEditors.SimpleButton();
            this.btnCheckUncheck = new DevExpress.XtraEditors.SimpleButton();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.tmrCardCreator = new System.Windows.Forms.Timer(this.components);
            this.chkKeepChromeVisisble = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKeepChromeVisisble.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPOICardsHolder
            // 
            this.pnlPOICardsHolder.FireScrollEventOnMouseWheel = true;
            this.pnlPOICardsHolder.Location = new System.Drawing.Point(40, 138);
            this.pnlPOICardsHolder.Name = "pnlPOICardsHolder";
            this.pnlPOICardsHolder.ScrollBarSmallChange = 128;
            this.pnlPOICardsHolder.Size = new System.Drawing.Size(818, 284);
            this.pnlPOICardsHolder.TabIndex = 7;
            // 
            // lblScrapingCaution
            // 
            this.lblScrapingCaution.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScrapingCaution.Appearance.Options.UseFont = true;
            this.lblScrapingCaution.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblScrapingCaution.Location = new System.Drawing.Point(40, 92);
            this.lblScrapingCaution.Name = "lblScrapingCaution";
            this.lblScrapingCaution.Size = new System.Drawing.Size(818, 18);
            this.lblScrapingCaution.TabIndex = 22;
            this.lblScrapingCaution.Text = "Pressing this button will start the program scraping. Please leave it alone until" +
    " it finishes";
            // 
            // btnShowMeWhatsAvailable
            // 
            this.btnShowMeWhatsAvailable.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowMeWhatsAvailable.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnShowMeWhatsAvailable.Appearance.Options.UseFont = true;
            this.btnShowMeWhatsAvailable.Appearance.Options.UseForeColor = true;
            this.btnShowMeWhatsAvailable.Location = new System.Drawing.Point(40, 55);
            this.btnShowMeWhatsAvailable.Name = "btnShowMeWhatsAvailable";
            this.btnShowMeWhatsAvailable.Size = new System.Drawing.Size(204, 30);
            this.btnShowMeWhatsAvailable.TabIndex = 21;
            this.btnShowMeWhatsAvailable.Text = "&Show Me What\'s Available";
            this.btnShowMeWhatsAvailable.Click += new System.EventHandler(this.btnShowMeWhatsAvailable_Click);
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 47;
            // 
            // lblOtherPOIHeader
            // 
            this.lblOtherPOIHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherPOIHeader.Appearance.Options.UseFont = true;
            this.lblOtherPOIHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblOtherPOIHeader.Location = new System.Drawing.Point(40, 6);
            this.lblOtherPOIHeader.Name = "lblOtherPOIHeader";
            this.lblOtherPOIHeader.Size = new System.Drawing.Size(432, 23);
            this.lblOtherPOIHeader.TabIndex = 46;
            this.lblOtherPOIHeader.Text = "Google Places of Interest (POI)";
            // 
            // btnDeleteSelected
            // 
            this.btnDeleteSelected.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteSelected.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            this.btnDeleteSelected.Appearance.Options.UseFont = true;
            this.btnDeleteSelected.Appearance.Options.UseForeColor = true;
            this.btnDeleteSelected.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSelected.ImageOptions.Image")));
            this.btnDeleteSelected.Location = new System.Drawing.Point(765, 55);
            this.btnDeleteSelected.Name = "btnDeleteSelected";
            this.btnDeleteSelected.Size = new System.Drawing.Size(33, 30);
            this.btnDeleteSelected.TabIndex = 50;
            this.btnDeleteSelected.ToolTip = "Delete Selected";
            this.btnDeleteSelected.Visible = false;
            this.btnDeleteSelected.Click += new System.EventHandler(this.btnDeleteSelected_Click);
            // 
            // btnCheckUncheck
            // 
            this.btnCheckUncheck.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckUncheck.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            this.btnCheckUncheck.Appearance.Options.UseFont = true;
            this.btnCheckUncheck.Appearance.Options.UseForeColor = true;
            this.btnCheckUncheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCheckUncheck.ImageOptions.Image")));
            this.btnCheckUncheck.Location = new System.Drawing.Point(726, 55);
            this.btnCheckUncheck.Name = "btnCheckUncheck";
            this.btnCheckUncheck.Size = new System.Drawing.Size(33, 30);
            this.btnCheckUncheck.TabIndex = 51;
            this.btnCheckUncheck.Tag = "Unchecked";
            this.btnCheckUncheck.ToolTip = "Check / Uncheck All";
            this.btnCheckUncheck.Visible = false;
            this.btnCheckUncheck.Click += new System.EventHandler(this.btnCheckUncheck_Click);
            // 
            // tmrCardCreator
            // 
            this.tmrCardCreator.Interval = 500;
            this.tmrCardCreator.Tick += new System.EventHandler(this.tmrCardCreator_Tick);
            // 
            // chkKeepChromeVisisble
            // 
            this.chkKeepChromeVisisble.EditValue = true;
            this.chkKeepChromeVisisble.Location = new System.Drawing.Point(268, 60);
            this.chkKeepChromeVisisble.Name = "chkKeepChromeVisisble";
            this.chkKeepChromeVisisble.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkKeepChromeVisisble.Properties.Appearance.Options.UseFont = true;
            this.chkKeepChromeVisisble.Properties.Caption = "Keep Chrome Visible";
            this.chkKeepChromeVisisble.Size = new System.Drawing.Size(175, 20);
            this.chkKeepChromeVisisble.TabIndex = 52;
            // 
            // PanelWiz6ScrapePOIs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkKeepChromeVisisble);
            this.Controls.Add(this.btnCheckUncheck);
            this.Controls.Add(this.btnDeleteSelected);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblOtherPOIHeader);
            this.Controls.Add(this.lblScrapingCaution);
            this.Controls.Add(this.btnShowMeWhatsAvailable);
            this.Controls.Add(this.pnlPOICardsHolder);
            this.Name = "PanelWiz6ScrapePOIs";
            this.Size = new System.Drawing.Size(872, 431);
            this.Tag = "5|7";
            this.Load += new System.EventHandler(this.Panel6ScrapePOIs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKeepChromeVisisble.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl pnlPOICardsHolder;
        private DevExpress.XtraEditors.LabelControl lblScrapingCaution;
        private DevExpress.XtraEditors.SimpleButton btnShowMeWhatsAvailable;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.LabelControl lblOtherPOIHeader;
        private DevExpress.XtraEditors.SimpleButton btnDeleteSelected;
        private DevExpress.XtraEditors.SimpleButton btnCheckUncheck;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private System.Windows.Forms.Timer tmrCardCreator;
        private DevExpress.XtraEditors.CheckEdit chkKeepChromeVisisble;
    }
}
