﻿using System;

namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz14ArtilceManager : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelWiz14ArtilceManager()
        {
            InitializeComponent();
        }

        private void Panel11ArtilceManager_Load(object sender, EventArgs e)
        {
            String[] APIServices = {"-None-", "Spin ReWriter", "Spinner Chief", "Word AI" };
            for (int a = 0; a< APIServices.Length; a++) this.cmbSpinnerServices.Properties.Items.Add(APIServices[a]);
            cmbSpinnerServices.SelectedIndex = 2;
        }

        private void rtArticle_TextChanged(object sender, EventArgs e)
        {
        }

        private void btnGetArticle_Click(object sender, EventArgs e)
        {

        }

        private void chkSpinArticle_EditValueChanged(object sender, EventArgs e)
        {
            if (this.chkSpinArticle.Checked == false)
            {
                this.cmbSpinnerServices.SelectedIndex = 0;
                this.cmbSpinnerServices.Visible = false;
                this.btnGetArticleAndSpin.Visible = false;
            }
            else
            {
                this.cmbSpinnerServices.SelectedIndex = 2;
                this.cmbSpinnerServices.Visible = true;
                this.btnGetArticleAndSpin.Visible = true;
                cmbSpinnerServices.Focus();
            }
        }
    }
}
