﻿using System;

namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz8QtyPOI : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelWiz8QtyPOI()
        {
            InitializeComponent();
        }


        private void ShowHideControls()
        {
            if (this.chkGPOIWantAll.Checked)
            {
                this.txtNonGooglePOIQtyNumber.Visible = false;
                this.lblNonGooglePOIQtyLabel.Visible = false;
                this.lblNonGooglePOIQtyHelp.Visible = false;
            }
            else
            {
                this.txtNonGooglePOIQtyNumber.Visible = true;
                this.lblNonGooglePOIQtyLabel.Visible = true;
                this.lblNonGooglePOIQtyHelp.Visible = true;
                txtNonGooglePOIQtyNumber.Focus();
            }
        }

        private void chkGPOIWantAll_EditValueChanged(object sender, EventArgs e)
        {
            chkGPOIWantSome.Checked = !chkGPOIWantAll.Checked;
            ShowHideControls();
        }

        private void chkGPOIWantSome_EditValueChanged(object sender, EventArgs e)
        {
            chkGPOIWantAll.Checked = !chkGPOIWantSome.Checked;
            ShowHideControls();
        }
    }
}
