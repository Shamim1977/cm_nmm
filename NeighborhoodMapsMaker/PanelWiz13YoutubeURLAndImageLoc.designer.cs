﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz13YoutubeURLAndImageLoc
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz13YoutubeURLAndImageLoc));
            this.lblOtherFeaturesHead = new DevExpress.XtraEditors.LabelControl();
            this.lblOtherFeaturesYoutubeURL = new DevExpress.XtraEditors.LabelControl();
            this.lblOtherFeaturesFolderOfPhotos = new DevExpress.XtraEditors.LabelControl();
            this.btnEdtOtherFeaturesFolderOfPhotos = new DevExpress.XtraEditors.ButtonEdit();
            this.lblOtherFeaturesYoutubeURLOptional = new DevExpress.XtraEditors.LabelControl();
            this.lblOtherFeaturesFolderOfPhotosOptional = new DevExpress.XtraEditors.LabelControl();
            this.txtOtherFeaturesYoutubeURL = new DevExpress.XtraEditors.TextEdit();
            this.xscYoutubeURLCardsHolder = new DevExpress.XtraEditors.XtraScrollableControl();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.btnRemoveAllYoutubeURL = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadYTURLFromFile = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddUpdateYTURLOK = new DevExpress.XtraEditors.SimpleButton();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.mpbcBusy = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.btnClearYTField = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.btnEdtOtherFeaturesFolderOfPhotos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherFeaturesYoutubeURL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbcBusy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOtherFeaturesHead
            // 
            this.lblOtherFeaturesHead.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherFeaturesHead.Appearance.Options.UseFont = true;
            this.lblOtherFeaturesHead.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblOtherFeaturesHead.Location = new System.Drawing.Point(40, 6);
            this.lblOtherFeaturesHead.Name = "lblOtherFeaturesHead";
            this.lblOtherFeaturesHead.Size = new System.Drawing.Size(221, 23);
            this.lblOtherFeaturesHead.TabIndex = 21;
            this.lblOtherFeaturesHead.Text = "Other Features";
            // 
            // lblOtherFeaturesYoutubeURL
            // 
            this.lblOtherFeaturesYoutubeURL.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherFeaturesYoutubeURL.Appearance.Options.UseFont = true;
            this.lblOtherFeaturesYoutubeURL.Location = new System.Drawing.Point(40, 123);
            this.lblOtherFeaturesYoutubeURL.Name = "lblOtherFeaturesYoutubeURL";
            this.lblOtherFeaturesYoutubeURL.Size = new System.Drawing.Size(99, 18);
            this.lblOtherFeaturesYoutubeURL.TabIndex = 20;
            this.lblOtherFeaturesYoutubeURL.Text = "Youtube™ URL";
            // 
            // lblOtherFeaturesFolderOfPhotos
            // 
            this.lblOtherFeaturesFolderOfPhotos.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherFeaturesFolderOfPhotos.Appearance.Options.UseFont = true;
            this.lblOtherFeaturesFolderOfPhotos.Location = new System.Drawing.Point(40, 60);
            this.lblOtherFeaturesFolderOfPhotos.Name = "lblOtherFeaturesFolderOfPhotos";
            this.lblOtherFeaturesFolderOfPhotos.Size = new System.Drawing.Size(159, 18);
            this.lblOtherFeaturesFolderOfPhotos.TabIndex = 24;
            this.lblOtherFeaturesFolderOfPhotos.Text = "Folder of Photos to Use:";
            // 
            // btnEdtOtherFeaturesFolderOfPhotos
            // 
            this.btnEdtOtherFeaturesFolderOfPhotos.Location = new System.Drawing.Point(224, 60);
            this.btnEdtOtherFeaturesFolderOfPhotos.Name = "btnEdtOtherFeaturesFolderOfPhotos";
            this.btnEdtOtherFeaturesFolderOfPhotos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdtOtherFeaturesFolderOfPhotos.Properties.Appearance.Options.UseFont = true;
            this.btnEdtOtherFeaturesFolderOfPhotos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnEdtOtherFeaturesFolderOfPhotos.Size = new System.Drawing.Size(529, 24);
            this.btnEdtOtherFeaturesFolderOfPhotos.TabIndex = 0;
            this.btnEdtOtherFeaturesFolderOfPhotos.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnEdtOtherFeaturesFolderOfPhotos_ButtonClick);
            // 
            // lblOtherFeaturesYoutubeURLOptional
            // 
            this.lblOtherFeaturesYoutubeURLOptional.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherFeaturesYoutubeURLOptional.Appearance.Options.UseFont = true;
            this.lblOtherFeaturesYoutubeURLOptional.Location = new System.Drawing.Point(40, 148);
            this.lblOtherFeaturesYoutubeURLOptional.Name = "lblOtherFeaturesYoutubeURLOptional";
            this.lblOtherFeaturesYoutubeURLOptional.Size = new System.Drawing.Size(55, 14);
            this.lblOtherFeaturesYoutubeURLOptional.TabIndex = 27;
            this.lblOtherFeaturesYoutubeURLOptional.Text = "(Optional)";
            // 
            // lblOtherFeaturesFolderOfPhotosOptional
            // 
            this.lblOtherFeaturesFolderOfPhotosOptional.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherFeaturesFolderOfPhotosOptional.Appearance.Options.UseFont = true;
            this.lblOtherFeaturesFolderOfPhotosOptional.Location = new System.Drawing.Point(40, 85);
            this.lblOtherFeaturesFolderOfPhotosOptional.Name = "lblOtherFeaturesFolderOfPhotosOptional";
            this.lblOtherFeaturesFolderOfPhotosOptional.Size = new System.Drawing.Size(55, 14);
            this.lblOtherFeaturesFolderOfPhotosOptional.TabIndex = 28;
            this.lblOtherFeaturesFolderOfPhotosOptional.Text = "(Optional)";
            // 
            // txtOtherFeaturesYoutubeURL
            // 
            this.txtOtherFeaturesYoutubeURL.Location = new System.Drawing.Point(224, 123);
            this.txtOtherFeaturesYoutubeURL.Name = "txtOtherFeaturesYoutubeURL";
            this.txtOtherFeaturesYoutubeURL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOtherFeaturesYoutubeURL.Properties.Appearance.Options.UseFont = true;
            this.txtOtherFeaturesYoutubeURL.Size = new System.Drawing.Size(402, 24);
            this.txtOtherFeaturesYoutubeURL.TabIndex = 1;
            this.txtOtherFeaturesYoutubeURL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOtherFeaturesYoutubeURL_KeyDown);
            // 
            // xscYoutubeURLCardsHolder
            // 
            this.xscYoutubeURLCardsHolder.FireScrollEventOnMouseWheel = true;
            this.xscYoutubeURLCardsHolder.Location = new System.Drawing.Point(224, 166);
            this.xscYoutubeURLCardsHolder.Name = "xscYoutubeURLCardsHolder";
            this.xscYoutubeURLCardsHolder.ScrollBarSmallChange = 30;
            this.xscYoutubeURLCardsHolder.Size = new System.Drawing.Size(567, 176);
            this.xscYoutubeURLCardsHolder.TabIndex = 32;
            // 
            // toolTipController1
            // 
            this.toolTipController1.ToolTipLocation = DevExpress.Utils.ToolTipLocation.TopRight;
            // 
            // btnRemoveAllYoutubeURL
            // 
            this.btnRemoveAllYoutubeURL.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveAllYoutubeURL.Appearance.Options.UseFont = true;
            this.btnRemoveAllYoutubeURL.Location = new System.Drawing.Point(40, 254);
            this.btnRemoveAllYoutubeURL.Name = "btnRemoveAllYoutubeURL";
            this.btnRemoveAllYoutubeURL.Size = new System.Drawing.Size(159, 33);
            this.btnRemoveAllYoutubeURL.TabIndex = 34;
            this.btnRemoveAllYoutubeURL.TabStop = false;
            this.btnRemoveAllYoutubeURL.Text = "Remove All Links";
            this.btnRemoveAllYoutubeURL.Click += new System.EventHandler(this.btnRemoveAllYoutubeURL_Click);
            // 
            // btnLoadYTURLFromFile
            // 
            this.btnLoadYTURLFromFile.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadYTURLFromFile.Appearance.Options.UseFont = true;
            this.btnLoadYTURLFromFile.Location = new System.Drawing.Point(40, 215);
            this.btnLoadYTURLFromFile.Name = "btnLoadYTURLFromFile";
            this.btnLoadYTURLFromFile.Size = new System.Drawing.Size(159, 33);
            this.btnLoadYTURLFromFile.TabIndex = 35;
            this.btnLoadYTURLFromFile.TabStop = false;
            this.btnLoadYTURLFromFile.Text = "Import From Text File";
            this.btnLoadYTURLFromFile.Click += new System.EventHandler(this.btnLoadYTURLFromFile_Click);
            // 
            // btnAddUpdateYTURLOK
            // 
            this.btnAddUpdateYTURLOK.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddUpdateYTURLOK.Appearance.Options.UseFont = true;
            this.btnAddUpdateYTURLOK.Location = new System.Drawing.Point(632, 123);
            this.btnAddUpdateYTURLOK.Name = "btnAddUpdateYTURLOK";
            this.btnAddUpdateYTURLOK.Size = new System.Drawing.Size(56, 25);
            this.btnAddUpdateYTURLOK.TabIndex = 2;
            this.btnAddUpdateYTURLOK.TabStop = false;
            this.btnAddUpdateYTURLOK.Text = "&OK";
            this.btnAddUpdateYTURLOK.Click += new System.EventHandler(this.btnAddUpdateYTURLOK_Click);
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 37;
            // 
            // mpbcBusy
            // 
            this.mpbcBusy.EditValue = 0;
            this.mpbcBusy.Location = new System.Drawing.Point(40, 203);
            this.mpbcBusy.Name = "mpbcBusy";
            this.mpbcBusy.Size = new System.Drawing.Size(159, 6);
            this.mpbcBusy.TabIndex = 38;
            this.mpbcBusy.Visible = false;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            this.fileSystemWatcher1.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Changed);
            // 
            // btnClearYTField
            // 
            this.btnClearYTField.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearYTField.Appearance.Options.UseFont = true;
            this.btnClearYTField.Location = new System.Drawing.Point(694, 123);
            this.btnClearYTField.Name = "btnClearYTField";
            this.btnClearYTField.Size = new System.Drawing.Size(59, 25);
            this.btnClearYTField.TabIndex = 39;
            this.btnClearYTField.TabStop = false;
            this.btnClearYTField.Text = "&Clear";
            this.btnClearYTField.Click += new System.EventHandler(this.btnClearYTField_Click);
            // 
            // PanelWiz13YoutubeURLAndImageLoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnClearYTField);
            this.Controls.Add(this.mpbcBusy);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.btnAddUpdateYTURLOK);
            this.Controls.Add(this.btnLoadYTURLFromFile);
            this.Controls.Add(this.btnRemoveAllYoutubeURL);
            this.Controls.Add(this.xscYoutubeURLCardsHolder);
            this.Controls.Add(this.txtOtherFeaturesYoutubeURL);
            this.Controls.Add(this.lblOtherFeaturesFolderOfPhotosOptional);
            this.Controls.Add(this.lblOtherFeaturesYoutubeURLOptional);
            this.Controls.Add(this.btnEdtOtherFeaturesFolderOfPhotos);
            this.Controls.Add(this.lblOtherFeaturesFolderOfPhotos);
            this.Controls.Add(this.lblOtherFeaturesHead);
            this.Controls.Add(this.lblOtherFeaturesYoutubeURL);
            this.Name = "PanelWiz13YoutubeURLAndImageLoc";
            this.Size = new System.Drawing.Size(803, 360);
            this.Tag = "12|14";
            this.Load += new System.EventHandler(this.PanelWiz13YoutubeURLAndImageLoc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnEdtOtherFeaturesFolderOfPhotos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherFeaturesYoutubeURL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpbcBusy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblOtherFeaturesHead;
        private DevExpress.XtraEditors.LabelControl lblOtherFeaturesYoutubeURL;
        private DevExpress.XtraEditors.LabelControl lblOtherFeaturesFolderOfPhotos;
        private DevExpress.XtraEditors.ButtonEdit btnEdtOtherFeaturesFolderOfPhotos;
        private DevExpress.XtraEditors.LabelControl lblOtherFeaturesYoutubeURLOptional;
        private DevExpress.XtraEditors.LabelControl lblOtherFeaturesFolderOfPhotosOptional;
        private DevExpress.XtraEditors.TextEdit txtOtherFeaturesYoutubeURL;
        private DevExpress.XtraEditors.XtraScrollableControl xscYoutubeURLCardsHolder;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraEditors.SimpleButton btnRemoveAllYoutubeURL;
        private DevExpress.XtraEditors.SimpleButton btnLoadYTURLFromFile;
        private DevExpress.XtraEditors.SimpleButton btnAddUpdateYTURLOK;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.MarqueeProgressBarControl mpbcBusy;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private DevExpress.XtraEditors.SimpleButton btnClearYTField;
    }
}
