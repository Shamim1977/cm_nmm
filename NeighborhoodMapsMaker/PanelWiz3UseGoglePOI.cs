﻿using System;

namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz3UseGoglePOI : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelWiz3UseGoglePOI()
        {
            InitializeComponent();
        }

        private void Panel3UseGoglePOI_Load(object sender, EventArgs e)
        {
            DecideNav();
        }

        private void DecideNav()
        {
            if (chkUseGooglePOI.Checked)
            {
                this.Tag = "2|4";
            }
            else
            {
                this.Tag = "2|7";
                Globals.VAR_USE_GOOGLE_POI = false;
            }

            Globals.VAR_USE_GOOGLE_POI = chkUseGooglePOI.Checked;
        }

        private void chkUseGooglePOI_EditValueChanged(object sender, EventArgs e)
        {
            DecideNav();
        }
    }
}
