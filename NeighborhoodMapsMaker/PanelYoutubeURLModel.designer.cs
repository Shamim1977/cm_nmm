﻿namespace NeighborhoodMapsMaker
{
    partial class PanelYoutubeURLModel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelYoutubeURLModel));
            this.label1 = new System.Windows.Forms.Label();
            this.picEditYoutubeURL = new System.Windows.Forms.PictureBox();
            this.lblOneYoutubeURLNum = new DevExpress.XtraEditors.LabelControl();
            this.lblPOICardTitle = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.picDeleteURL = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picEditYoutubeURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDeleteURL)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(3, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // picEditYoutubeURL
            // 
            this.picEditYoutubeURL.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picEditYoutubeURL.Image = ((System.Drawing.Image)(resources.GetObject("picEditYoutubeURL.Image")));
            this.picEditYoutubeURL.Location = new System.Drawing.Point(470, 5);
            this.picEditYoutubeURL.Name = "picEditYoutubeURL";
            this.picEditYoutubeURL.Size = new System.Drawing.Size(20, 20);
            this.picEditYoutubeURL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picEditYoutubeURL.TabIndex = 32;
            this.picEditYoutubeURL.TabStop = false;
            this.toolTipController1.SetTitle(this.picEditYoutubeURL, "Neighorhood Maps Maker");
            this.toolTipController1.SetToolTip(this.picEditYoutubeURL, "Click to edit url");
            // 
            // lblOneYoutubeURLNum
            // 
            this.lblOneYoutubeURLNum.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOneYoutubeURLNum.Appearance.Options.UseFont = true;
            this.lblOneYoutubeURLNum.Appearance.Options.UseTextOptions = true;
            this.lblOneYoutubeURLNum.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblOneYoutubeURLNum.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblOneYoutubeURLNum.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblOneYoutubeURLNum.Location = new System.Drawing.Point(10, 3);
            this.lblOneYoutubeURLNum.Name = "lblOneYoutubeURLNum";
            this.lblOneYoutubeURLNum.Size = new System.Drawing.Size(32, 23);
            this.lblOneYoutubeURLNum.TabIndex = 34;
            this.lblOneYoutubeURLNum.Text = "1";
            // 
            // lblPOICardTitle
            // 
            this.lblPOICardTitle.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblPOICardTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPOICardTitle.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblPOICardTitle.Appearance.Options.UseBackColor = true;
            this.lblPOICardTitle.Appearance.Options.UseFont = true;
            this.lblPOICardTitle.Appearance.Options.UseForeColor = true;
            this.lblPOICardTitle.Appearance.Options.UseTextOptions = true;
            this.lblPOICardTitle.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblPOICardTitle.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblPOICardTitle.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblPOICardTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPOICardTitle.Location = new System.Drawing.Point(48, 3);
            this.lblPOICardTitle.Name = "lblPOICardTitle";
            this.lblPOICardTitle.Size = new System.Drawing.Size(409, 23);
            this.lblPOICardTitle.TabIndex = 35;
            this.lblPOICardTitle.Text = "<href=https://www.youtube.com/watch?v=U5FnDuRGMIY>https://www.youtube.com/watch?v" +
    "=U5FnDuRGMIY</href>";
            this.lblPOICardTitle.Click += new System.EventHandler(this.lblPOICardTitle_Click);
            // 
            // toolTipController1
            // 
            this.toolTipController1.ToolTipLocation = DevExpress.Utils.ToolTipLocation.TopRight;
            // 
            // picDeleteURL
            // 
            this.picDeleteURL.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picDeleteURL.Image = ((System.Drawing.Image)(resources.GetObject("picDeleteURL.Image")));
            this.picDeleteURL.Location = new System.Drawing.Point(507, 5);
            this.picDeleteURL.Name = "picDeleteURL";
            this.picDeleteURL.Size = new System.Drawing.Size(20, 20);
            this.picDeleteURL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDeleteURL.TabIndex = 36;
            this.picDeleteURL.TabStop = false;
            this.toolTipController1.SetTitle(this.picDeleteURL, "Neighorhood Maps Maker");
            this.toolTipController1.SetToolTip(this.picDeleteURL, "Click to remove url");
            // 
            // PanelYoutubeURLModel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picDeleteURL);
            this.Controls.Add(this.lblPOICardTitle);
            this.Controls.Add(this.lblOneYoutubeURLNum);
            this.Controls.Add(this.picEditYoutubeURL);
            this.Controls.Add(this.label1);
            this.Name = "PanelYoutubeURLModel";
            this.Size = new System.Drawing.Size(534, 30);
            ((System.ComponentModel.ISupportInitialize)(this.picEditYoutubeURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDeleteURL)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picEditYoutubeURL;
        private DevExpress.XtraEditors.LabelControl lblOneYoutubeURLNum;
        private DevExpress.XtraEditors.HyperlinkLabelControl lblPOICardTitle;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private System.Windows.Forms.PictureBox picDeleteURL;
    }
}
