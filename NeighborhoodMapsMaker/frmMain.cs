﻿using DevExpress.XtraBars.Docking2010;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;

namespace NeighborhoodMapsMaker
{
    public partial class frmMain : DevExpress.XtraEditors.XtraForm
    {
        String wheretogo = string.Empty;
        BackgroundWorker bw = new BackgroundWorker();

        public frmMain()
        {
            InitializeComponent();

            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;

            bw.DoWork += bw_DoWork;
            bw.ProgressChanged += bw_ProgressChanged;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;

        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            string tVal = string.Empty;

            if (wheretogo.Equals("SIGNIN")) {
            }

            if (e.Cancelled == true | e.Error != null)
            {
                //lblStatus.Text = "Request Canceled!";
            }

        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //Me.Text = e.ProgressPercentage.ToString() & "% completed"
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker worker = (BackgroundWorker)sender;
                if (bw.CancellationPending == true)
                {
                    e.Cancel = true;
                }

            }
            catch
            {
                if (bw.WorkerSupportsCancellation == true)
                {
                    bw.CancelAsync();
                }
            }
        }

        int GroupIndex = 0;

        DevExpress.XtraEditors.XtraUserControl pnl1 = new PanelWiz1ChooseProject();
        DevExpress.XtraEditors.XtraUserControl pnl2 = new PanelWiz2AddGMB();
        DevExpress.XtraEditors.XtraUserControl pnl3 = new PanelWiz3UseGoglePOI();
        DevExpress.XtraEditors.XtraUserControl pnl4 = new PanelWiz4QtyPOI();
        DevExpress.XtraEditors.XtraUserControl pnl5 = new PanelWiz5ChoosePOIs();
        DevExpress.XtraEditors.XtraUserControl pnl6 = new PanelWiz6ScrapePOIs();
        DevExpress.XtraEditors.XtraUserControl pnl7 = new PanelWiz7UseNonGooglePOI();
        DevExpress.XtraEditors.XtraUserControl pnl8 = new PanelWiz8QtyPOI();
        DevExpress.XtraEditors.XtraUserControl pnl9 = new PanelWiz9ChoosePOIs();
        DevExpress.XtraEditors.XtraUserControl pnl10 = new PanelWiz10DistanceFromBiz();
        DevExpress.XtraEditors.XtraUserControl pnl11 = new PanelWiz11ScrapePOIs();
        DevExpress.XtraEditors.XtraUserControl pnl12 = new PanelWiz12GoogleAuth();
        DevExpress.XtraEditors.XtraUserControl pnl13 = new PanelWiz13YoutubeURLAndImageLoc();
        DevExpress.XtraEditors.XtraUserControl pnl14 = new PanelWiz14ArtilceManager();
        DevExpress.XtraEditors.XtraUserControl pnl15 = new PanelWiz15MapCreation();

        DevExpress.XtraEditors.XtraUserControl pnl16 = new PanelSett1General();
     
        DevExpress.XtraEditors.XtraUserControl pnl18 = new PanelSupport1();
        DevExpress.XtraEditors.XtraUserControl pnl19 = new PanelTraining1();

        List<DevExpress.XtraEditors.XtraUserControl> GroupOnePanels = new List<DevExpress.XtraEditors.XtraUserControl>();
        List<DevExpress.XtraEditors.XtraUserControl> GroupTwoPanels = new List<DevExpress.XtraEditors.XtraUserControl>();
        List<DevExpress.XtraEditors.XtraUserControl> GroupThreePanels = new List<DevExpress.XtraEditors.XtraUserControl>();
        List<DevExpress.XtraEditors.XtraUserControl> GroupFourPanels = new List<DevExpress.XtraEditors.XtraUserControl>();

        PanelPrevNext pnlnextprevcancel;
        DevExpress.XtraEditors.SimpleButton btnPrev;
        DevExpress.XtraEditors.SimpleButton btnNext;
        DevExpress.XtraEditors.SimpleButton btnCancel;

        private void ManageDependencies()
        {
            //Binary Path
            String BinPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            String UsersDBDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Local\\NMM";

            try
            {
                //DB
                String DBOrigPath = BinPath + "\\db\\nmm.db";
                String UserDBPath = UsersDBDir + "\\nmm.db";
                if (!Directory.Exists(UsersDBDir)) Directory.CreateDirectory(UsersDBDir);
                if (!File.Exists(UserDBPath)) File.Copy(DBOrigPath, UserDBPath, true);

                //bool is64bit = !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"));

                //and chromedriver
                String ChromeOrigPath = BinPath + "\\chromedriver\\chromedriver.exe";
                String UserChromePath = UsersDBDir + "\\chromedriver.exe";
                if (!File.Exists(UserChromePath)) File.Copy(ChromeOrigPath, UserChromePath, true);

            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
                args.Caption = Globals.AppName;
                args.Text = Environment.NewLine
                    + "Something went wrong while creating database in AppData directory!"
                    + Environment.NewLine
                    + "There is a high chance that the app won't work properly!";
                args.Buttons = new DialogResult[] { DialogResult.OK };
                DevExpress.XtraEditors.XtraMessageBox.Show(args);
            }

            //Delete any old fingerprints
            string[] filePaths = Directory.GetFiles(@UsersDBDir);
            foreach (string filePath in filePaths)
            {
                if (filePath.StartsWith("project-nmm-") && filePath.EndsWith(".txt"))
                    File.Delete(filePath);
            }
                

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            DevExpress.XtraEditors.XtraForm.CheckForIllegalCrossThreadCalls = false;

            this.Text = Globals.AppName;
            this.lblAppName.Text = Globals.AppName;
            this.lblAppVer.Text = Globals.AppVer;
            lblChadConnects.Text = Globals.DevName;

            ManageDependencies();

            //set panel definitions

            GroupOnePanels.Add(pnl1); GroupOnePanels.Add(pnl2); GroupOnePanels.Add(pnl3);
            GroupOnePanels.Add(pnl4); GroupOnePanels.Add(pnl5); GroupOnePanels.Add(pnl6);
            GroupOnePanels.Add(pnl7); GroupOnePanels.Add(pnl8); GroupOnePanels.Add(pnl9);
            GroupOnePanels.Add(pnl10); GroupOnePanels.Add(pnl11); GroupOnePanels.Add(pnl12);
            GroupOnePanels.Add(pnl13); GroupOnePanels.Add(pnl14); GroupOnePanels.Add(pnl15);

            GroupTwoPanels.Add(pnl16); //GroupTwoPanels.Add(pnl17);

            GroupThreePanels.Add(pnl18);
            GroupFourPanels.Add(pnl19);

            windowsUIButtonPanel1.SetBounds(1120, (this.Height - windowsUIButtonPanel1.Height) / 2, windowsUIButtonPanel1.Width, windowsUIButtonPanel1.Height);

            //https://github.com/praeclarum/sqlite-net

            int xPos = 100;
            int yPos = 4000;

            //Project Panels
            for (int x = 0; x < GroupOnePanels.Count; x++)
            {
                DevExpress.XtraEditors.XtraUserControl pnl = GroupOnePanels[x];
                this.Controls.Add(pnl);
                pnl.SetBounds(xPos, yPos, pnl.Width, pnl.Height);
            }

            //Settings Panels
            for (int x = 0; x < GroupTwoPanels.Count; x++)
            {
                DevExpress.XtraEditors.XtraUserControl pnl = GroupTwoPanels[x];
                this.Controls.Add(pnl);
                pnl.SetBounds(xPos, yPos, pnl.Width, pnl.Height);
            }

            //Use settings if any
            string v = Properties.Settings.Default.SETT_GOOGLE_EMAIL.ToString();
            DevExpress.XtraEditors.TextEdit txtEmail = (DevExpress.XtraEditors.TextEdit)pnl12.Controls["txtGoogleAuthEmail"];
            txtEmail.Text = v;
            v = Properties.Settings.Default.SETT_GOGGLE_PASS.ToString();
            DevExpress.XtraEditors.TextEdit txtEmailPass = (DevExpress.XtraEditors.TextEdit)pnl12.Controls["txtGoogleAuthPass"];
            txtEmailPass.Text = v;
            v = Properties.Settings.Default.SETT_GOGGLE_ALT_EMAIL.ToString();
            DevExpress.XtraEditors.TextEdit txtEmailAlt = (DevExpress.XtraEditors.TextEdit)pnl12.Controls["txtGoogleAuthRecoEmail"];
            txtEmailAlt.Text = v;
            DevExpress.XtraEditors.CheckEdit chkRemember = (DevExpress.XtraEditors.CheckEdit)pnl12.Controls["chkRememberMe"];
            if (!txtEmail.Text.Trim().Equals(""))
            {
                chkRemember.Checked = true;
                chkRemember.CheckState = CheckState.Checked;
            }
            else
            {
                chkRemember.Checked = false;
                chkRemember.CheckState = CheckState.Unchecked;
            }

            //Support
            DevExpress.XtraEditors.XtraUserControl pnl_s = GroupThreePanels[0];
            this.Controls.Add(pnl_s);
            pnl_s.SetBounds(xPos, yPos, pnl_s.Width, pnl_s.Height);


            //Training
            DevExpress.XtraEditors.XtraUserControl pnl_t = GroupFourPanels[0];
            this.Controls.Add(pnl_t);
            pnl_t.SetBounds(xPos, yPos, pnl_t.Width, pnl_t.Height);


            //Prev next cancel buttons
            pnlnextprevcancel = new PanelPrevNext();
            this.Controls.Add(pnlnextprevcancel);
            pnlnextprevcancel.SetBounds(136, this.Height - 100, pnlnextprevcancel.Width, pnlnextprevcancel.Height);

            //Attach event listeners   
            btnCancel = (DevExpress.XtraEditors.SimpleButton)pnlnextprevcancel.Controls["btnCancel"];
            btnCancel.Click += new EventHandler(Cancel_Click);
            btnPrev = (DevExpress.XtraEditors.SimpleButton)pnlnextprevcancel.Controls["btnPrev"];
            btnPrev.Click += new EventHandler(Prev_Click);
            btnNext = (DevExpress.XtraEditors.SimpleButton)pnlnextprevcancel.Controls["btnNext"];
            btnNext.Click += new EventHandler(Next_Click);


            //Project List Edit Val Change Listner
            DevExpress.XtraEditors.ComboBoxEdit cmbProjectList = (DevExpress.XtraEditors.ComboBoxEdit)pnl1.Controls["cmbProjectList"];
            //cmbProjectList.SelectedIndexChanged += new EventHandler(ProjectNameChanged);


            //set initial state of components
            SetToInitStat();
            FillProjectNamesCombo();
            DataForSettingsOne();
            
            //launch type
            DecideLaunchType();

        }

        /*private void ProjectNameChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmbProjectList = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            if (cmbProjectList.SelectedIndex<=0) return;
            ResetPrjectControls();
            GetAProject();
        }*/

        private void DecideLaunchType()
        {
            String stmt = "SELECT IFNULL(StartSizeNormal, 0) AS StartSizeNormal FROM tbl_settings WHERE UserID = "
                + Globals.USER_ID.ToString() + ";";
            string v = clsSQLiteCRUD.GetACellVal(stmt);
            int WinStat = v.Equals("") ? 0 : Int16.Parse(v);
            if (WinStat == 0) this.WindowState = FormWindowState.Minimized;
        }

        private void FillProjectNamesCombo()
        {

            string stmt = "SELECT IFNULL(NameOfPoject,'') AS NameOfPoject  FROM tbl_projects ORDER BY RowID DESC;";
            DataTable dt = clsSQLiteCRUD.GiveADataTable(stmt);
            if (dt != null)
            {
                DevExpress.XtraEditors.ComboBoxEdit cmb_p_name
               = (DevExpress.XtraEditors.ComboBoxEdit)pnl1.Controls["cmbProjectList"];

                int itms = cmb_p_name.Properties.Items.Count;
                if (itms > 0)
                {
                    for (int x = itms-1; x >=0; x--)
                    {
                        cmb_p_name.Properties.Items.RemoveAt(x);
                    }
                }
               
                cmb_p_name.Properties.Items.Add("-None-");
                for (int x = 0; x < dt.Rows.Count; x++)
                {
                    string itm = dt.Rows[x][0].ToString();
                    cmb_p_name.Properties.Items.Add(itm);
                }

                cmb_p_name.SelectedIndex = 0;
            }
        }

        private void DataForSettingsOne()
        {
            String UsersDBDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Local\\NMM";
            String UserDBPath = UsersDBDir + "\\nmm.db";
            if (File.Exists(UserDBPath))
            {
                SQLiteConnection cn = new SQLiteConnection(@"Data Source=" + UserDBPath);
                String stmt = "SELECT * FROM tbl_settings WHERE UserID = " + Globals.USER_ID.ToString() + ";";
                DataSet ds = new DataSet();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(stmt, cn);
                adapter.Fill(ds);
                DataTable dt = ds.Tables[0];

                //Auto-Start
                int AutoStart = dt.Rows.Count > 0 ? Int16.Parse(dt.Rows[0]["StartWithOS"].ToString()) : 0;
                DevExpress.XtraEditors.CheckEdit chkAutoLaunch
                    = (DevExpress.XtraEditors.CheckEdit)pnl16.Controls["chkAutoLaunch"];
                chkAutoLaunch.Checked = (AutoStart == 1);

                //Screen size
                int StartNormal = dt.Rows.Count > 0 ? Int16.Parse(dt.Rows[0]["StartSizeNormal"].ToString()) : 0;
                DevExpress.XtraEditors.CheckEdit chkStartSize
                    = (DevExpress.XtraEditors.CheckEdit)pnl16.Controls["chkStartSize"];
                chkStartSize.Checked = (StartNormal == 1);

                //user's email id
                string UserEmailID = dt.Rows.Count > 0 ? dt.Rows[0]["UserEmail"].ToString() : "";
                DevExpress.XtraEditors.TextEdit tbEmailID
                    = (DevExpress.XtraEditors.TextEdit)pnl16.Controls["txtUserEmail"];
                tbEmailID.Text = UserEmailID;


                string apikey = dt.Rows[0]["GooglePlacesAPIKey"].ToString();
                DevExpress.XtraEditors.TextEdit tbGPKey
                    = (DevExpress.XtraEditors.TextEdit)pnl16.Controls["txtGooglePlacesAPIKey"];
                tbGPKey.Text = apikey;

                tbGPKey.Focus();

                //Article Builder API Key
                apikey = dt.Rows[0]["ArticleBuilderDotNetAPIKey"].ToString();
                DevExpress.XtraEditors.TextEdit tbABKey
                    = (DevExpress.XtraEditors.TextEdit)pnl16.Controls["txtArticleBuilderAPIKey"];
                tbABKey.Text = apikey;

                //SpinRewriter Builder API Key
                apikey = dt.Rows[0]["SpinRewriterAPIKey"].ToString();
                DevExpress.XtraEditors.TextEdit tbSpinRewriter
                    = (DevExpress.XtraEditors.TextEdit)pnl16.Controls["txtSpinWriterAPIKey"];
                tbSpinRewriter.Text = apikey;

                //SpinnerChiefAPIKey Builder API Key
                apikey = dt.Rows[0]["SpinnerChiefAPIKey"].ToString();
                DevExpress.XtraEditors.TextEdit tbSpinnerChiefKey
                    = (DevExpress.XtraEditors.TextEdit)pnl16.Controls["txtSpinnerChiefAPIKey"];
                tbSpinnerChiefKey.Text = apikey;

                //SpinnerChiefAPIKey Builder API Key
                apikey = dt.Rows[0]["WordAIAPIKey"].ToString();
                DevExpress.XtraEditors.TextEdit tbWordAIKey
                    = (DevExpress.XtraEditors.TextEdit)pnl16.Controls["txtWordAIPIKey"];
                tbWordAIKey.Text = apikey;


                tbEmailID.Focus();
                cn.Close();
            }
        }

        private List<DevExpress.XtraEditors.XtraUserControl> GroupOfWorkingPanels(int indx)
        {
            //Check which panel group to use
            List<DevExpress.XtraEditors.XtraUserControl> WorkingPanelGroup = GroupOnePanels;
            if (GroupIndex == 1) WorkingPanelGroup = GroupTwoPanels;
            if (GroupIndex == 2) WorkingPanelGroup = GroupThreePanels;
            if (GroupIndex == 3) WorkingPanelGroup = GroupFourPanels;

            return WorkingPanelGroup;
        }

        protected void Prev_Click(object sender, EventArgs e)
        {
            List<DevExpress.XtraEditors.XtraUserControl> WorkingGroup = GroupOfWorkingPanels(GroupIndex);
            int panels_in_group = WorkingGroup.Count;
            if (panels_in_group <= 1) return;

            DevExpress.XtraEditors.XtraUserControl visibleOne = null;

            String panel_tag = "";
            for (int x = 0; x < panels_in_group; x++)
            {
                if (WorkingGroup[x] is DevExpress.XtraEditors.XtraUserControl && WorkingGroup[x].Visible)
                {
                    visibleOne = WorkingGroup[x];
                    panel_tag = WorkingGroup[x].Tag.ToString();
                    break;
                }
            }

            //Save/Update, but not nagging!
            string retval = ValidateInputAndMove(visibleOne);

            if (panel_tag.Equals("")) return;

            char[] delimiters = new char[] { '|' };
            string[] entries = panel_tag.Split(delimiters);
            int prev_panel_number = int.Parse(entries[0]);


            if (prev_panel_number > 0)
            {
                HideAllPanels();
                DevExpress.XtraEditors.SimpleButton btn = (DevExpress.XtraEditors.SimpleButton)sender;

                ShowAPanel(WorkingGroup[prev_panel_number - 1]);
                ManageNextPrevButtons();
            }

        }

        protected void Next_Click(object sender, EventArgs e)
        {
            List<DevExpress.XtraEditors.XtraUserControl> WorkingGroup = GroupOfWorkingPanels(GroupIndex);
            int panels_in_group = WorkingGroup.Count;
            if (panels_in_group <= 1) return;

            DevExpress.XtraEditors.XtraUserControl visibleOne = null;

            string panel_tag = "";
            for (int x = 0; x < panels_in_group; x++)
            {
                
                if (x==0 && WorkingGroup[x].Visible)
                {
                    ResetPrjectControls();
                    DevExpress.XtraEditors.XtraUserControl ProjectPanel = (DevExpress.XtraEditors.XtraUserControl) WorkingGroup[x];

                    DevExpress.XtraEditors.CheckEdit chkNewProject = (DevExpress.XtraEditors.CheckEdit)ProjectPanel.Controls["chkCreateNewProject"];
                    DevExpress.XtraEditors.CheckEdit chkExProject = (DevExpress.XtraEditors.CheckEdit)ProjectPanel.Controls["chkChooseOneExisting"];
                    
                    DevExpress.XtraEditors.ComboBoxEdit cmbProjectList = (DevExpress.XtraEditors.ComboBoxEdit)ProjectPanel.Controls["cmbProjectList"];
                    if (!chkNewProject.Checked && cmbProjectList.SelectedIndex > 0)
                    {
                        string stmt = "SELECT RowID AS RID FROM tbl_projects WHERE NameOfPoject = '" 
                            + cmbProjectList.SelectedItem.ToString() + "' LIMIT 0, 1;";
                        Globals.VAR_PROJECT_ID = Int16.Parse(clsSQLiteCRUD.GetACellVal(stmt));

                        stmt = "SELECT RowID AS NameOfProject FROM tbl_projects WHERE RowID = "
                            + Globals.VAR_PROJECT_ID.ToString() + ";";
                        Globals.VAR_PROJECT_NAME = clsSQLiteCRUD.GetACellVal(stmt);

                        GetAProject();
                    }
                }

                if (WorkingGroup[x] is DevExpress.XtraEditors.XtraUserControl && WorkingGroup[x].Visible) {
                    visibleOne = WorkingGroup[x];
                    panel_tag = WorkingGroup[x].Tag.ToString();
                    break;
                }
            }

            //Save Update Values
            string retval = ValidateInputAndMove(visibleOne);
            if (!retval.Equals(""))
            {
                DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
                args.Caption = Globals.AppName;
                args.Text = Environment.NewLine + retval + Environment.NewLine;
                args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.OK };
                DevExpress.XtraEditors.XtraMessageBox.Show(args);
                return;
            }

            if (panel_tag.Equals("")) return;

            char[] delimiters = new char[] { '|' };
            string[] entries = panel_tag.Split(delimiters);
            int next_panel_number = int.Parse(entries[1]);

            if (next_panel_number > 0 && next_panel_number <= panels_in_group)
            {
                HideAllPanels();
                DevExpress.XtraEditors.SimpleButton btn = (DevExpress.XtraEditors.SimpleButton)sender;

                ShowAPanel(WorkingGroup[next_panel_number - 1]);
                ManageNextPrevButtons();
            }

        }

        private string ValidateInputAndMove(DevExpress.XtraEditors.XtraUserControl currentPanel)
        {
            string RetVal = "";
            if (currentPanel == pnl1)
            {
                Globals.VAR_PROJECT_ID = 0;
                Globals.VAR_PROJECT_NAME = "";

                //New or Old
                DevExpress.XtraEditors.CheckEdit chkNewProject
                    = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkCreateNewProject"];
                DevExpress.XtraEditors.CheckEdit chkExisting
                    = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkChooseOneExisting"];

                Boolean IfNew = (chkNewProject.Checked);

                if (IfNew == true)
                {
                    DevExpress.XtraEditors.TextEdit tbProjectName
                    = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtNewProjectName"];
                    string ProjectName = tbProjectName.Text.Trim();
                    if (ProjectName.Equals(""))
                    {
                        RetVal = "Name of project left blank!";
                    }
                    else
                    {
                        string stmt = "SELECT IfNull(RowID, 0) AS RowID FROM tbl_projects WHERE LOWER(NameOfPoject) = '"
                            + ProjectName.ToLower() + "' AND UserID = " + Globals.USER_ID + ";";
                        String v = clsSQLiteCRUD.GetACellVal(stmt);
                        int RID = v.Equals("") ? 0 : Int16.Parse(v);
                        if (RID <= 0)
                        {
                            stmt = "INSERT INTO tbl_projects (UserID, NameOfPoject) VALUES ("
                                + Globals.USER_ID.ToString()
                                + ", '" + ProjectName + "');";
                            RID = clsSQLiteCRUD.INSERT_ROWS(stmt, false);
                            if (RID > 0)
                            {
                                stmt = "SELECT RowID AS RID FROM tbl_projects WHERE NameOfPoject = '" + ProjectName + "' LIMIT 0, 1;";
                              
                                v = clsSQLiteCRUD.GetACellVal(stmt);
                                RID = v.Equals("") ? 0 : Int16.Parse(v);
                                
                                if (RID > 0)
                                {
                                    Globals.VAR_PROJECT_ID = RID;
                                    Globals.VAR_PROJECT_NAME = ProjectName;
                                    DevExpress.XtraEditors.ComboBoxEdit cmbProjects
                                        = (DevExpress.XtraEditors.ComboBoxEdit)pnl1.Controls["cmbProjectList"];
                                    cmbProjects.Properties.Items.Add(ProjectName);
                                    GetAProject();
                                }
                                else
                                {
                                    RetVal = "Failed creating project!";
                                }
                            }
                            else
                            {
                                RetVal = "Failed creating project!";
                            }
                        }
                        else
                        {
                            Globals.VAR_PROJECT_ID = RID;
                            Globals.VAR_PROJECT_NAME = ProjectName;
                        }
                    }
                }
                else
                {
                    DevExpress.XtraEditors.ComboBoxEdit cmbProjectList
                    = (DevExpress.XtraEditors.ComboBoxEdit)currentPanel.Controls["cmbProjectList"];
                    if (cmbProjectList.SelectedIndex <= 0)
                    {
                        RetVal = "You haven't selected any project!";
                    }
                    else
                    {
                        string ProjectName = cmbProjectList.SelectedItem.ToString();
                        string stmt = "SELECT IfNull(RowID, 0) AS RowID FROM tbl_projects WHERE LOWER(NameOfPoject = '"
                            + ProjectName.ToLower() + "') AND UserID = " + Globals.USER_ID.ToString() + ";";
                        string v = clsSQLiteCRUD.GetACellVal(stmt);
                        Globals.VAR_PROJECT_ID = v.Equals("") ? 0 : Int16.Parse(v);
                        stmt = "SELECT IfNull(NameOfPoject, '') AS NameOfPoject FROM tbl_projects WHERE RowID = "
                            + Globals.VAR_PROJECT_ID.ToString() + ";";
                        Globals.VAR_PROJECT_NAME = clsSQLiteCRUD.GetACellVal(stmt);
                    }
                }
            }

            if (currentPanel == pnl2)
            {
                //GMB Country
                DevExpress.XtraEditors.ComboBoxEdit cmbCountry
                  = (DevExpress.XtraEditors.ComboBoxEdit)currentPanel.Controls["cmbCountries"];
                if (cmbCountry.SelectedIndex<=0)
                {
                    RetVal = "GMB Country left blank!";
                }
                

                //GMB Name
                DevExpress.XtraEditors.TextEdit tbGMBName
                   = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtGMBListing"];
                string GMBName = tbGMBName.Text.Trim();
                if (GMBName.Equals(""))
                {
                    RetVal = "GMB Listing left blank!";
                }

                //GMB URL
                DevExpress.XtraEditors.TextEdit txtGMBURL
                   = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtGMBURL"];
                string GMBBiz = txtGMBURL.Text.Trim();
                if (GMBBiz.Equals(""))
                {
                    RetVal = "GMB URL left blank!";
                }

                //City
                DevExpress.XtraEditors.TextEdit tbCity
                   = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtGMBCity"];
                string gmbCity = tbCity.Text.Trim();
                if (gmbCity.Equals(""))
                {
                    RetVal = "City left blank!";
                }

                //Main Keyword
                DevExpress.XtraEditors.TextEdit tbMainKeyword
                   = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtGMBMainKeyword"];
                string GMBMainKeywords = tbMainKeyword.Text.Trim();
                if (GMBMainKeywords.Equals(""))
                {
                    RetVal = "Main Keyword left blank!";
                }

                //Secondary Keywords
                DevExpress.XtraEditors.TextEdit tbSecKeywords
                   = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtGMBSecondaryKeyword"];
                String GMBSecKeywords = tbSecKeywords.Text.Trim();


                if (Globals.VAR_PROJECT_ID > 0 && RetVal.Equals(""))
                {
                    string v = cmbCountry.SelectedItem.ToString().Replace("'", "''");
                    string stmt = "SELECt row_id FROM tbl_countries WHERE nicename = '" + v + "';";
                    int GMB_Country_ID = Int16.Parse(clsSQLiteCRUD.GetACellVal(stmt));

                    stmt = "UPDATE tbl_projects SET "
                   + "GMBCountryID = " + GMB_Country_ID.ToString() + ", "
                   + "NameOfGMB = " + (GMBName.Equals("") ? "NULL" : "'" + GMBName + "'") + ", "
                   + "GMBURL = " + (GMBBiz.Equals("") ? "NULL" : "'" + GMBBiz + "'") + ", "
                   + "GMBCity = " + (gmbCity.Equals("") ? "NULL" : "'" + gmbCity + "'") + ", "
                   + "GMBMainKeyword = " + (GMBMainKeywords.Equals("") ? "NULL" : "'" + GMBMainKeywords + "'") + ", "
                   + "GMBSecondaryKeywords = " + (GMBSecKeywords.Equals("") ? "NULL" : "'" + GMBSecKeywords + "'")
                   + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                    int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                    if (res < 0)
                    {
                        RetVal = "Failed updating GMB information!";
                    }
                }
            }

            //To Use Google POI or Not
            if (currentPanel == pnl3)
            {
                if (Globals.VAR_PROJECT_ID > 0)
                {
                    DevExpress.XtraEditors.CheckEdit chkUseGooglePOI
                  = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkUseGooglePOI"];

                    string stmt = "UPDATE tbl_projects SET "
                        + "UseGooglePOI = " + (chkUseGooglePOI.Checked ? 1 : 0) + " "
                        + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";

                    if (!chkUseGooglePOI.Checked)
                    {
                        stmt = "UPDATE tbl_projects SET "
                        + "UseGooglePOI = " + (chkUseGooglePOI.Checked ? 1 : 0) + ", "
                        + "HowManyGooglePOI = -1, "
                        + "UseUnusedGooglePOIOnly = 0 "
                        + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                        stmt += Environment.NewLine + "DELETE FROM tbl_poi WHERE ProjectID = "
                            + Globals.VAR_PROJECT_ID.ToString() + " AND IsGoogle=1;";
                    }

                    int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                    if (res < 0)
                    {
                        RetVal = "Failed updating project!";
                    }
                }
            }

            //Google POI Quantity
            if (currentPanel == pnl4)
            {

                DevExpress.XtraEditors.CheckEdit chkGPOIWantAll
                = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkGPOIWantAll"];

                DevExpress.XtraEditors.CheckEdit chkGPOIWantSome
                = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkGPOIWantSome"];


                DevExpress.XtraEditors.TextEdit txtGooglePOIQtyNumber
                = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtGooglePOIQtyNumber"];
                string GooglePOIQty = txtGooglePOIQtyNumber.Text.Trim();
                int n;
                bool isNumeric = int.TryParse(GooglePOIQty, out n);

                if (Globals.VAR_PROJECT_ID > 0)
                {


                    string stmt = "";
                    if (chkGPOIWantAll.Checked)
                    {
                        stmt = "UPDATE tbl_projects SET "
                                + "HowManyGooglePOI = -1 "
                                + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                    }
                    else
                    {
                        if (isNumeric == false)
                        {
                            RetVal = "Given value isn't a valid number!";
                        }
                        else
                        {
                            stmt = "UPDATE tbl_projects SET "
                            + "HowManyGooglePOI = " + GooglePOIQty + " "
                            + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                        }
                    }

                    int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);

                    if (res < 0) RetVal = "Failed updating Point of Interest!";

                }
            }

            //Use All POI or Unsed
            if (currentPanel == pnl5)
            {
                DevExpress.XtraEditors.CheckEdit chkUseUnused
                = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkUseUnused"];
                DevExpress.XtraEditors.CheckEdit chkLetMeChoosePOI
                = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkLetMeChoosePOI"];


                if (Globals.VAR_PROJECT_ID > 0)
                {
                  
                    string stmt = "UPDATE tbl_projects SET "
                        + "UseNonGooglePOI = " + (chkUseUnused.Checked ? 1 : 0) + " "
                        + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";

                    if (chkUseUnused.Checked == false)
                    {
                        stmt = "UPDATE tbl_projects SET "
                        + "UseNonGooglePOI = " + (chkUseUnused.Checked ? 1 : 0) + ", "
                        + "HowManyNonGooglePOI = -1, "
                        + "UseUnusedNonGooglePOIOnly = 0 "
                        + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                        stmt += Environment.NewLine + "DELETE FROM tbl_poi WHERE ProjectID = "
                            + Globals.VAR_PROJECT_ID.ToString() + " AND IsGoogle=1;";
                    }

                    int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                    if (res < 0)
                    {
                        RetVal = "Failed updating project!";
                    }
                }


            }

            if (currentPanel == pnl6)
            {
                //None for now - Scraping Google POI
            }

            //To get Non-Google POI or Not
            if (currentPanel == pnl7)
            {
                if (Globals.VAR_PROJECT_ID > 0)
                {
                    DevExpress.XtraEditors.CheckEdit chkUseUnused
                  = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkUseUnused"];

                    string stmt = "UPDATE tbl_projects SET "
                        + "UseNonGooglePOI = " + (chkUseUnused.Checked ? 1 : 0) + " "
                        + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";

                    if (chkUseUnused.Checked == false)
                    {
                        stmt = "UPDATE tbl_projects SET "
                        + "UseNonGooglePOI = " + (chkUseUnused.Checked ? 1 : 0) + ", "
                        + "HowManyNonGooglePOI = -1, "
                        + "UseUnusedNonGooglePOIOnly = 0 "
                        + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                        stmt += Environment.NewLine + "DELETE FROM tbl_poi WHERE ProjectID = "
                            + Globals.VAR_PROJECT_ID.ToString() + " AND IsGoogle=0;";
                    }

                    int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                    if (res < 0)
                    {
                        RetVal = "Failed updating project!";
                    }
                }
            }

            //Non-Google POI All or Given Qty
            if (currentPanel == pnl8)
            {

                DevExpress.XtraEditors.CheckEdit chkGPOIWantAll
                = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkGPOIWantAll"];
                DevExpress.XtraEditors.CheckEdit chkGPOIWantSome
                = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkGPOIWantSome"];

                DevExpress.XtraEditors.TextEdit txtGooglePOIQtyNumber
                = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtNonGooglePOIQtyNumber"];

                string NonGooglePOIQty = txtGooglePOIQtyNumber.Text.Trim();
                int n;
                bool isNumeric = int.TryParse(NonGooglePOIQty, out n);

                if (Globals.VAR_PROJECT_ID > 0)
                {
                    string stmt = "";
                    if (chkGPOIWantAll.Checked)
                    {
                        stmt = "UPDATE tbl_projects SET "
                                + "HowManyNonGooglePOI = -1 "
                                + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                    }
                    else
                    {
                        if (isNumeric == false)
                        {
                            RetVal = "Given value isn't a valid number!";
                        }
                        else
                        {
                            stmt = "UPDATE tbl_projects SET "
                            + "HowManyNonGooglePOI = " + NonGooglePOIQty + " "
                            + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                        }
                    }

                    int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                    if (res < 0)
                    {
                        RetVal = "Failed updating project!";
                    }
                }
            }

            //Used or All Non-Google POI
            if (currentPanel == pnl9)
            {
                DevExpress.XtraEditors.CheckEdit chkUseUnused
                = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkUseUnused"];
                DevExpress.XtraEditors.CheckEdit chkLetMeChoosePOI
                = (DevExpress.XtraEditors.CheckEdit)currentPanel.Controls["chkLetMeChoosePOI"];

                if (Globals.VAR_PROJECT_ID > 0)
                {
                    string stmt = "UPDATE tbl_projects SET "
                            + "UseUnusedNonGooglePOIOnly = " + (chkUseUnused.Checked ? 1 : 0) + " "
                            + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                    int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                    if (res < 0)
                    {
                        RetVal = "Failed updating project!";
                    }
                }

            }

            //Distance from GMB - Non-Google POI
            if (currentPanel == pnl10)
            {
                DevExpress.XtraEditors.ComboBoxEdit txtGooglePOIQtyNumber
                = (DevExpress.XtraEditors.ComboBoxEdit)currentPanel.Controls["cmbPOIFromAPIDistanceKM"];

                if (txtGooglePOIQtyNumber.SelectedIndex <= 0)
                {
                    RetVal = "Distance isn't selected!";
                }
                else
                {
                    if (Globals.VAR_PROJECT_ID > 0)
                    {
                        string distance = txtGooglePOIQtyNumber.SelectedItem.ToString();
                        string stmt = "UPDATE tbl_projects SET "
                                + "NonGooglePOIDistance = " + distance + " "
                                + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                        int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                        if (res < 0)
                        {
                            RetVal = "Failed updating project!";
                        }
                    }
                }

            }

            if (currentPanel == pnl11)
            {
                //None for now - Scraping Non-Google POI
            }

            //Google Auth - Not saving in DB
            if (currentPanel == pnl12)
            {
                DevExpress.XtraEditors.TextEdit txtGoogleEmail
                = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtGoogleAuthEmail"];
                String G_Email = txtGoogleEmail.Text.Trim();

                DevExpress.XtraEditors.TextEdit txtGooglePass
                = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtGoogleAuthPass"];
                String G_Pass = txtGooglePass.Text.Trim();

                DevExpress.XtraEditors.TextEdit txtGoogleAltEmail
               = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["txtGoogleAuthRecoEmail"];
                String G_Alt_Email = txtGoogleAltEmail.Text.Trim();

                if (G_Email.Equals("") || G_Pass.Equals(""))
                {
                    RetVal = "Email ID and/or Password left blank!";
                }

            }

            if (currentPanel == pnl13)
            {

                DevExpress.XtraEditors.TextEdit txtOtherFeaturesYoutubeURL
                = (DevExpress.XtraEditors.TextEdit)currentPanel.Controls["btnEdtOtherFeaturesFolderOfPhotos"];
                String Folder_OF_Photos = txtOtherFeaturesYoutubeURL.Text.Trim();

                if (!Folder_OF_Photos.Equals(""))
                {
                    if (!Directory.Exists(Folder_OF_Photos))
                    {
                        RetVal = "Directory of Photos left blank!";
                    }
                    else
                    {
                        string stmt = "UPDATE tbl_projects SET "
                                + "FolderOfPhotosToUse = '" + Folder_OF_Photos + "' "
                                + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                        int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                        if (res < 0)
                        {
                            RetVal = "Failed updating project!";
                        }
                    }
                }
            }

            if (currentPanel == pnl14)
            {
                RichTextBox rtArticle
                = (RichTextBox)currentPanel.Controls["rtArticle"];
                string full_text = rtArticle.Text.Trim();

                string AppDataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Local\\NMM";
                if (!Directory.Exists(AppDataDirectory))
                {
                    try
                    {
                        Directory.CreateDirectory(AppDataDirectory);
                    }
                    catch (Exception ex)
                    {
                        //do nothing
                    }
                }
                if (Directory.Exists(AppDataDirectory))
                {
                    string ArticlePath = AppDataDirectory
                    + "\\NMM-Article-"
                    + Globals.USER_ID.ToString()
                    + "-"
                    + Globals.VAR_PROJECT_ID.ToString() + ".txt";

                    Boolean res = Globals.CreateAFileWithContent(ArticlePath, full_text);
                    if (res == true)
                    {
                        string stmt = "UPDATE tbl_projects SET "
                                + "ArticleFileLocation = '" + ArticlePath + "' "
                                + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
                        int sql_res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                        if (sql_res < 0)
                        {
                            RetVal = "Failed updating project!";
                        }
                    }
                }
            }

            if (currentPanel == pnl15)
            {
                
                //Start creating map

            }

            //Settings - Gen
            if (currentPanel == pnl16)
            {
                //None
            }

            
            //Support
            if (currentPanel == pnl18)
            {
                //None
            }

            //Training
            if (currentPanel == pnl19)
            {
                //None
            }

            return RetVal;
        }

        private void ManageNextPrevButtons()
        {
            List<DevExpress.XtraEditors.XtraUserControl> WorkingGroup = GroupOfWorkingPanels(GroupIndex);
            int panels_in_group = WorkingGroup.Count;
            if (panels_in_group <= 1)
            {
                btnPrev.Enabled = false;
                btnNext.Enabled = false;
                return;
            }

            String panel_tag = "";
            for (int x = 0; x < panels_in_group; x++)
            {
                if (WorkingGroup[x] is DevExpress.XtraEditors.XtraUserControl && WorkingGroup[x].Visible)
                {
                    panel_tag = WorkingGroup[x].Tag.ToString();
                    break;
                }
            }

            if (panel_tag.Equals("")) return;

            char[] delimiters = new char[] { '|' };
            string[] entries = panel_tag.Split(delimiters);

            int prev_panel_number = int.Parse(entries[0]);
            int next_panel_number = int.Parse(entries[1]);

            if (prev_panel_number > 0) btnPrev.Enabled = true;
            if (prev_panel_number == 0) btnPrev.Enabled = false;

            if (next_panel_number <= panels_in_group) btnNext.Text = "&Next";
            if (next_panel_number == 0) btnNext.Text = "&Finish";
            btnNext.Enabled = true;

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            ResetAll();
        }

        private void ResetAll()
        {
            ResetPanelOne();
            ResetPrjectControls();
            Globals.RESET_VARS();
            SetToInitStat();
        }

        private void SetToInitStat()
        {
            List<DevExpress.XtraEditors.XtraUserControl> WorkingGroup = GroupOfWorkingPanels(GroupIndex);
            HideAllPanels();
            if (WorkingGroup.Count > 0) ShowAPanel(WorkingGroup[0]);

            if (GroupIndex > 0)
            {
                pnlnextprevcancel.Visible = false;
                return;
            }
            else
            {
                pnlnextprevcancel.Visible = true;
            }

            btnNext.Text = "&Next";
            btnNext.Enabled = true;
            btnPrev.Enabled = false;
        }

        private void HideAllPanels()
        {
            for (int x = 0; x < this.Controls.Count; x++)
            {
                if (this.Controls[x] is DevExpress.XtraEditors.XtraUserControl)
                {
                    if (this.Controls[x] == pnlnextprevcancel) continue;
                    this.Controls[x].SetBounds(200, 4000, this.Controls[x].Width, this.Controls[x].Height);
                    this.Controls[x].Visible = false;
                }
            }
        }

        private void ResetPanelOne()
        {
            DevExpress.XtraEditors.CheckEdit chkCreateNewProject
            = (DevExpress.XtraEditors.CheckEdit)pnl1.Controls["chkCreateNewProject"];
            DevExpress.XtraEditors.CheckEdit chkChooseOneExisting
           = (DevExpress.XtraEditors.CheckEdit)pnl1.Controls["chkChooseOneExisting"];
            DevExpress.XtraEditors.TextEdit txtNewProjectName
            = (DevExpress.XtraEditors.TextEdit)pnl1.Controls["txtNewProjectName"];
            DevExpress.XtraEditors.SimpleButton btnGenProjectName
           = (DevExpress.XtraEditors.SimpleButton)pnl1.Controls["btnGenProjectName"];
            DevExpress.XtraEditors.ComboBoxEdit cmbProjectList
           = (DevExpress.XtraEditors.ComboBoxEdit)pnl1.Controls["cmbProjectList"];

            chkCreateNewProject.Checked = true;
            txtNewProjectName.Text = Globals.SetAName();
            txtNewProjectName.Visible = true;
            btnGenProjectName.Visible = true;

            cmbProjectList.SelectedIndex = 0;
            chkChooseOneExisting.Checked = false;
            cmbProjectList.Visible = false;
        }

        private void ResetPrjectControls()
        {
            DevExpress.XtraEditors.ComboBoxEdit CC
            = (DevExpress.XtraEditors.ComboBoxEdit)pnl2.Controls["cmbCountries"];
            CC.SelectedIndex=0;

            DevExpress.XtraEditors.TextEdit C4
            = (DevExpress.XtraEditors.TextEdit)pnl2.Controls["txtGMBListing"];
            C4.Text = "";

            DevExpress.XtraEditors.TextEdit C4_2
            = (DevExpress.XtraEditors.TextEdit)pnl2.Controls["txtGMBURL"];
            C4_2.Text = "";

            DevExpress.XtraEditors.TextEdit C5
            = (DevExpress.XtraEditors.TextEdit)pnl2.Controls["txtGMBCity"];
            C5.Text = "";

            DevExpress.XtraEditors.TextEdit C6
            = (DevExpress.XtraEditors.TextEdit)pnl2.Controls["txtGMBMainKeyword"];
            C6.Text = "";

            DevExpress.XtraEditors.TextEdit C7
            = (DevExpress.XtraEditors.TextEdit)pnl2.Controls["txtGMBSecondaryKeyword"];
            C7.Text = "";

            DevExpress.XtraEditors.CheckEdit chkUseGooglePOI
            = (DevExpress.XtraEditors.CheckEdit)pnl3.Controls["chkUseGooglePOI"];
            chkUseGooglePOI.Checked = false;
            
            DevExpress.XtraEditors.CheckEdit C9
            = (DevExpress.XtraEditors.CheckEdit)pnl4.Controls["chkGPOIWantAll"];
            C9.Checked = true;

            DevExpress.XtraEditors.CheckEdit C9_2
           = (DevExpress.XtraEditors.CheckEdit)pnl4.Controls["chkGPOIWantSome"];
            C9_2.Checked = false;

            DevExpress.XtraEditors.TextEdit C10
            = (DevExpress.XtraEditors.TextEdit)pnl4.Controls["txtGooglePOIQtyNumber"];
            C10.Text = "";
            C10.Visible = false;
                                                                           
            DevExpress.XtraEditors.LabelControl C10_1
            = (DevExpress.XtraEditors.LabelControl)pnl4.Controls["lblGooglePOIQtyLabel"];
            C10_1.Visible = false;

            DevExpress.XtraEditors.LabelControl C10_2
            = (DevExpress.XtraEditors.LabelControl)pnl4.Controls["lblGooglePOIQtyHelp"];
            C10_2.Visible = false;
    

            DevExpress.XtraEditors.CheckEdit chkUseUnused
            = (DevExpress.XtraEditors.CheckEdit)pnl5.Controls["chkUseUnused"];
            DevExpress.XtraEditors.CheckEdit chkLetMeChoosePOI
            = (DevExpress.XtraEditors.CheckEdit)pnl5.Controls["chkLetMeChoosePOI"];
            chkUseUnused.Checked= true;
            chkLetMeChoosePOI.Checked = false;


            //pnl6
            DevExpress.XtraEditors.XtraScrollableControl card_holder_google_poi
            = (DevExpress.XtraEditors.XtraScrollableControl)pnl6.Controls["pnlPOICardsHolder"];
            for (int y = card_holder_google_poi.Controls.Count-1; y>=0; y--) card_holder_google_poi.Controls.RemoveAt(y);
            PanelPOINoCards ucNOPOI = new PanelPOINoCards();
            card_holder_google_poi.Controls.Add(ucNOPOI);
            ucNOPOI.SetBounds(0, 0, ucNOPOI.Width, ucNOPOI.Height);
            card_holder_google_poi.VerticalScroll.Visible = false;
            DevExpress.XtraEditors.SimpleButton btnCheckUncheck = (DevExpress.XtraEditors.SimpleButton) pnl6.Controls["btnCheckUncheck"];
            btnCheckUncheck.Visible = false;
            DevExpress.XtraEditors.SimpleButton btnDeleteSelected = (DevExpress.XtraEditors.SimpleButton)pnl6.Controls["btnDeleteSelected"];
            btnDeleteSelected.Visible = false;


            DevExpress.XtraEditors.CheckEdit chkUseUnused5
            = (DevExpress.XtraEditors.CheckEdit)pnl7.Controls["chkUseUnused"];
            chkUseUnused5.Checked = false;
            DevExpress.XtraEditors.CheckEdit chkGPOIWantAll
            = (DevExpress.XtraEditors.CheckEdit)pnl8.Controls["chkGPOIWantAll"];
            DevExpress.XtraEditors.CheckEdit chkGPOIWantSome
            = (DevExpress.XtraEditors.CheckEdit)pnl8.Controls["chkGPOIWantSome"];
            DevExpress.XtraEditors.TextEdit txtNonGooglePOIQtyNumber
            = (DevExpress.XtraEditors.TextEdit)pnl8.Controls["txtNonGooglePOIQtyNumber"];
            DevExpress.XtraEditors.LabelControl lblNonGooglePOIQtyLabel
            = (DevExpress.XtraEditors.LabelControl)pnl8.Controls["lblNonGooglePOIQtyLabel"];
            DevExpress.XtraEditors.LabelControl lblNonGooglePOIQtyHelp
           = (DevExpress.XtraEditors.LabelControl)pnl8.Controls["lblNonGooglePOIQtyHelp"];
           
            chkGPOIWantAll.Checked = true;
            chkGPOIWantSome.Checked = false;
            txtNonGooglePOIQtyNumber.Text = "";
            txtNonGooglePOIQtyNumber.Visible = false;
            lblNonGooglePOIQtyLabel.Visible = false;
            lblNonGooglePOIQtyHelp.Visible = false;
            

            DevExpress.XtraEditors.CheckEdit chkUseUnused_2
           = (DevExpress.XtraEditors.CheckEdit)pnl9.Controls["chkUseUnused"];
            DevExpress.XtraEditors.CheckEdit chkLetMeChoosePOI_2
            = (DevExpress.XtraEditors.CheckEdit)pnl9.Controls["chkLetMeChoosePOI"];
            chkUseUnused_2.Checked = true;
            chkLetMeChoosePOI_2.Checked = false;

            DevExpress.XtraEditors.ComboBoxEdit C16
            = (DevExpress.XtraEditors.ComboBoxEdit)pnl10.Controls["cmbPOIFromAPIDistanceKM"];
            C16.SelectedIndex = -1;

            //pnl11
            DevExpress.XtraEditors.XtraScrollableControl card_holder_non_google_poi
            = (DevExpress.XtraEditors.XtraScrollableControl)pnl11.Controls["pnlPOICardsHolder"];
            for (int y = card_holder_non_google_poi.Controls.Count-1; y>=0; y--) card_holder_non_google_poi.Controls.RemoveAt(y);
            ucNOPOI = new PanelPOINoCards();
            card_holder_non_google_poi.Controls.Add(ucNOPOI);
            ucNOPOI.SetBounds(0, 0, ucNOPOI.Width, ucNOPOI.Height);
            card_holder_non_google_poi.VerticalScroll.Visible = false;
            DevExpress.XtraEditors.SimpleButton btnCheckUncheck_ex = (DevExpress.XtraEditors.SimpleButton)pnl11.Controls["btnCheckUncheck"];
            btnCheckUncheck_ex.Visible = false;
            DevExpress.XtraEditors.SimpleButton btnDeleteSelected_ex = (DevExpress.XtraEditors.SimpleButton)pnl11.Controls["btnDeleteSelected"];
            btnDeleteSelected_ex.Visible = false;

            //Panel12
            DevExpress.XtraEditors.TextEdit C17
            = (DevExpress.XtraEditors.TextEdit)pnl12.Controls["txtGoogleAuthEmail"];
            //C17.Text = "";

            DevExpress.XtraEditors.TextEdit C18
            = (DevExpress.XtraEditors.TextEdit)pnl12.Controls["txtGoogleAuthPass"];
            //C18.Text = "";

            DevExpress.XtraEditors.TextEdit C19
            = (DevExpress.XtraEditors.TextEdit)pnl12.Controls["txtGoogleAuthRecoEmail"];
            //C19.Text = "";

            DevExpress.XtraEditors.ButtonEdit C20
            = (DevExpress.XtraEditors.ButtonEdit)pnl13.Controls["btnEdtOtherFeaturesFolderOfPhotos"];
            C20.Text = "";

            DevExpress.XtraEditors.TextEdit C21
           = (DevExpress.XtraEditors.TextEdit)pnl13.Controls["txtOtherFeaturesYoutubeURL"];
            C21.Text = "";

            DevExpress.XtraEditors.XtraScrollableControl card_holder_yt_links
            = (DevExpress.XtraEditors.XtraScrollableControl)pnl13.Controls["xscYoutubeURLCardsHolder"];
            for (int y = 0; y < card_holder_yt_links.Controls.Count; y++) card_holder_yt_links.Controls.RemoveAt(0);
            PanelNoVideo ucNoVid = new PanelNoVideo();
            card_holder_yt_links.Controls.Add(ucNoVid);
            ucNOPOI.SetBounds(0, 0, ucNoVid.Width, ucNoVid.Height);
            card_holder_yt_links.VerticalScroll.Visible = false;

            RichTextBox C22
            = (RichTextBox)pnl14.Controls["rtArticle"];
            C22.Text = "";
        }

        private void SurfALink(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.HyperlinkLabelControl poi_title = (DevExpress.XtraEditors.HyperlinkLabelControl)sender;
            string v = poi_title.Tag.ToString();
            if (!v.ToLower().Contains("https") && !v.ToLower().Contains("www")) return;
            System.Diagnostics.Process.Start(v);
        }

        DataTable dt = null;
        DataTable dt_poi = null;
        DataTable dt_poi_non_google = null;
        DataTable dt_vid = null;

        private void GetAProject()
        {
            //project
            string stmt = "SELECT RowID, UserID, " +
                    "IFNULL(NameOfPoject, '') AS NameOfPoject, " +
                    "IFNULL(NameOfGMB, '') AS NameOfGMB, " +
                    "IFNULL(GMBURL, '') AS GMBURL, " +
                    "IFNULL(GMBCity, '') AS GMBCity, " +
                    "IFNULL(GMBMainKeyword, '') AS GMBMainKeyword, " +
                    "IFNULL(GMBSecondaryKeywords, '') AS GMBSecondaryKeywords, " +
                    "IFNULL(UseGooglePOI, 0) AS UseGooglePOI, " +
                    "IFNULL(HowManyGooglePOI, -1) AS HowManyGooglePOI, " +
                    "IFNULL(UseUnusedGooglePOIOnly, 0) AS UseUnusedGooglePOIOnly, " +
                    "IFNULL(UseNonGooglePOI, 0) AS UseNonGooglePOI, " +
                    "IFNULL(HowManyNonGooglePOI, -1) AS HowManyNonGooglePOI, " +
                    "IFNULL(UseUnusedNonGooglePOIOnly, 0) AS UseUnusedNonGooglePOIOnly, " +
                    "IFNULL(NonGooglePOIDistance, -1) AS NonGooglePOIDistance, " +
                    "IFNULL(FolderOfPhotosToUse, '') AS FolderOfPhotosToUse, " +
                    "IFNULL(ArticleFileLocation, '') AS ArticleFileLocation, " +
                    "(SELECT nicename FROM tbl_countries WHERE tbl_countries.row_id = tbl_projects.GMBCountryID) AS GMBCountry "+
                    "FROM tbl_projects WHERE RowID = "
                    + Globals.VAR_PROJECT_ID + " ORDER BY RowID DESC;";
            dt = clsSQLiteCRUD.GiveADataTable(stmt);

            //google poi
            stmt = "SELECT RowID, ProjectID, " +
                "IFNULL(IsGoogle, 0) AS IsGoogle, " +
                "IFNULL(POITitle, '') AS POITitle, " +
                "IFNULL(POILink, '') AS POILink," +
                "IFNULL(POIAddress, '') POIAddress," +
                "IFNULL(ToUse, 0) AS ToUse," +
                "IFNULL(Merged, 0) AS Merged " +
                "FROM tbl_poi WHERE ProjectID = " + Globals.VAR_PROJECT_ID + " AND IsGoogle = 1 " +
                "ORDER BY RowID DESC;";
            dt_poi = clsSQLiteCRUD.GiveADataTable(stmt);

            //non-google poi
            stmt = "SELECT RowID, ProjectID, " +
                "IFNULL(IsGoogle, 0) AS IsGoogle, " +
                "IFNULL(POITitle, '') AS POITitle, " +
                "IFNULL(POILink, '') AS POILink," +
                "IFNULL(ToUse, 0) AS ToUse," +
                "IFNULL(Merged, 0) AS Merged " +
                "FROM tbl_poi WHERE ProjectID = " + Globals.VAR_PROJECT_ID + " AND IsGoogle = 0 " +
                "ORDER BY RowID DESC;";
            dt_poi_non_google = clsSQLiteCRUD.GiveADataTable(stmt);

            //vids
            stmt = "SELECT RowID, ProjectID, IFNULL(VidLink, '') AS VidLink FROM tbl_vids " +
                "WHERE ProjectID = " + Globals.VAR_PROJECT_ID + " " +
                "ORDER BY RowID DESC;";
            dt_vid = clsSQLiteCRUD.GiveADataTable(stmt);

            Boolean IfData = dt == null ? false : dt.Rows.Count > 0 ? true : false;
            Boolean IfData_POI = dt_poi == null ? false : dt_poi.Rows.Count > 0 ? true : false;
            Boolean IfData_VID = dt_vid == null ? false : dt_vid.Rows.Count > 0 ? true : false;

            //Panel2
            DevExpress.XtraEditors.ComboBoxEdit cmbGMBCountry
                   = (DevExpress.XtraEditors.ComboBoxEdit)pnl2.Controls["cmbCountries"];
            cmbGMBCountry.SelectedItem = IfData == true ? dt.Rows[0]["GMBCountry"].ToString() : "";


            DevExpress.XtraEditors.TextEdit txtGMBListing
                    = (DevExpress.XtraEditors.TextEdit)pnl2.Controls["txtGMBListing"];
            txtGMBListing.Text = IfData == true ? dt.Rows[0]["NameOfGMB"].ToString() : "";

            DevExpress.XtraEditors.TextEdit txtGMBURL
                    = (DevExpress.XtraEditors.TextEdit)pnl2.Controls["txtGMBURL"];
            txtGMBURL.Text = IfData == true ? dt.Rows[0]["GMBURL"].ToString() : "";


            DevExpress.XtraEditors.TextEdit txtGMBCity
                = (DevExpress.XtraEditors.TextEdit)pnl2.Controls["txtGMBCity"];
            txtGMBCity.Text = IfData == true ? dt.Rows[0]["GMBCity"].ToString() : "";

            DevExpress.XtraEditors.TextEdit txtGMBMainKeyword
                = (DevExpress.XtraEditors.TextEdit)pnl2.Controls["txtGMBMainKeyword"];
            txtGMBMainKeyword.Text = IfData == true ? dt.Rows[0]["GMBMainKeyword"].ToString() : "";

            DevExpress.XtraEditors.TextEdit txtGMBSecondaryKeyword
                = (DevExpress.XtraEditors.TextEdit)pnl2.Controls["txtGMBSecondaryKeyword"];
            txtGMBSecondaryKeyword.Text = IfData == true ? dt.Rows[0]["GMBSecondaryKeywords"].ToString() : "";

            //Panel3
            DevExpress.XtraEditors.CheckEdit chkUseGooglePOI
                    = (DevExpress.XtraEditors.CheckEdit)pnl3.Controls["chkUseGooglePOI"];
            int v = IfData == true ? Int16.Parse(dt.Rows[0]["UseGooglePOI"].ToString()) : 0;
            chkUseGooglePOI.Checked = v == 1;

            //Panel4
            DevExpress.XtraEditors.CheckEdit chkGPOIWantAll
                    = (DevExpress.XtraEditors.CheckEdit)pnl4.Controls["chkGPOIWantAll"];
            DevExpress.XtraEditors.CheckEdit chkGPOIWantSome
                    = (DevExpress.XtraEditors.CheckEdit)pnl4.Controls["chkGPOIWantSome"];

            DevExpress.XtraEditors.TextEdit txtGooglePOIQtyNumber
                = (DevExpress.XtraEditors.TextEdit)pnl4.Controls["txtGooglePOIQtyNumber"];
            v = IfData == true ? Int16.Parse(dt.Rows[0]["HowManyGooglePOI"].ToString()) : 0;

            chkGPOIWantAll.Checked = v < 0;
            chkGPOIWantSome.Checked = v >= 0;
            txtGooglePOIQtyNumber.Text = v > 0 ? v.ToString() : "0";
            
            txtGooglePOIQtyNumber.Visible = v >= 0;
            DevExpress.XtraEditors.LabelControl lblGooglePOIQtyLabel
            = (DevExpress.XtraEditors.LabelControl)pnl4.Controls["lblGooglePOIQtyLabel"];
            lblGooglePOIQtyLabel.Visible = v >= 0;
            DevExpress.XtraEditors.LabelControl lblGooglePOIQtyHelp
            = (DevExpress.XtraEditors.LabelControl)pnl4.Controls["lblGooglePOIQtyHelp"];
            lblGooglePOIQtyHelp.Visible = v >= 0;


            //Panel5
            DevExpress.XtraEditors.CheckEdit chkUseUnused
                   = (DevExpress.XtraEditors.CheckEdit)pnl5.Controls["chkUseUnused"];
            DevExpress.XtraEditors.CheckEdit chkLetMeChoosePOI
                   = (DevExpress.XtraEditors.CheckEdit)pnl5.Controls["chkLetMeChoosePOI"];
            v = IfData == true ? Int16.Parse(dt.Rows[0]["UseUnusedGooglePOIOnly"].ToString()) : 0;
            chkUseUnused.Checked = v == 1;

            //Panel6
            DevExpress.XtraEditors.XtraScrollableControl pnlPOICardsHolder
                   = (DevExpress.XtraEditors.XtraScrollableControl)pnl6.Controls["pnlPOICardsHolder"];

            for (int x = 0; x < pnlPOICardsHolder.Controls.Count; x++)
            {
                if (pnlPOICardsHolder.Controls[x] is PanelPOINoCards) pnlPOICardsHolder.Controls.RemoveAt(x);
            }

            Boolean ToShowBtnDel = dt_poi == null ? false : (dt_poi.Rows.Count == 0 ? false : true);
            DevExpress.XtraEditors.SimpleButton btnDeleteSelected
                = (DevExpress.XtraEditors.SimpleButton)pnl6.Controls["btnDeleteSelected"];
            btnDeleteSelected.Visible = ToShowBtnDel;

            DevExpress.XtraEditors.SimpleButton btnCheckUncheck
               = (DevExpress.XtraEditors.SimpleButton)pnl6.Controls["btnCheckUncheck"];
            btnCheckUncheck.Visible = ToShowBtnDel;


            if (dt_poi != null)
            {

                for (int x = 0; x < dt_poi.Rows.Count; x++)
                {
                    string poi_text = dt_poi.Rows[x]["POITitle"].ToString();
                    string poi_link = dt_poi.Rows[x]["POILink"].ToString();
                    string poi_address = dt_poi.Rows[x]["POIAddress"].ToString();
                    int ToUse = Int16.Parse(dt_poi.Rows[x]["ToUse"].ToString());
                    
                    PanelModelPOI pmpoi = new PanelModelPOI();
                    DevExpress.XtraEditors.HyperlinkLabelControl lblPOICardTitle
                       = (DevExpress.XtraEditors.HyperlinkLabelControl)pmpoi.Controls["lblPOICardTitle"];
                    lblPOICardTitle.Text = "<href=" + poi_link + ">" + poi_text + "</href>";
                    lblPOICardTitle.Tag = poi_link;

                    DevExpress.XtraEditors.LabelControl lblPOIAddress
                        = (DevExpress.XtraEditors.LabelControl)pmpoi.Controls["lblPOIAddress"];
                    lblPOIAddress.Text = (poi_address .Equals("")?"(Address not found)": poi_address);

                    DevExpress.XtraEditors.CheckEdit chkUseThisPOI
                       = (DevExpress.XtraEditors.CheckEdit)pmpoi.Controls["chkUseThisPOI"];
                    chkUseThisPOI.Tag = poi_link;
                    chkUseThisPOI.Checked = (ToUse == 1);

                    lblPOICardTitle.Click += new EventHandler(SurfALink);
                    chkUseThisPOI.Click += new EventHandler(UsePOIOrNot_Click);


                    if (pnlPOICardsHolder.Controls.Count > 0)
                    {
                        for (int d = pnlPOICardsHolder.Controls.Count - 1; d >= 0; d--)
                        {
                            if (pnlPOICardsHolder.Controls[d] is PanelModelPOI)
                            {
                                int curTop = pnlPOICardsHolder.Controls[d].Location.Y;
                                int newTop = curTop + 5 + pnlPOICardsHolder.Controls[d].Height;
                                int cWidth = pnlPOICardsHolder.Controls[d].Width;
                                int cHeight = pnlPOICardsHolder.Controls[d].Height;
                                pnlPOICardsHolder.Controls[d].SetBounds(0, newTop, cWidth, cHeight);
                            }
                        }
                    }


                    pnlPOICardsHolder.Controls.Add(pmpoi);
                    //int yPos = (x * pmpoi.Height) + (x * 5);
                    pmpoi.SetBounds(0, 0, pmpoi.Width, pmpoi.Height);
                }

                if (dt_poi.Rows.Count >6) pnlPOICardsHolder.VerticalScroll.Visible = true;

            }
            else
            {
                PanelPOINoCards pmpoi_none = new PanelPOINoCards();
                pnlPOICardsHolder.Controls.Add(pmpoi_none);
                pmpoi_none.SetBounds(0, 0, pmpoi_none.Width, pmpoi_none.Height);
                pnlPOICardsHolder.VerticalScroll.Visible = false;
            }

            //Panel7
            DevExpress.XtraEditors.CheckEdit chkUseUnused7
                   = (DevExpress.XtraEditors.CheckEdit)pnl7.Controls["chkUseUnused"];
            v = IfData == true ? Int16.Parse(dt.Rows[0]["UseNonGooglePOI"].ToString()) : 0;
            chkUseUnused7.Checked = v == 1;

            //Panel8
            DevExpress.XtraEditors.CheckEdit chkGPOIWantAll8
                   = (DevExpress.XtraEditors.CheckEdit)pnl8.Controls["chkGPOIWantAll"];
            DevExpress.XtraEditors.CheckEdit chkGPOIWantSome8
                   = (DevExpress.XtraEditors.CheckEdit)pnl8.Controls["chkGPOIWantSome"];

            v = IfData == true ? Int16.Parse(dt.Rows[0]["HowManyNonGooglePOI"].ToString()) : 0;
            chkGPOIWantAll8.Checked = v == -1;
            chkGPOIWantSome8.Checked = v == 1;

            DevExpress.XtraEditors.TextEdit txtNonGooglePOIQtyNumber
                   = (DevExpress.XtraEditors.TextEdit)pnl8.Controls["txtNonGooglePOIQtyNumber"];
            txtNonGooglePOIQtyNumber.Text = v >= 0 ? v.ToString() : "0";


            //Panel9
            DevExpress.XtraEditors.CheckEdit chkUseUnused_2
                   = (DevExpress.XtraEditors.CheckEdit)pnl9.Controls["chkUseUnused"];
            DevExpress.XtraEditors.CheckEdit chkLetMeChoosePOI_2
                   = (DevExpress.XtraEditors.CheckEdit)pnl9.Controls["chkLetMeChoosePOI"];

            v = IfData == true ? Int16.Parse(dt.Rows[0]["UseUnusedNonGooglePOIOnly"].ToString()) : 0;
            chkUseUnused_2.Checked = v == 1;

            //Panel10
            DevExpress.XtraEditors.ComboBoxEdit cmbPOIFromAPIDistanceKM
                   = (DevExpress.XtraEditors.ComboBoxEdit)pnl10.Controls["cmbPOIFromAPIDistanceKM"];
            string v_str = IfData == true ? dt.Rows[0]["NonGooglePOIDistance"].ToString() : "";
            string fail_safe = cmbPOIFromAPIDistanceKM.Properties.Items[0].ToString();
            cmbPOIFromAPIDistanceKM.SelectedIndex = 0;
            for (int d=0; d< cmbPOIFromAPIDistanceKM.Properties.Items.Count;d++)
            {
                if (cmbPOIFromAPIDistanceKM.Properties.Items[d].ToString().Equals(v_str))
                {
                    cmbPOIFromAPIDistanceKM.SelectedIndex=d;
                    break;
                }
            }

            //Panel11
            DevExpress.XtraEditors.XtraScrollableControl pnlPOICardsHolder_2
                   = (DevExpress.XtraEditors.XtraScrollableControl)pnl11.Controls["pnlPOICardsHolder"];

            for (int x = 0; x < pnlPOICardsHolder_2.Controls.Count; x++)
            {
                if (pnlPOICardsHolder_2.Controls[x] is PanelPOINoCards) pnlPOICardsHolder_2.Controls.RemoveAt(x);
            }

            ToShowBtnDel = dt_poi_non_google == null ? false : (dt_poi_non_google.Rows.Count == 0 ? false : true);
            DevExpress.XtraEditors.SimpleButton btnDeleteSelectedEx
                = (DevExpress.XtraEditors.SimpleButton)pnl11.Controls["btnDeleteSelected"];
            btnDeleteSelectedEx.Visible = ToShowBtnDel;

            DevExpress.XtraEditors.SimpleButton btnCheckUncheckEx
               = (DevExpress.XtraEditors.SimpleButton)pnl11.Controls["btnCheckUncheck"];
            btnCheckUncheckEx.Visible = ToShowBtnDel;

           
            if (dt_poi_non_google != null)
            {
                btnDeleteSelectedEx.Visible = true;
                btnCheckUncheckEx.Visible = true;

                for (int x = 0; x < dt_poi_non_google.Rows.Count; x++)
                {
                   
                    string poi_text = dt_poi_non_google.Rows[x]["POITitle"].ToString();
                    string poi_link = dt_poi_non_google.Rows[x]["POILink"].ToString();
                    string poi_address = dt_poi_non_google.Rows[x]["POIAddress"].ToString();
                    int ToUse = Int16.Parse(dt_poi_non_google.Rows[x]["ToUse"].ToString());

                    PanelModelPOI pmpoi = new PanelModelPOI();
                    DevExpress.XtraEditors.HyperlinkLabelControl lblPOICardTitle
                       = (DevExpress.XtraEditors.HyperlinkLabelControl)pmpoi.Controls["lblPOICardTitle"];
                    lblPOICardTitle.Text = "<href=" + poi_link + ">" + poi_text + "</href>";
                    lblPOICardTitle.Tag = poi_link;

                    DevExpress.XtraEditors.LabelControl lblPOIAddress
                        = (DevExpress.XtraEditors.LabelControl)pmpoi.Controls["lblPOIAddress"];
                    lblPOIAddress.Text = (poi_address.Equals("") ? "(Address not found)" : poi_address);

                    DevExpress.XtraEditors.CheckEdit chkUseThisPOI
                       = (DevExpress.XtraEditors.CheckEdit)pmpoi.Controls["chkUseThisPOI"];
                    chkUseThisPOI.Tag = poi_link;
                    chkUseThisPOI.Checked = (ToUse == 1);
                                        
                    chkUseThisPOI.Click += new EventHandler(UsePOIOrNot_Click);


                    if (pnlPOICardsHolder_2.Controls.Count > 0)
                    {
                        for (int d = pnlPOICardsHolder_2.Controls.Count - 1; d >= 0; d--)
                        {
                            if (pnlPOICardsHolder_2.Controls[d] is PanelModelPOI)
                            {
                                int curTop = pnlPOICardsHolder_2.Controls[d].Location.Y;
                                int newTop = curTop + 5 + pnlPOICardsHolder_2.Controls[d].Height;
                                int cWidth = pnlPOICardsHolder_2.Controls[d].Width;
                                int cHeight = pnlPOICardsHolder_2.Controls[d].Height;
                                pnlPOICardsHolder_2.Controls[d].SetBounds(0, newTop, cWidth, cHeight);
                            }
                        }
                    }


                    pnlPOICardsHolder_2.Controls.Add(pmpoi);
                    //int yPos = (x * pmpoi.Height) + (x * 5);
                    pmpoi.SetBounds(0, 0, pmpoi.Width, pmpoi.Height);

                }

                if (dt_poi_non_google.Rows.Count > 6) pnlPOICardsHolder.VerticalScroll.Visible = true;

            }
            else
            {
                PanelPOINoCards pmpoi_none = new PanelPOINoCards();
                pnlPOICardsHolder_2.Controls.Add(pmpoi_none);
                pmpoi_none.SetBounds(0, 0, pmpoi_none.Width, pmpoi_none.Height);
                pnlPOICardsHolder.VerticalScroll.Visible = false;
            }

            //Panel12
            //Email, Pass, Reco Email - leave as is

            //Panel13
            DevExpress.XtraEditors.ButtonEdit btnEdtOtherFeaturesFolderOfPhotos
                   = (DevExpress.XtraEditors.ButtonEdit)pnl13.Controls["btnEdtOtherFeaturesFolderOfPhotos"];
            v_str = IfData == true ? dt.Rows[0]["FolderOfPhotosToUse"].ToString() : "";
            btnEdtOtherFeaturesFolderOfPhotos.Text = v_str;

            DevExpress.XtraEditors.XtraScrollableControl xscYoutubeURLCardsHolder
                   = (DevExpress.XtraEditors.XtraScrollableControl)pnl13.Controls["xscYoutubeURLCardsHolder"];
            xscYoutubeURLCardsHolder.Controls.Clear();

            if (dt_vid != null)
            {
               
                for (int x = 0; x < dt_vid.Rows.Count; x++)
                {
                    string vid_link = dt_vid.Rows[x]["VidLink"].ToString();
                    
                    PanelYoutubeURLModel pymodel = new PanelYoutubeURLModel();
                    DevExpress.XtraEditors.LabelControl lblytserial
                       = (DevExpress.XtraEditors.LabelControl)pymodel.Controls["lblOneYoutubeURLNum"];
                    lblytserial.Text = (dt_vid.Rows.Count - x).ToString();

                    DevExpress.XtraEditors.HyperlinkLabelControl lblytlink
                       = (DevExpress.XtraEditors.HyperlinkLabelControl)pymodel.Controls["lblPOICardTitle"];
                    lblytlink.Tag = vid_link;
                    lblytlink.Text = "<href=" + vid_link + ">" + vid_link + "</href>";

                    PictureBox picEditYoutubeURL = (PictureBox)pymodel.Controls["picEditYoutubeURL"];
                    picEditYoutubeURL.Tag = vid_link;
                    picEditYoutubeURL.Click += new EventHandler(GoEdit);

                    PictureBox picDeleteURL = (PictureBox)pymodel.Controls["picDeleteURL"];
                    picDeleteURL.Tag = vid_link;
                    picDeleteURL.Click += new EventHandler(GDelete);


                    if (xscYoutubeURLCardsHolder.Controls.Count > 0)
                    {
                        for (int d = xscYoutubeURLCardsHolder.Controls.Count - 1; d >= 0; d--)
                        {
                            if (xscYoutubeURLCardsHolder.Controls[d] is PanelYoutubeURLModel)
                            {
                                int curTop = xscYoutubeURLCardsHolder.Controls[d].Location.Y;
                                int newTop = curTop + 5 + xscYoutubeURLCardsHolder.Controls[d].Height;
                                int cWidth = xscYoutubeURLCardsHolder.Controls[d].Width;
                                int cHeight = xscYoutubeURLCardsHolder.Controls[d].Height;
                                xscYoutubeURLCardsHolder.Controls[d].SetBounds(0, newTop, cWidth, cHeight);
                            }
                        }
                    }


                    xscYoutubeURLCardsHolder.Controls.Add(pymodel);
                    //int yPos = (x * pymodel.Height) + (x * 5);
                    pymodel.SetBounds(0, 0, pymodel.Width, pymodel.Height);
                }

                //ResetRowNums();
                if (dt_vid.Rows.Count > 6) xscYoutubeURLCardsHolder.VerticalScroll.Visible = true;

            }
            else
            {
                PanelNoVideo pmpoi_no_vid = new PanelNoVideo();
                xscYoutubeURLCardsHolder.Controls.Add(pmpoi_no_vid);
                pmpoi_no_vid.SetBounds(0, 0, pmpoi_no_vid.Width, pmpoi_no_vid.Height);
                xscYoutubeURLCardsHolder.VerticalScroll.Visible = false;
            }


            //Panel14
            RichTextBox rtArticle = (RichTextBox)pnl14.Controls["rtArticle"];
            v_str = IfData == true ? dt.Rows[0]["ArticleFileLocation"].ToString() : "";

            string s_article_body = "";
            if (File.Exists(v_str))
            {
                s_article_body = File.ReadAllText(v_str);
            }
            rtArticle.Text = s_article_body;


            //panel15
            GETPOICountAndETA();

        }

        DevExpress.XtraEditors.XtraUserControl WorkingPOIPanel;
        
        private void GETPOICountAndETA()
        {
            string sql = "SELECT Count(*) AS CNT FROM tbl_poi WHERE ProjectID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
            string p_cnt = clsSQLiteCRUD.GetACellVal(sql);

            DevExpress.XtraEditors.LabelControl qty_poi_label = (DevExpress.XtraEditors.LabelControl)pnl15.Controls["lblMakingMapsQty"];
            qty_poi_label.Text = p_cnt;

            DevExpress.XtraEditors.LabelControl eta_label = (DevExpress.XtraEditors.LabelControl)pnl15.Controls["lblMakingMapsETAVal"];
            int eta = (Int16.Parse(p_cnt) * 8);
            if (eta == 0)
            {
                eta_label.Text = "N/A";
            }
            else
            {
                var time = TimeSpan.FromSeconds(eta);
                DateTime dateTime = DateTime.Today.Add(time);
                string formattedDate = dateTime.ToString("hh:mm:ss");
                eta_label.Text = formattedDate;
            }
        }

        protected void GoEdit(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.XtraScrollableControl vidContainer =
                (DevExpress.XtraEditors.XtraScrollableControl)pnl13.Controls["xscYoutubeURLCardsHolder"];
                
            DevExpress.XtraEditors.TextEdit lblytlink
                       = (DevExpress.XtraEditors.TextEdit)pnl13.Controls["txtOtherFeaturesYoutubeURL"];

            String my_vid_link = GetURLByPicTag(sender);
            lblytlink.Text = my_vid_link;
            Globals.WORKING_VID_URL = my_vid_link;
        }

        private String GetURLByPicTag(object sender)
        {
            System.Windows.Forms.PictureBox cur_img = (System.Windows.Forms.PictureBox)sender;
            return cur_img.Tag.ToString();
        }
        protected void GDelete(object sender, EventArgs e)
        {
            string my_vid_link = GetURLByPicTag(sender);

            DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
            args.Caption = Globals.AppName;
            args.Text = "The following youtube link will be deleted permanently-"
                + Environment.NewLine + Environment.NewLine + my_vid_link + Environment.NewLine + Environment.NewLine
                + "This action is irreversible."
                + Environment.NewLine + "Are you sure to proceed?" + Environment.NewLine;
            args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.Yes, System.Windows.Forms.DialogResult.No };
            //args.Icon = System.Drawing.SystemIcons.Question;
            if (DevExpress.XtraEditors.XtraMessageBox.Show(args) == System.Windows.Forms.DialogResult.No) return;

            DevExpress.XtraEditors.XtraScrollableControl yt_card_holder =
                        (DevExpress.XtraEditors.XtraScrollableControl)pnl13.Controls["xscYoutubeURLCardsHolder"];

            for (int x = 0; x < yt_card_holder.Controls.Count; x++)
            {
                if (yt_card_holder.Controls[x] is DevExpress.XtraEditors.XtraUserControl
                    && yt_card_holder.Controls[x].Name.Equals("PanelYoutubeURLModel"))
                {
                    System.Windows.Forms.PictureBox pbDel =
                         (System.Windows.Forms.PictureBox)yt_card_holder.Controls[x].Controls["picDeleteURL"];
                    string curURL = pbDel.Tag.ToString();

                    if (curURL.Equals(my_vid_link))
                    {

                        //Delete from DB
                        string sql = "DELETE FROM tbl_vids WHERE VidLink = '" + curURL + "' AND ProjectID = "
                            + Globals.VAR_PROJECT_ID.ToString() + ";";
                        int ret = clsSQLiteCRUD.UPDATE_ROWS(sql);

                        //Delete from card holder panel
                        yt_card_holder.Controls.RemoveAt(x);

                        //Reset Row Number
                        ResetRowNums();

                        break;
                    }

                }
            }
        }

        private void ResetRowNums()
        {
            DevExpress.XtraEditors.XtraScrollableControl yt_card_holder =
                        (DevExpress.XtraEditors.XtraScrollableControl)pnl13.Controls["xscYoutubeURLCardsHolder"];

            if (yt_card_holder.Controls.Count > 0)
            {
                for (int x = 0; x < yt_card_holder.Controls.Count; x++)
                {
                    DevExpress.XtraEditors.XtraUserControl one_card =
                        (DevExpress.XtraEditors.XtraUserControl)yt_card_holder.Controls[x];

                    DevExpress.XtraEditors.LabelControl one_card_sl =
                        (DevExpress.XtraEditors.LabelControl)one_card.Controls["lblOneYoutubeURLNum"];
                    one_card_sl.Text = (x+1).ToString();

                    int model_y = (x * one_card.Height) + (x * 5);
                    one_card.SetBounds(0, model_y, one_card.Width, one_card.Height);

                }
            }

            ManageNoVidsView();
        }

        private void ManageNoVidsView()
        {
            DevExpress.XtraEditors.XtraScrollableControl yt_card_holder =
                        (DevExpress.XtraEditors.XtraScrollableControl)pnl13.Controls["xscYoutubeURLCardsHolder"];

            string sql = "SELECT Count(*) AS CNT FROM tbl_vids WHERE ProjectID = "
                + Globals.VAR_PROJECT_ID.ToString() + ";";
            string v = clsSQLiteCRUD.GetACellVal(sql);

            for (int x = 0; x < yt_card_holder.Controls.Count; x++) yt_card_holder.Controls.RemoveAt(x);

            if (Int16.Parse(v) == 0)
            {   
                PanelNoVideo pnv = new PanelNoVideo();
                yt_card_holder.Controls.Add(pnv);
                pnv.SetBounds(0, 0, pnv.Width, pnv.Height);
            }
        }

        private void UsePOIOrNot_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit chkUseThisPOI = (DevExpress.XtraEditors.CheckEdit)sender;
            Boolean ToUse = chkUseThisPOI.Checked;

            DevExpress.XtraEditors.XtraScrollableControl POICardsHolder = (DevExpress.XtraEditors.XtraScrollableControl)WorkingPOIPanel.Controls["pnlPOICardsHolder"];

            for (int x = 0; x < POICardsHolder.Controls.Count; x++)
            {
                DevExpress.XtraEditors.XtraUserControl OneCard = (DevExpress.XtraEditors.XtraUserControl)POICardsHolder.Controls[x];
                DevExpress.XtraEditors.CheckEdit chkOne = (DevExpress.XtraEditors.CheckEdit)OneCard.Controls["chkUseThisPOI"];
                
                if (chkUseThisPOI.Tag.ToString().Equals(chkOne.Tag.ToString()))
                {
                    int cond = 1;
                    if (ToUse == false) cond = 0;
                    string stmt = "UPDATE tbl_poi SET ToUse = " + cond.ToString() + " WHERE ProjectID = " + Globals.VAR_PROJECT_ID.ToString();
                    int res = clsSQLiteCRUD.UPDATE_ROWS(stmt);
                }
            }
        }
        private void DeleteAnYTCard_Click(object sender, EventArgs e)
        {
            PictureBox picDeleteSender = (PictureBox) sender;
            string tagDeleteSender = picDeleteSender.ToString();

            DevExpress.XtraEditors.XtraScrollableControl xscYoutubeURLCardsHolder = (DevExpress.XtraEditors.XtraScrollableControl)pnl13.Controls["xscYoutubeURLCardsHolder"];

            int TotalDel = 0;
            string cond = "";
            string del_stmt = "DELETE FROM tbl_vids WHERE ProjectID = " + Globals.VAR_PROJECT_ID.ToString();

            for (int x =0; x< xscYoutubeURLCardsHolder.Controls.Count;x++)
            {
                DevExpress.XtraEditors.XtraUserControl OneCard  = (DevExpress.XtraEditors.XtraUserControl)xscYoutubeURLCardsHolder.Controls[x];
                PictureBox picDel = (PictureBox) OneCard.Controls["PanelYoutubeURLModel"];
                string tagDelCur = picDel.ToString();

                if (tagDeleteSender.Equals(tagDelCur))
                {
                    xscYoutubeURLCardsHolder.Controls.RemoveAt(x);
                    cond += (cond.Equals("")?"":" OR ") + "VidLink = '" + tagDelCur+ "'";
                     
                    TotalDel++;
                    break;
                }
            }

            if (TotalDel > 0)
            {
                int res = clsSQLiteCRUD.UPDATE_ROWS(del_stmt + " AND (" + cond + ")");

                for (int x=0;x< xscYoutubeURLCardsHolder.Controls.Count;x++)
                {

                    if (xscYoutubeURLCardsHolder.Controls[x] is PanelYoutubeURLModel)
                    {
                        xscYoutubeURLCardsHolder.Controls.RemoveAt(x);
                        PanelYoutubeURLModel pym = (PanelYoutubeURLModel)xscYoutubeURLCardsHolder.Controls[x];
                        DevExpress.XtraEditors.LabelControl lblRowNum = (DevExpress.XtraEditors.LabelControl)pym.Controls["lblOneYoutubeURLNum"];
                        lblRowNum.Text = (x + 1).ToString();
                    }
                }
            }

        }
        private void ShowAPanel(DevExpress.XtraEditors.XtraUserControl WhichOne)
        {
            int xPos = 100;
            int yPos = ((this.Height - WhichOne.Height) / 2);
            WhichOne.SetBounds(xPos, yPos, WhichOne.Width, WhichOne.Height);
            WhichOne.Visible = true;


            //GMB Detail
            if (pnl2.Visible == true)
            {
                DevExpress.XtraEditors.ComboBoxEdit cmbCountries
                    = (DevExpress.XtraEditors.ComboBoxEdit)pnl2.Controls["cmbCountries"];
                cmbCountries.Focus();
                return;
            }

            if (pnl3.Visible == true)
            {
                DevExpress.XtraEditors.CheckEdit chkUseGooglePOI
                    = (DevExpress.XtraEditors.CheckEdit)pnl3.Controls["chkUseGooglePOI"];
                chkUseGooglePOI.Focus();
                return;
            }

            if (pnl4.Visible == true)
            {
                DevExpress.XtraEditors.CheckEdit chkGPOIWantAll
                    = (DevExpress.XtraEditors.CheckEdit)pnl4.Controls["chkGPOIWantAll"];
                chkGPOIWantAll.Focus();
                return;
            }

            if (pnl5.Visible == true)
            {
                DevExpress.XtraEditors.CheckEdit chkUseUnused
                   = (DevExpress.XtraEditors.CheckEdit)pnl5.Controls["chkUseUnused"];
                chkUseUnused.Focus();
                return;
            }


            //Scrape/List POI - Google
            if (pnl6.Visible == true)
            {
                WorkingPOIPanel = pnl6;
                DevExpress.XtraEditors.SimpleButton btnShowMeWhatsAvailable
                   = (DevExpress.XtraEditors.SimpleButton)pnl6.Controls["btnShowMeWhatsAvailable"];
                btnShowMeWhatsAvailable.Focus();
                return;
            }

            if (pnl7.Visible == true) return;


            if (pnl8.Visible == true) return;


            if (pnl9.Visible == true)
            {
                DevExpress.XtraEditors.CheckEdit chkUseUnused
                  = (DevExpress.XtraEditors.CheckEdit)pnl9.Controls["chkUseUnused"];
                chkUseUnused.Focus();
            }


            //Distance from Biz
            if (pnl10.Visible == true)
            {
                DevExpress.XtraEditors.ComboBoxEdit btn_phot_dir = (DevExpress.XtraEditors.ComboBoxEdit)pnl10.Controls["cmbPOIFromAPIDistanceKM"];
                btn_phot_dir.Focus();
            }

            //Scrape/List POI - Non-Google
            if (pnl11.Visible == true)
            {
                WorkingPOIPanel = pnl11;
                return;
            }
            
            if (pnl12.Visible == true)
            {
                DevExpress.XtraEditors.TextEdit txtgemail = (DevExpress.XtraEditors.TextEdit)pnl12.Controls["txtGoogleAuthEmail"];
                txtgemail.Focus();
                return;
            }

            if (pnl13.Visible == true)
            {
                DevExpress.XtraEditors.ButtonEdit btn_phot_dir = (DevExpress.XtraEditors.ButtonEdit)pnl13.Controls["btnEdtOtherFeaturesFolderOfPhotos"];
                btn_phot_dir.Focus();
                return;
            }

            if (pnl14.Visible == true)
            {
                System.Windows.Forms.RichTextBox rt_Article = (System.Windows.Forms.RichTextBox)pnl14.Controls["rtArticle"];
                rt_Article.Focus();
                return;
            }

            if (pnl15.Visible == true)
            {
                GETPOICountAndETA();
            }
            
        }

        private void frmMain_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {

            Globals.frmmain = null;
            Application.Exit();
        }

        private void frmMain_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {

            //FontFamily myFont = new FontFamily("Tahoma");
            
            DevExpress.XtraEditors.XtraMessageBoxArgs args = new DevExpress.XtraEditors.XtraMessageBoxArgs();
            args.Caption = Globals.AppName;
            args.Text = Environment.NewLine + "Are you sure you want to quit?" + Environment.NewLine;
            args.Buttons = new System.Windows.Forms.DialogResult[] { System.Windows.Forms.DialogResult.Yes, System.Windows.Forms.DialogResult.No };
            //args.Icon = System.Drawing.SystemIcons.Question;
            
            if (DevExpress.XtraEditors.XtraMessageBox.Show(args) == DialogResult.No) e.Cancel = true;

        }
        
        private void windowsUIButtonPanel1_ButtonClick(object sender, ButtonEventArgs e)
        {

            //uncheck all
            for (int x = 0; x < windowsUIButtonPanel1.Buttons.Count; x++)
            {
                windowsUIButtonPanel1.Buttons[x].Properties.Checked = false;
                windowsUIButtonPanel1.Buttons[x].Properties.Appearance.BackColor = System.Drawing.Color.Gray;
            }

            //check current one
            WindowsUIButton btn = e.Button as WindowsUIButton;
            if (btn.Tag != null)
            {
                btn.Checked = true;
                btn.Appearance.BackColor = System.Drawing.Color.White;

                ResetAll();

                if (GroupIndex != 0 && btn.Tag.Equals("Wizard")) SetToStart(0);
                if (GroupIndex != 1 && btn.Tag.Equals("Settings")) SetToStart(1);
                if (GroupIndex != 2 && btn.Tag.Equals("Support")) SetToStart(2);
                if (GroupIndex != 3 && btn.Tag.Equals("Training")) SetToStart(3);

            }
        }

        private void SetToStart(int Indx)
        {
            GroupIndex = Indx;
            SetToInitStat();
        }

        private void lblChadConnects_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(lblChadConnects.Tag.ToString());
        }

        private void frmMain_Click(object sender, EventArgs e)
        {
            if (pnl2.Visible == true)
            {
                DevExpress.XtraEditors.XtraScrollableControl suggest_holder =
                    (DevExpress.XtraEditors.XtraScrollableControl)pnl2.Controls["pnlAutoSuggestHolder"];
                suggest_holder.Visible = false;
            }
        }

        private void picAppLogo_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }

}