﻿using Microsoft.Win32;
using System;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace NeighborhoodMapsMaker
{
    class Globals
    {
        public static string AppName = "Neighborhood Maps Maker";
        public static string AppVer = "1.1.4";
        public static string RelDate = "08 July 2019";
        public static string DevName = "Chad Ware";
        public static string DevSite = "http://www.chadconnects.com/";
        public static string ValidURL = "";

        public static string SupportPage = "https://www.chadware.com";
        public static string TutorialPage = "https://www.chadware.com";

        public static string USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36";
        //public static string USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36 Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36";

        public static frmMain frmmain;

        public static int USER_ID = 1;
        public static Boolean USER_VALIDATED = false;
        public static String VAR_PROJECT_NAME = "";
        public static int VAR_PROJECT_ID = 0;
        public static Boolean VAR_USE_GOOGLE_POI = false;
        public static Boolean VAR_USE_NON_GOOGLE_POI = false;

        public static string WORKING_VID_URL = "";

        public static void RESET_VARS()
        {
            VAR_PROJECT_ID = -1;
            VAR_PROJECT_NAME = "";
        }

        public static string NumbersOnly(string phone)
        {
            Regex digitsOnly = new Regex("[^\\d]");
            return digitsOnly.Replace(phone, "");
        }

        public static string SetAName()
        {
            String datepart = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff");
            String project_name = "project-nmm-" + datepart;
            Globals.VAR_PROJECT_NAME = "";
            return project_name;
        }

        public static int GET_RANDOM(int Min, int Max)
        {
            return new Random().Next(Min, Max + 1);
        }

        public static string GetPageSource(string url)
        {
            string htmlCode = "";
            using (WebClient client = new WebClient())
            {
                htmlCode = client.DownloadString(url);
            }
            return htmlCode;
        }

        [System.Runtime.InteropServices.DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        public static bool CheckNet()
        {
            int desc;
            return InternetGetConnectedState(out desc, 0);
        }

        public static int NTH_INDEX(string SearchIn, string SearchFor, int Nth)
        {
            int functionReturnValue = 0;
            int tmpPointer = 0;
            int StartPoint = 0;
            int FindCount = 0;

            FindCount = 0;
            StartPoint = 1;
            do
            {
                tmpPointer = SearchIn.IndexOf(SearchFor, StartPoint);
                if (tmpPointer > 0)
                {
                    FindCount = FindCount + 1;
                    if (FindCount == Nth)
                    {
                        functionReturnValue = tmpPointer;
                        return functionReturnValue;
                    }
                    StartPoint = tmpPointer + 1;
                }
            } while (!(StartPoint > SearchIn.Length | tmpPointer == 0));
            functionReturnValue = 0;
            return functionReturnValue;
        }
        
        public static int INSTANCE_COUNT(String haystack, String needle)
        {
            return haystack.Split(new[] { needle }, StringSplitOptions.None).Length - 1;
        }

        public static bool IS_VALID_EMAIL_ID(String DoubtfulEmailID)
        {
            return Regex.IsMatch(DoubtfulEmailID, "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
        }

        public static void WRITE_APPEND_TEXT(String OutPath, String NewString)
        {

            if (!File.Exists(OutPath))
            {
                File.Create(OutPath).Dispose();
                using (TextWriter tw = new StreamWriter(OutPath))
                {
                    tw.WriteLine(NewString);
                    tw.Close();
                }

            }

            else if (File.Exists(OutPath))
            {
                using (TextWriter tw = new StreamWriter(OutPath))
                {
                    tw.WriteLine(NewString);
                    tw.Close();
                }
            }
        }

        public static String[] READ_TEXT_LINES(String TextFileLoc)
        {
            String[] TheArray = null;

            int lines = 0;
            var reader = new StreamReader(TextFileLoc);

            String line;
            while ((line = reader.ReadLine()) != null)
            {
                lines += 1;
            }

            if (lines <= 0)
            {
                return TheArray;
            }

            TheArray = new String[lines];

            lines = 0;
            while ((line = reader.ReadLine()) != null)
            {
                TheArray[lines] = line;
                lines += 1;
            }

            return TheArray;
        }

        public static Bitmap IMAGE_FILTER_GRAYSCALE(String InputFilePath)
        {
            Bitmap bt = new Bitmap(InputFilePath, true);

            for (int y = 0; y < bt.Height; y++)
            {
                for (int x = 0; x < bt.Width; x++)
                {
                    Color c = bt.GetPixel(x, y);

                    int r = c.R;
                    int g = c.G;
                    int b = c.B;
                    int avg = (r + g + b) / 3;
                    bt.SetPixel(x, y, Color.FromArgb(avg, avg, avg));
                }
            }

            //bt.Save("d:\\out.bmp");
            return bt;
        }

        public static void AUTO_RUN_ADD(String ApplicationName, String ApplicationPath)
        {
            Microsoft.Win32.RegistryKey CU = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
            var _with1 = CU;
            _with1.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            _with1.SetValue(ApplicationName, ApplicationPath);

        }
        //remove value
        public static void AUTO_RUN_REMOVE(String ApplicationName)
        {
            Microsoft.Win32.RegistryKey CU = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
            var _with2 = CU;
            _with2.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            _with2.DeleteValue(ApplicationName, false);

        }

        public static String GET_FILE_SIZE(String FilePath)
        {
            System.IO.FileInfo infoReader = new System.IO.FileInfo(FilePath);
            string[] suffixes = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int s = 0;
            long size = infoReader.Length;
            while (size >= 1024)
            {
                s++;
                size /= 1024;
            }
            string ReadableSize = String.Format("{0} {1}", size, suffixes[s]);

            return ReadableSize;
        }

        //------------------------------------String Part
        public static String REVERSE_STRING(String InputString)
        {
            char[] arr = InputString.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static int STRING_INDEX_IN_ARRAY(String[] InputHayStack, String TheeNeedle)
        {
            int TheIndex = -1;
            for (int i = InputHayStack.GetLowerBound(0); i <= InputHayStack.GetUpperBound(0); i++)
                if (InputHayStack[i].ToLower().Equals(TheeNeedle.ToLower()))
                {
                    TheIndex = i;
                    break;
                }
            return TheIndex;
        }

        public static string CHROME_BINARY_PATH()
        {
            var path = Microsoft.Win32.Registry.GetValue(
            @"HKEY_CLASSES_ROOT\ChromeHTML\shell\open\command", null, null) as string;
            if (path != null)
            {
                var split = path.Split('\"');
                path = split.Length >= 2 ? split[1] : null;
            }

            return path;
        }

        public static Boolean CreateAFileWithContent(string out_path, string text_to_write)
        {
            Boolean Res = false;

            try
            {
                // Check if file already exists. If yes, delete it.     
                if (File.Exists(out_path)) File.Delete(out_path);

                // Create a new file     
                FileStream fs = File.Create(out_path);
                Byte[] full_string_bytes = new UTF8Encoding(true).GetBytes(text_to_write);
                fs.Write(full_string_bytes, 0, full_string_bytes.Length);
                fs.Close();
                fs.Dispose();

                Res = true;
            }
            catch (Exception Ex)
            {
                //Console.WriteLine(Ex.ToString());
            }

            return Res;
        }
               
    }
}
