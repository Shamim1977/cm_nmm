﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz7UseNonGooglePOI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz7UseNonGooglePOI));
            this.lblUseGooglePOIYesOrNo = new DevExpress.XtraEditors.LabelControl();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.chkUseUnused = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseUnused.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUseGooglePOIYesOrNo
            // 
            this.lblUseGooglePOIYesOrNo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUseGooglePOIYesOrNo.Appearance.Options.UseFont = true;
            this.lblUseGooglePOIYesOrNo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblUseGooglePOIYesOrNo.Location = new System.Drawing.Point(40, 6);
            this.lblUseGooglePOIYesOrNo.Name = "lblUseGooglePOIYesOrNo";
            this.lblUseGooglePOIYesOrNo.Size = new System.Drawing.Size(419, 18);
            this.lblUseGooglePOIYesOrNo.TabIndex = 16;
            this.lblUseGooglePOIYesOrNo.Text = "Get More Places of Interest?";
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 43;
            // 
            // chkUseUnused
            // 
            this.chkUseUnused.EditValue = true;
            this.chkUseUnused.Location = new System.Drawing.Point(40, 57);
            this.chkUseUnused.Name = "chkUseUnused";
            this.chkUseUnused.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUseUnused.Properties.Appearance.Options.UseFont = true;
            this.chkUseUnused.Properties.Caption = "Yes";
            this.chkUseUnused.Size = new System.Drawing.Size(89, 22);
            this.chkUseUnused.TabIndex = 47;
            this.chkUseUnused.EditValueChanged += new System.EventHandler(this.chkUseUnused_EditValueChanged);
            // 
            // PanelWiz7UseNonGooglePOI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkUseUnused);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblUseGooglePOIYesOrNo);
            this.Name = "PanelWiz7UseNonGooglePOI";
            this.Size = new System.Drawing.Size(474, 107);
            this.Load += new System.EventHandler(this.Panel7UseNonGooglePOI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseUnused.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl lblUseGooglePOIYesOrNo;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.CheckEdit chkUseUnused;
    }
}
