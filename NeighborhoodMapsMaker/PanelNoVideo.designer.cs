﻿namespace NeighborhoodMapsMaker
{
    partial class PanelNoVideo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOneYoutubeURLNum = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblOneYoutubeURLNum
            // 
            this.lblOneYoutubeURLNum.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblOneYoutubeURLNum.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOneYoutubeURLNum.Appearance.Options.UseBackColor = true;
            this.lblOneYoutubeURLNum.Appearance.Options.UseFont = true;
            this.lblOneYoutubeURLNum.Appearance.Options.UseTextOptions = true;
            this.lblOneYoutubeURLNum.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblOneYoutubeURLNum.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblOneYoutubeURLNum.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblOneYoutubeURLNum.Location = new System.Drawing.Point(26, 74);
            this.lblOneYoutubeURLNum.Name = "lblOneYoutubeURLNum";
            this.lblOneYoutubeURLNum.Size = new System.Drawing.Size(276, 23);
            this.lblOneYoutubeURLNum.TabIndex = 36;
            this.lblOneYoutubeURLNum.Text = "No links added yet!";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(16, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(3, 30);
            this.label1.TabIndex = 35;
            this.label1.Text = "label1";
            // 
            // PanelNoVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblOneYoutubeURLNum);
            this.Name = "PanelNoVideo";
            this.Size = new System.Drawing.Size(462, 165);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblOneYoutubeURLNum;
        private System.Windows.Forms.Label label1;
    }
}
