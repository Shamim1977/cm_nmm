﻿using System;

namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz10DistanceFromBiz : DevExpress.XtraEditors.XtraUserControl
    {
        public PanelWiz10DistanceFromBiz()
        {
            InitializeComponent();
        }

        private void Panel8DistanceFromBiz_Load(object sender, EventArgs e)
        {
            this.cmbPOIFromAPIDistanceKM.Properties.Items.Add("-None-");
            
            int[] Distances = {10, 15, 20, 25, 30, 40, 50 };
            for (int x = 0; x< Distances.Length; x++)
            {
                this.cmbPOIFromAPIDistanceKM.Properties.Items.Add(Distances[x]);
            }
            this.cmbPOIFromAPIDistanceKM.SelectedIndex = 4;

            this.cmbPOIFromAPIDistanceKM.Focus();
        }
    }
}
