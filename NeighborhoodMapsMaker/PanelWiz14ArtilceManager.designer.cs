﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz14ArtilceManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz14ArtilceManager));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblGoogleAuthEmail = new DevExpress.XtraEditors.LabelControl();
            this.lblArticleSpintaxAceeptable = new DevExpress.XtraEditors.LabelControl();
            this.rtArticle = new System.Windows.Forms.RichTextBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSpinnerServices = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnGetArticleAndSpin = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.btnGetArticle = new DevExpress.XtraEditors.SimpleButton();
            this.chkSpinArticle = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSpinnerServices.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSpinArticle.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(40, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(216, 23);
            this.labelControl1.TabIndex = 19;
            this.labelControl1.Text = "Manage Article";
            // 
            // lblGoogleAuthEmail
            // 
            this.lblGoogleAuthEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoogleAuthEmail.Appearance.Options.UseFont = true;
            this.lblGoogleAuthEmail.Location = new System.Drawing.Point(40, 60);
            this.lblGoogleAuthEmail.Name = "lblGoogleAuthEmail";
            this.lblGoogleAuthEmail.Size = new System.Drawing.Size(43, 18);
            this.lblGoogleAuthEmail.TabIndex = 18;
            this.lblGoogleAuthEmail.Text = "Article:";
            // 
            // lblArticleSpintaxAceeptable
            // 
            this.lblArticleSpintaxAceeptable.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArticleSpintaxAceeptable.Appearance.Options.UseFont = true;
            this.lblArticleSpintaxAceeptable.Location = new System.Drawing.Point(40, 85);
            this.lblArticleSpintaxAceeptable.Name = "lblArticleSpintaxAceeptable";
            this.lblArticleSpintaxAceeptable.Size = new System.Drawing.Size(115, 14);
            this.lblArticleSpintaxAceeptable.TabIndex = 23;
            this.lblArticleSpintaxAceeptable.Text = "(Spintax Acceptable)";
            // 
            // rtArticle
            // 
            this.rtArticle.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtArticle.Location = new System.Drawing.Point(40, 117);
            this.rtArticle.Name = "rtArticle";
            this.rtArticle.Size = new System.Drawing.Size(840, 301);
            this.rtArticle.TabIndex = 24;
            this.rtArticle.Text = "";
            this.rtArticle.TextChanged += new System.EventHandler(this.rtArticle_TextChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(49, 435);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(420, 14);
            this.labelControl2.TabIndex = 25;
            this.labelControl2.Text = "Select one or more words in the article and right click to get replace options";
            // 
            // cmbSpinnerServices
            // 
            this.cmbSpinnerServices.Location = new System.Drawing.Point(628, 76);
            this.cmbSpinnerServices.Name = "cmbSpinnerServices";
            this.cmbSpinnerServices.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSpinnerServices.Properties.Appearance.Options.UseFont = true;
            this.cmbSpinnerServices.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSpinnerServices.Properties.AppearanceDisabled.Options.UseFont = true;
            this.cmbSpinnerServices.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSpinnerServices.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbSpinnerServices.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSpinnerServices.Properties.AppearanceFocused.Options.UseFont = true;
            this.cmbSpinnerServices.Properties.AppearanceItemDisabled.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSpinnerServices.Properties.AppearanceItemDisabled.Options.UseFont = true;
            this.cmbSpinnerServices.Properties.AppearanceItemHighlight.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSpinnerServices.Properties.AppearanceItemHighlight.Options.UseFont = true;
            this.cmbSpinnerServices.Properties.AppearanceItemSelected.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSpinnerServices.Properties.AppearanceItemSelected.Options.UseFont = true;
            this.cmbSpinnerServices.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSpinnerServices.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cmbSpinnerServices.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSpinnerServices.Size = new System.Drawing.Size(181, 24);
            this.cmbSpinnerServices.TabIndex = 26;
            this.cmbSpinnerServices.Visible = false;
            // 
            // btnGetArticleAndSpin
            // 
            this.btnGetArticleAndSpin.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetArticleAndSpin.Appearance.Options.UseFont = true;
            this.btnGetArticleAndSpin.Location = new System.Drawing.Point(815, 76);
            this.btnGetArticleAndSpin.Name = "btnGetArticleAndSpin";
            this.btnGetArticleAndSpin.Size = new System.Drawing.Size(64, 23);
            this.btnGetArticleAndSpin.TabIndex = 27;
            this.btnGetArticleAndSpin.Text = "&Spin";
            this.btnGetArticleAndSpin.Visible = false;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.DodgerBlue;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(38, 434);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(2, 15);
            this.labelControl3.TabIndex = 31;
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 38;
            // 
            // btnGetArticle
            // 
            this.btnGetArticle.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetArticle.Appearance.Options.UseFont = true;
            this.btnGetArticle.Location = new System.Drawing.Point(180, 80);
            this.btnGetArticle.Name = "btnGetArticle";
            this.btnGetArticle.Size = new System.Drawing.Size(106, 23);
            this.btnGetArticle.TabIndex = 39;
            this.btnGetArticle.Text = "&Get Article";
            this.btnGetArticle.Click += new System.EventHandler(this.btnGetArticle_Click);
            // 
            // chkSpinArticle
            // 
            this.chkSpinArticle.Location = new System.Drawing.Point(498, 76);
            this.chkSpinArticle.Name = "chkSpinArticle";
            this.chkSpinArticle.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSpinArticle.Properties.Appearance.Options.UseFont = true;
            this.chkSpinArticle.Properties.Caption = "Spin Article";
            this.chkSpinArticle.Size = new System.Drawing.Size(124, 22);
            this.chkSpinArticle.TabIndex = 40;
            this.chkSpinArticle.EditValueChanged += new System.EventHandler(this.chkSpinArticle_EditValueChanged);
            // 
            // PanelWiz14ArtilceManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkSpinArticle);
            this.Controls.Add(this.btnGetArticle);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.btnGetArticleAndSpin);
            this.Controls.Add(this.cmbSpinnerServices);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.rtArticle);
            this.Controls.Add(this.lblArticleSpintaxAceeptable);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.lblGoogleAuthEmail);
            this.Name = "PanelWiz14ArtilceManager";
            this.Size = new System.Drawing.Size(894, 467);
            this.Tag = "13|15";
            this.Load += new System.EventHandler(this.Panel11ArtilceManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmbSpinnerServices.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSpinArticle.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblGoogleAuthEmail;
        private DevExpress.XtraEditors.LabelControl lblArticleSpintaxAceeptable;
        private System.Windows.Forms.RichTextBox rtArticle;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSpinnerServices;
        private DevExpress.XtraEditors.SimpleButton btnGetArticleAndSpin;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.SimpleButton btnGetArticle;
        private DevExpress.XtraEditors.CheckEdit chkSpinArticle;
    }
}
