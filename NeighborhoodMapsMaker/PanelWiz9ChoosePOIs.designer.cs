﻿namespace NeighborhoodMapsMaker
{
    partial class PanelWiz9ChoosePOIs
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelWiz9ChoosePOIs));
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.lblGooglePOIChooseWhichOnesHeader = new DevExpress.XtraEditors.LabelControl();
            this.chkLetMeChoosePOI = new DevExpress.XtraEditors.CheckEdit();
            this.chkUseUnused = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLetMeChoosePOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseUnused.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 45;
            // 
            // lblGooglePOIChooseWhichOnesHeader
            // 
            this.lblGooglePOIChooseWhichOnesHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGooglePOIChooseWhichOnesHeader.Appearance.Options.UseFont = true;
            this.lblGooglePOIChooseWhichOnesHeader.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGooglePOIChooseWhichOnesHeader.Location = new System.Drawing.Point(40, 6);
            this.lblGooglePOIChooseWhichOnesHeader.Name = "lblGooglePOIChooseWhichOnesHeader";
            this.lblGooglePOIChooseWhichOnesHeader.Size = new System.Drawing.Size(513, 23);
            this.lblGooglePOIChooseWhichOnesHeader.TabIndex = 44;
            this.lblGooglePOIChooseWhichOnesHeader.Text = "Other Places of Interest (POI)";
            // 
            // chkLetMeChoosePOI
            // 
            this.chkLetMeChoosePOI.Location = new System.Drawing.Point(40, 100);
            this.chkLetMeChoosePOI.Name = "chkLetMeChoosePOI";
            this.chkLetMeChoosePOI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLetMeChoosePOI.Properties.Appearance.Options.UseFont = true;
            this.chkLetMeChoosePOI.Properties.Caption = "Let me choose which ones I want";
            this.chkLetMeChoosePOI.Size = new System.Drawing.Size(265, 22);
            this.chkLetMeChoosePOI.TabIndex = 47;
            this.chkLetMeChoosePOI.EditValueChanged += new System.EventHandler(this.chkLetMeChoosePOI_EditValueChanged);
            // 
            // chkUseUnused
            // 
            this.chkUseUnused.EditValue = true;
            this.chkUseUnused.Location = new System.Drawing.Point(40, 61);
            this.chkUseUnused.Name = "chkUseUnused";
            this.chkUseUnused.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUseUnused.Properties.Appearance.Options.UseFont = true;
            this.chkUseUnused.Properties.Caption = "Choose ones I haven\'t used yet";
            this.chkUseUnused.Size = new System.Drawing.Size(242, 22);
            this.chkUseUnused.TabIndex = 46;
            this.chkUseUnused.EditValueChanged += new System.EventHandler(this.chkUseUnused_EditValueChanged);
            // 
            // PanelWiz9ChoosePOIs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkLetMeChoosePOI);
            this.Controls.Add(this.chkUseUnused);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblGooglePOIChooseWhichOnesHeader);
            this.Name = "PanelWiz9ChoosePOIs";
            this.Size = new System.Drawing.Size(563, 147);
            this.Tag = "8|10";
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLetMeChoosePOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseUnused.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.LabelControl lblGooglePOIChooseWhichOnesHeader;
        private DevExpress.XtraEditors.CheckEdit chkLetMeChoosePOI;
        private DevExpress.XtraEditors.CheckEdit chkUseUnused;
    }
}
