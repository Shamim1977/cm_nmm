﻿namespace NeighborhoodMapsMaker
{
    partial class PanelSett1General
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelSett1General));
            this.picFancyUSEPOI = new DevExpress.XtraEditors.PictureEdit();
            this.lblUseGooglePOIYesOrNo = new DevExpress.XtraEditors.LabelControl();
            this.lblUserEmail = new DevExpress.XtraEditors.LabelControl();
            this.txtUserEmail = new DevExpress.XtraEditors.TextEdit();
            this.lblEmailIDWhy = new DevExpress.XtraEditors.LabelControl();
            this.btnSett1Next = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.picArticleBuilderDotNet = new System.Windows.Forms.PictureBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtArticleBuilderAPIKey = new DevExpress.XtraEditors.TextEdit();
            this.picGooglePlaces = new System.Windows.Forms.PictureBox();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtGooglePlacesAPIKey = new DevExpress.XtraEditors.TextEdit();
            this.picWordAI = new System.Windows.Forms.PictureBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtWordAIPIKey = new DevExpress.XtraEditors.TextEdit();
            this.picSpinnerChief = new System.Windows.Forms.PictureBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtSpinnerChiefAPIKey = new DevExpress.XtraEditors.TextEdit();
            this.picSpinnerRewriter = new System.Windows.Forms.PictureBox();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtSpinWriterAPIKey = new DevExpress.XtraEditors.TextEdit();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.chkAutoLaunch = new DevExpress.XtraEditors.CheckEdit();
            this.chkStartSize = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picArticleBuilderDotNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArticleBuilderAPIKey.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGooglePlaces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGooglePlacesAPIKey.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picWordAI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWordAIPIKey.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpinnerChief)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpinnerChiefAPIKey.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpinnerRewriter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpinWriterAPIKey.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoLaunch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStartSize.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // picFancyUSEPOI
            // 
            this.picFancyUSEPOI.EditValue = ((object)(resources.GetObject("picFancyUSEPOI.EditValue")));
            this.picFancyUSEPOI.Location = new System.Drawing.Point(0, 0);
            this.picFancyUSEPOI.Name = "picFancyUSEPOI";
            this.picFancyUSEPOI.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFancyUSEPOI.Properties.Appearance.Options.UseBackColor = true;
            this.picFancyUSEPOI.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFancyUSEPOI.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picFancyUSEPOI.Size = new System.Drawing.Size(36, 36);
            this.picFancyUSEPOI.TabIndex = 18;
            // 
            // lblUseGooglePOIYesOrNo
            // 
            this.lblUseGooglePOIYesOrNo.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUseGooglePOIYesOrNo.Appearance.Options.UseFont = true;
            this.lblUseGooglePOIYesOrNo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblUseGooglePOIYesOrNo.Location = new System.Drawing.Point(40, 6);
            this.lblUseGooglePOIYesOrNo.Name = "lblUseGooglePOIYesOrNo";
            this.lblUseGooglePOIYesOrNo.Size = new System.Drawing.Size(274, 23);
            this.lblUseGooglePOIYesOrNo.TabIndex = 17;
            this.lblUseGooglePOIYesOrNo.Text = "Settings - General";
            // 
            // lblUserEmail
            // 
            this.lblUserEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserEmail.Appearance.Options.UseFont = true;
            this.lblUserEmail.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblUserEmail.Location = new System.Drawing.Point(31, 126);
            this.lblUserEmail.Name = "lblUserEmail";
            this.lblUserEmail.Size = new System.Drawing.Size(182, 24);
            this.lblUserEmail.TabIndex = 31;
            this.lblUserEmail.Text = "My Email Address:";
            // 
            // txtUserEmail
            // 
            this.txtUserEmail.Location = new System.Drawing.Point(219, 126);
            this.txtUserEmail.Name = "txtUserEmail";
            this.txtUserEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserEmail.Properties.Appearance.Options.UseFont = true;
            this.txtUserEmail.Size = new System.Drawing.Size(512, 24);
            this.txtUserEmail.TabIndex = 0;
            this.txtUserEmail.Leave += new System.EventHandler(this.txtUserEmail_Leave);
            // 
            // lblEmailIDWhy
            // 
            this.lblEmailIDWhy.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailIDWhy.Appearance.Options.UseFont = true;
            this.lblEmailIDWhy.Location = new System.Drawing.Point(219, 156);
            this.lblEmailIDWhy.Name = "lblEmailIDWhy";
            this.lblEmailIDWhy.Size = new System.Drawing.Size(224, 16);
            this.lblEmailIDWhy.TabIndex = 32;
            this.lblEmailIDWhy.Text = "(Used to send critical notifications only)";
            // 
            // btnSett1Next
            // 
            this.btnSett1Next.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSett1Next.Appearance.Options.UseFont = true;
            this.btnSett1Next.Location = new System.Drawing.Point(219, 409);
            this.btnSett1Next.Name = "btnSett1Next";
            this.btnSett1Next.Size = new System.Drawing.Size(95, 30);
            this.btnSett1Next.TabIndex = 6;
            this.btnSett1Next.TabStop = false;
            this.btnSett1Next.Text = "Save";
            this.btnSett1Next.Click += new System.EventHandler(this.btnSett1Next_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(737, 126);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(25, 24);
            this.simpleButton1.TabIndex = 50;
            // 
            // picArticleBuilderDotNet
            // 
            this.picArticleBuilderDotNet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picArticleBuilderDotNet.Image = ((System.Drawing.Image)(resources.GetObject("picArticleBuilderDotNet.Image")));
            this.picArticleBuilderDotNet.Location = new System.Drawing.Point(737, 254);
            this.picArticleBuilderDotNet.Name = "picArticleBuilderDotNet";
            this.picArticleBuilderDotNet.Size = new System.Drawing.Size(24, 24);
            this.picArticleBuilderDotNet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picArticleBuilderDotNet.TabIndex = 65;
            this.picArticleBuilderDotNet.TabStop = false;
            this.picArticleBuilderDotNet.Tag = "https://app.paykickstart.com/checkout/15520";
            this.toolTipController1.SetTitle(this.picArticleBuilderDotNet, "Neighborhood Maps Maker");
            this.toolTipController1.SetToolTip(this.picArticleBuilderDotNet, "Click to visit site for an API Key");
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(31, 257);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(169, 18);
            this.labelControl4.TabIndex = 64;
            this.labelControl4.Text = "ArticleBuilder.net API Key:";
            // 
            // txtArticleBuilderAPIKey
            // 
            this.txtArticleBuilderAPIKey.Location = new System.Drawing.Point(219, 254);
            this.txtArticleBuilderAPIKey.Name = "txtArticleBuilderAPIKey";
            this.txtArticleBuilderAPIKey.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtArticleBuilderAPIKey.Properties.Appearance.Options.UseFont = true;
            this.txtArticleBuilderAPIKey.Size = new System.Drawing.Size(512, 24);
            this.txtArticleBuilderAPIKey.TabIndex = 2;
            // 
            // picGooglePlaces
            // 
            this.picGooglePlaces.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picGooglePlaces.Image = ((System.Drawing.Image)(resources.GetObject("picGooglePlaces.Image")));
            this.picGooglePlaces.Location = new System.Drawing.Point(737, 204);
            this.picGooglePlaces.Name = "picGooglePlaces";
            this.picGooglePlaces.Size = new System.Drawing.Size(24, 24);
            this.picGooglePlaces.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picGooglePlaces.TabIndex = 63;
            this.picGooglePlaces.TabStop = false;
            this.picGooglePlaces.Tag = "https://console.cloud.google.com/google/maps-apis/apis/places-backend.googleapis." +
    "com/";
            this.toolTipController1.SetTitle(this.picGooglePlaces, "Neighborhood Maps Maker");
            this.toolTipController1.SetToolTip(this.picGooglePlaces, "Click to visit site for an API Key");
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(31, 207);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(152, 18);
            this.labelControl3.TabIndex = 62;
            this.labelControl3.Text = "Google Places API Key:";
            // 
            // txtGooglePlacesAPIKey
            // 
            this.txtGooglePlacesAPIKey.Location = new System.Drawing.Point(219, 204);
            this.txtGooglePlacesAPIKey.Name = "txtGooglePlacesAPIKey";
            this.txtGooglePlacesAPIKey.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGooglePlacesAPIKey.Properties.Appearance.Options.UseFont = true;
            this.txtGooglePlacesAPIKey.Size = new System.Drawing.Size(512, 24);
            this.txtGooglePlacesAPIKey.TabIndex = 1;
            // 
            // picWordAI
            // 
            this.picWordAI.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picWordAI.Image = ((System.Drawing.Image)(resources.GetObject("picWordAI.Image")));
            this.picWordAI.Location = new System.Drawing.Point(737, 364);
            this.picWordAI.Name = "picWordAI";
            this.picWordAI.Size = new System.Drawing.Size(24, 24);
            this.picWordAI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picWordAI.TabIndex = 61;
            this.picWordAI.TabStop = false;
            this.picWordAI.Tag = "https://wordai.com/login.php";
            this.toolTipController1.SetTitle(this.picWordAI, "Neighborhood Maps Maker");
            this.toolTipController1.SetToolTip(this.picWordAI, "Click to visit site for an API Key");
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(31, 367);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(118, 18);
            this.labelControl2.TabIndex = 60;
            this.labelControl2.Text = "Word AI API Key:";
            // 
            // txtWordAIPIKey
            // 
            this.txtWordAIPIKey.Location = new System.Drawing.Point(219, 364);
            this.txtWordAIPIKey.Name = "txtWordAIPIKey";
            this.txtWordAIPIKey.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWordAIPIKey.Properties.Appearance.Options.UseFont = true;
            this.txtWordAIPIKey.Size = new System.Drawing.Size(512, 24);
            this.txtWordAIPIKey.TabIndex = 5;
            // 
            // picSpinnerChief
            // 
            this.picSpinnerChief.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picSpinnerChief.Image = ((System.Drawing.Image)(resources.GetObject("picSpinnerChief.Image")));
            this.picSpinnerChief.Location = new System.Drawing.Point(737, 334);
            this.picSpinnerChief.Name = "picSpinnerChief";
            this.picSpinnerChief.Size = new System.Drawing.Size(24, 24);
            this.picSpinnerChief.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSpinnerChief.TabIndex = 59;
            this.picSpinnerChief.TabStop = false;
            this.picSpinnerChief.Tag = "http://account.spinnerchief.com/";
            this.toolTipController1.SetTitle(this.picSpinnerChief, "Neighborhood Maps Maker");
            this.toolTipController1.SetToolTip(this.picSpinnerChief, "Click to visit site for an API Key");
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(31, 337);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(147, 18);
            this.labelControl1.TabIndex = 58;
            this.labelControl1.Text = "Spinner Chief API Key:";
            // 
            // txtSpinnerChiefAPIKey
            // 
            this.txtSpinnerChiefAPIKey.Location = new System.Drawing.Point(219, 334);
            this.txtSpinnerChiefAPIKey.Name = "txtSpinnerChiefAPIKey";
            this.txtSpinnerChiefAPIKey.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpinnerChiefAPIKey.Properties.Appearance.Options.UseFont = true;
            this.txtSpinnerChiefAPIKey.Size = new System.Drawing.Size(512, 24);
            this.txtSpinnerChiefAPIKey.TabIndex = 4;
            // 
            // picSpinnerRewriter
            // 
            this.picSpinnerRewriter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picSpinnerRewriter.Image = ((System.Drawing.Image)(resources.GetObject("picSpinnerRewriter.Image")));
            this.picSpinnerRewriter.Location = new System.Drawing.Point(737, 304);
            this.picSpinnerRewriter.Name = "picSpinnerRewriter";
            this.picSpinnerRewriter.Size = new System.Drawing.Size(24, 24);
            this.picSpinnerRewriter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSpinnerRewriter.TabIndex = 57;
            this.picSpinnerRewriter.TabStop = false;
            this.picSpinnerRewriter.Tag = "https://www.spinrewriter.com/registration";
            this.toolTipController1.SetTitle(this.picSpinnerRewriter, "Neighborhood Maps Maker");
            this.toolTipController1.SetToolTip(this.picSpinnerRewriter, "Click to visit site for an API Key");
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(31, 307);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(146, 18);
            this.labelControl5.TabIndex = 56;
            this.labelControl5.Text = "Spin Rewriter API Key:";
            // 
            // txtSpinWriterAPIKey
            // 
            this.txtSpinWriterAPIKey.Location = new System.Drawing.Point(219, 304);
            this.txtSpinWriterAPIKey.Name = "txtSpinWriterAPIKey";
            this.txtSpinWriterAPIKey.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpinWriterAPIKey.Properties.Appearance.Options.UseFont = true;
            this.txtSpinWriterAPIKey.Size = new System.Drawing.Size(512, 24);
            this.txtSpinWriterAPIKey.TabIndex = 3;
            // 
            // chkAutoLaunch
            // 
            this.chkAutoLaunch.EditValue = true;
            this.chkAutoLaunch.Location = new System.Drawing.Point(219, 79);
            this.chkAutoLaunch.Name = "chkAutoLaunch";
            this.chkAutoLaunch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAutoLaunch.Properties.Appearance.Options.UseFont = true;
            this.chkAutoLaunch.Properties.Caption = "Start When OS Starts";
            this.chkAutoLaunch.Size = new System.Drawing.Size(193, 22);
            this.chkAutoLaunch.TabIndex = 66;
            // 
            // chkStartSize
            // 
            this.chkStartSize.Location = new System.Drawing.Point(418, 79);
            this.chkStartSize.Name = "chkStartSize";
            this.chkStartSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkStartSize.Properties.Appearance.Options.UseFont = true;
            this.chkStartSize.Properties.Caption = "Start Window - Minimized";
            this.chkStartSize.Size = new System.Drawing.Size(213, 22);
            this.chkStartSize.TabIndex = 67;
            // 
            // PanelSett1General
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkStartSize);
            this.Controls.Add(this.chkAutoLaunch);
            this.Controls.Add(this.picArticleBuilderDotNet);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.txtArticleBuilderAPIKey);
            this.Controls.Add(this.picGooglePlaces);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.txtGooglePlacesAPIKey);
            this.Controls.Add(this.picWordAI);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtWordAIPIKey);
            this.Controls.Add(this.picSpinnerChief);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtSpinnerChiefAPIKey);
            this.Controls.Add(this.picSpinnerRewriter);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.txtSpinWriterAPIKey);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnSett1Next);
            this.Controls.Add(this.lblEmailIDWhy);
            this.Controls.Add(this.lblUserEmail);
            this.Controls.Add(this.txtUserEmail);
            this.Controls.Add(this.picFancyUSEPOI);
            this.Controls.Add(this.lblUseGooglePOIYesOrNo);
            this.Name = "PanelSett1General";
            this.Size = new System.Drawing.Size(785, 458);
            this.Tag = "0|2";
            this.Load += new System.EventHandler(this.Panel1SettGeneral_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picFancyUSEPOI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picArticleBuilderDotNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArticleBuilderAPIKey.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGooglePlaces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGooglePlacesAPIKey.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picWordAI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWordAIPIKey.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpinnerChief)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpinnerChiefAPIKey.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpinnerRewriter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpinWriterAPIKey.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoLaunch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStartSize.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit picFancyUSEPOI;
        private DevExpress.XtraEditors.LabelControl lblUseGooglePOIYesOrNo;
        private DevExpress.XtraEditors.LabelControl lblUserEmail;
        private DevExpress.XtraEditors.TextEdit txtUserEmail;
        private DevExpress.XtraEditors.LabelControl lblEmailIDWhy;
        private DevExpress.XtraEditors.SimpleButton btnSett1Next;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.PictureBox picArticleBuilderDotNet;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtArticleBuilderAPIKey;
        private System.Windows.Forms.PictureBox picGooglePlaces;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtGooglePlacesAPIKey;
        private System.Windows.Forms.PictureBox picWordAI;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtWordAIPIKey;
        private System.Windows.Forms.PictureBox picSpinnerChief;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtSpinnerChiefAPIKey;
        private System.Windows.Forms.PictureBox picSpinnerRewriter;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtSpinWriterAPIKey;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraEditors.CheckEdit chkAutoLaunch;
        private DevExpress.XtraEditors.CheckEdit chkStartSize;
    }
}
