﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using DevExpress.Utils;

namespace NeighborhoodMapsMaker
{
    public partial class PanelWiz2AddGMB : DevExpress.XtraEditors.XtraUserControl
    {
        string TASKNAME;
        BackgroundWorker bw = new BackgroundWorker();

        public PanelWiz2AddGMB()
        {
            InitializeComponent();

            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;

            bw.DoWork += bw_DoWork;
            bw.ProgressChanged += bw_ProgressChanged;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true | e.Error != null) return;


            if (TASKNAME.Equals("GetPlaces"))
            {
                ResetAutoComplete();
                if (places_list.Count > 0)  places_list.RemoveAt(0);
            }

            if (TASKNAME.Equals("GetDetails"))
            {
                this.txtGMBURL.Text = web_found;
                this.txtGMBCity.Text = city_found;
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //Me.Text = e.ProgressPercentage.ToString() & "% completed"
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker worker = (BackgroundWorker)sender;

                if (bw.CancellationPending == true) e.Cancel = true;
                if (TASKNAME.Equals("GetPlaces")) GetPlaces();
                if (TASKNAME.Equals("GetDetails")) GetDetails();
            }
            catch
            {
                if (bw.WorkerSupportsCancellation == true)
                {
                    bw.CancelAsync();
                }
            }
        }

        string city_found = "";
        string state_found = "";
        string country_found = "";
        string web_found = "";

        private string GetAValFromJSON(string input, string level)
        {
            string val_to_ret = "";

            string marker = "\""+ level + "\"";
            int res_count = Regex.Matches(input, marker).Count;
            if (res_count <= 0) return val_to_ret;

            int spos = input.IndexOf(marker);
            while (!input.Substring(spos, 1).Equals("{")) spos--;

            string entry_marker = "\"long_name\"";
            spos = input.IndexOf(entry_marker, spos) + entry_marker.Length;
            spos = input.IndexOf("\"", spos) + 1;
            int epos = input.IndexOf("\"", spos);

            val_to_ret = epos - spos > 0 ? input.Substring(spos, epos - spos) : city_found;
            return val_to_ret;
        }

        private void GetDetails()
        {
            city_found = "";
            web_found = "";

            string goog_url = "https://maps.googleapis.com/maps/api/place/details/json?"
                + "placeid="+place_id
                + "&key="+g_places_api_key
                + "&sessiontoken=" + sess_token;

            string goog_resp = Globals.GetPageSource(goog_url);
            city_found = GetAValFromJSON(goog_resp, "locality");
            state_found = GetAValFromJSON(goog_resp, "administrative_area_level_1");
            country_found = GetAValFromJSON(goog_resp, "country");

            //Web
            string entry_marker = "\"website\" : \"";
            int spos = goog_resp.IndexOf(entry_marker) + entry_marker.Length;
            int epos = goog_resp.IndexOf("\"", spos);
            web_found = (epos>0 && spos>0 && epos-spos>0?goog_resp.Substring(spos, epos-spos):"");
            if (Globals.INSTANCE_COUNT(web_found, "http")<=0 
                && Globals.INSTANCE_COUNT(web_found, "www") <= 0) web_found = "";


            string sql = "UPDATE tbl_projects SET "
                + "GMBURL = " + (web_found .Equals("")?"NULL": "'"+ web_found+ "'") + ", "
                + "GMBCity = '" + city_found + "' "
                + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
            int res = clsSQLiteCRUD.UPDATE_ROWS(sql);


            this.txtGMBURL.Text = web_found;
            this.txtGMBCity.Text = city_found .Equals("")? biz_address : city_found;

        }

        string sess_token = "";
        Boolean GettingPlaces = false;
        private void GetPlaces()
        {
            GettingPlaces = true;

            string sql = "SELECT iso FROM tbl_countries WHERE nicename = '" + gmb_country_name + "';";
            gmb_country_iso = clsSQLiteCRUD.GetACellVal(sql);
            sess_token = Globals.SetAName() + "-" + Globals.VAR_PROJECT_NAME;

            string goog_url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
                + "key=" + g_places_api_key
                + "&input=" + WebUtility.UrlEncode(search_term)
                + "&region=" + gmb_country_iso
                + "&types=establishment"
                + "&sessiontoken=" + sess_token;

            //+ "&offset=" + search_term.Length.ToString()

            places_list = new System.Collections.Generic.List<String>();
            places_add_list = new System.Collections.Generic.List<String>();
            places_id_list = new System.Collections.Generic.List<String>();

            string goog_resp = Globals.GetPageSource(goog_url);
            int res_count = Regex.Matches(goog_resp, "\"place_id\"").Count;
            if (res_count <= 0) return;

            for (int x =0; x< res_count;x++)
            {
                string marker = "\"description\"";
                int marker_len = marker.Length;

                int spos = Globals.NTH_INDEX(goog_resp, marker, x + 1);
                int epos = (x==res_count-1?goog_resp.Length: goog_resp.IndexOf(marker, x + 2));
                string cur_scope = spos>0 && epos>0 && epos-spos>0? goog_resp.Substring(spos, epos - spos) :"";

                if (cur_scope.Equals("")) continue;

                spos = cur_scope.IndexOf(marker) + marker_len;
                spos = cur_scope.IndexOf("\"", spos) + 1;
                epos = cur_scope.IndexOf("\"", spos);
                string biz_desc = cur_scope.Substring(spos, epos-spos);


                int index_of_comma = biz_desc.IndexOf(",");
                string biz_name = "";
                string biz_full_add = "";
                if (biz_desc.Contains(","))
                {
                    biz_name = biz_desc.Substring(0, index_of_comma);
                    biz_full_add = biz_desc.Substring(index_of_comma + 1, biz_desc.Length - index_of_comma - 1);
                    biz_full_add = biz_full_add.Trim();

                    places_list.Add(biz_name);
                    places_add_list.Add(biz_full_add);
                }

                string id_marker = "\"place_id\"";
                int id_marker_len = id_marker.Length;

                spos = cur_scope.IndexOf(id_marker) + id_marker_len;
                spos = cur_scope.IndexOf("\"", spos) + 1;
                epos = cur_scope.IndexOf("\"", spos);
                string biz_id = cur_scope.Substring(spos, epos - spos);

                places_id_list.Add(biz_id);
            }

            GettingPlaces = false;
        }

        System.Collections.Generic.List<String> places_list = new System.Collections.Generic.List<String>();
        System.Collections.Generic.List<String> places_add_list = new System.Collections.Generic.List<String>();
        System.Collections.Generic.List<String> places_id_list = new System.Collections.Generic.List<String>();
        private void ResetAutoComplete()
        {

            if (pnlAutoSuggestHolder.Controls.Count > 0) pnlAutoSuggestHolder.Controls.Clear();
            if (places_list.Count==0)
            {
                HideSuggest();
                return;
            }

            pnlAutoSuggestHolder.Visible = true;

            for (int x = 0; x < places_list.Count; x++)
            {
                DevExpress.XtraEditors.XtraUserControl pnlPlaceCard
                    = (DevExpress.XtraEditors.XtraUserControl) new PanelPlaceCard();

                DevExpress.XtraEditors.LabelControl lblBizName
                   = (DevExpress.XtraEditors.LabelControl)pnlPlaceCard.Controls["lblBizName"];
                DevExpress.XtraEditors.LabelControl lblBizAd
                   = (DevExpress.XtraEditors.LabelControl)pnlPlaceCard.Controls["lblBizAdd"];

                lblBizName.Text = places_list[x];
                lblBizName.Tag = (x + 1).ToString();
                lblBizAd.Text = places_add_list[x];
                lblBizAd.Tag = (x+1).ToString();

                pnlPlaceCard.Tag = places_id_list[x];

                lblBizName.Font = new Font(lblBizName.Font.FontFamily, 10);
                lblBizName.ForeColor = Color.White; //Color.FromArgb(32, 31, 53);
                lblBizName.Cursor = System.Windows.Forms.Cursors.Hand;

                lblBizName.MouseEnter += new EventHandler(OneBizMouseEnter);
                lblBizName.MouseLeave += new EventHandler(OneBizMouseLeave);

                lblBizAd.ForeColor = Color.LightGray; //Color.FromArgb(32, 31, 53);

                lblBizName.Click += new EventHandler(OneBizClicker);
                lblBizAd.Click += new EventHandler(OneBizClicker);
                
                pnlAutoSuggestHolder.Controls.Add(pnlPlaceCard);
                pnlPlaceCard.SetBounds(0, (x * pnlPlaceCard.Height), pnlPlaceCard.Width, pnlPlaceCard.Height);
            }

            if (pnlAutoSuggestHolder.Controls.Count > 5)
            {
                pnlAutoSuggestHolder.VerticalScroll.Visible = true;
            }
            else
            {
                pnlAutoSuggestHolder.VerticalScroll.Visible = false;
            }

            //nullify
            places_list = new System.Collections.Generic.List<String>();
            places_add_list = new System.Collections.Generic.List<String>();
            places_id_list = new System.Collections.Generic.List<String>();
        }

        string place_id = "";
        string biz_address = "";
        private void OneBizClicker(object sender, EventArgs e)
        {

            string bizName = "";
            for (int x =0; x< pnlAutoSuggestHolder.Controls.Count; x++)
            {
                DevExpress.XtraEditors.XtraUserControl pnl_one_suggest
                     = (DevExpress.XtraEditors.XtraUserControl)pnlAutoSuggestHolder.Controls[x];
                
                DevExpress.XtraEditors.LabelControl lblBizName
                     = (DevExpress.XtraEditors.LabelControl)pnl_one_suggest.Controls["lblBizName"];

                DevExpress.XtraEditors.LabelControl lblBizAdd
                     = (DevExpress.XtraEditors.LabelControl)pnl_one_suggest.Controls["lblBizAdd"];


                if (lblBizName.Tag.ToString().Equals((x + 1).ToString()))
                {
                    place_id = pnl_one_suggest.Tag.ToString();
                    bizName = lblBizName.Text.Trim();
                    biz_address = lblBizAdd.Text.Trim();
                    txtGMBListing.Text = bizName;
                    break;
                }
                
            }

            //hide suggest panel
            pnlAutoSuggestHolder.Controls.Clear();
            pnlAutoSuggestHolder.Visible = false;

            if (place_id.Equals("") || bizName.Equals("")) return;

            //while (bw.IsBusy) System.Threading.Thread.Sleep(50);
            //Thread thr = new Thread(GetDetails);
            //thr.Start();

            string sql = "SELECT row_id FROM tbl_countries WHERE nicename = "
                + "'" + cmbCountries.SelectedItem.ToString().Replace("'", "''") + "'";
            string contry_id = clsSQLiteCRUD.GetACellVal(sql);

            sql = "UPDATE tbl_projects SET "
                + "GMBCountryID = " + contry_id + ", "
                + "NameOfGMB = '" + bizName + "' "
                + "WHERE RowID = " + Globals.VAR_PROJECT_ID.ToString() + ";";
            int res = clsSQLiteCRUD.UPDATE_ROWS(sql);


            GetDetails();

            //TASKNAME = "GetDetails";
            //bw.RunWorkerAsync();
        }

        private void OneBizMouseEnter(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.LabelControl lbl = (DevExpress.XtraEditors.LabelControl)sender;
            lbl.ForeColor = Color.White;
        }
        private void OneBizMouseLeave(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.LabelControl lbl = (DevExpress.XtraEditors.LabelControl)sender;
            lbl.ForeColor = Color.FromArgb(32, 31, 53);
        }

        private void Panel2AddGMB_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            DevExpress.XtraEditors.XtraForm.CheckForIllegalCrossThreadCalls = false;
    
            SetToolTip();
            this.txtGMBListing.Focus();

            this.pnlAutoSuggestHolder.SetBounds(194, 130, pnlAutoSuggestHolder.Width, pnlAutoSuggestHolder.Height);
            pnlAutoSuggestHolder.HorizontalScroll.Visible = false;

            GetCountries();

            string sql = "SELECT IFNULL(GooglePlacesAPIKey,'') AS GooglePlacesAPIKey FROM tbl_settings WHERE UserID = "
                + Globals.USER_ID.ToString() + ";";
            g_places_api_key = clsSQLiteCRUD.GetACellVal(sql);
        }

        private void GetCountries()
        {
            string stmt = "SELECT nicename FROM tbl_countries ORDER BY row_id;";
            DataTable dt = clsSQLiteCRUD.GiveADataTable(stmt);
            if (dt == null) return;
            if (dt.Rows.Count <= 0) return;

            cmbCountries.Properties.Items.Add("-None-");
            for (int x =0;x<dt.Rows.Count; x++) cmbCountries.Properties.Items.Add(dt.Rows[x][0]);

            //cmbCountries.SelectedIndex = 0;
        }

        private void SetToolTip()
        {
            ToolTipController defController = ToolTipController.DefaultController;
            defController.Appearance.BackColor = Color.AntiqueWhite;
            defController.ShowBeak = true;
            defController.ToolTipAnchor = ToolTipAnchor.Cursor;
            defController.ShowShadow = true;
            defController.ToolTipLocation = ToolTipLocation.RightTop;
            String msg = "Click to show description";
            //defController.SetToolTip(picQuestGMBListing, msg);
        }


        string g_places_api_key = "";
        string gmb_country_iso = "";
        string gmb_country_name = "";
        string search_term = "";

        private void GoHuntSuggests()
        {
            if (Globals.CheckNet() == false)
            {
                lblNoNetNoti.Text = "(No Internet Connection!)";
                return;
            }

            gmb_country_name = cmbCountries.SelectedItem.ToString();
            if (gmb_country_name.Equals("")) return;

            string pattern = txtGMBListing.Text.Trim();
            if (pattern.Equals("")) return;
            if (pattern.Length < 3) return;

            if (OLD_TERM.Equals(pattern)) return;

            if (g_places_api_key.Equals(""))
            {
                lblNoNetNoti.Text = "(Please add Google Places API Key in settings to get suggestions)";
                return;
            }
            
            
            OLD_TERM = pattern;
            search_term = pattern;
            lblNoNetNoti.Text = "";

            Thread thr = new Thread(GetPlaces);
            thr.Start();

            //GetPlaces();
            //TASKNAME = "GetPlaces";
            //bw.RunWorkerAsync();
        }

        private void PanelWiz2AddGMB_Click(object sender, EventArgs e)
        {
            HideSuggest();
        }

        private void HideSuggest()
        {
            if (bw.IsBusy) return;
            pnlAutoSuggestHolder.Visible = false;
            for (int x = 0; x < pnlAutoSuggestHolder.Controls.Count; x++) pnlAutoSuggestHolder.Controls.RemoveAt(x);
        }

        private void btnGetSuggest_Click(object sender, EventArgs e)
        {
            GoHuntSuggests();
        }

        private void txtGMBListing_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            GoHuntSuggests();
        }

        private void txtGMBListing_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            GoHuntSuggests();
        }

        string OLD_TERM = "";
        private void txtGMBListing_Enter(object sender, EventArgs e)
        {
            string v = txtGMBListing.Text.Trim();
            OLD_TERM = v;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (GettingPlaces == true) return;
            if (places_id_list == null) return;
            if (places_id_list.Count == 0) return;

            ResetAutoComplete();
        }

        private void btnSurfGMB_Click(object sender, EventArgs e)
        {
            string v = txtGMBURL.Text.Trim();
            if (v.Equals("")) return;
            if (!v.ToLower().Contains("http") && !v.ToLower().Contains("www")) return;
            System.Diagnostics.Process.Start(v);
        }
    }
}
